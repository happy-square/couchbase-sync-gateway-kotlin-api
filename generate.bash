mv library/.gitignore .gitignore
#mv library/gradle gradle
rm -r library
SPEC_URL="bundled-admin.yaml"
swagger-codegen generate \
  -l java \
  -i $SPEC_URL \
  --group-id "de.happy-square" \
  --artifact-id "couchbase-sync-gateway-kotlin-api" \
  --model-package "de.happysquare.couchbase.sg.models" \
  --api-package "de.happysquare.couchbase.sg.api" \
  -o library/
#echo -e "apply plugin: 'maven-publish'\n\n$(cat library/build.gradle)\n\npublishing {\n    publications {\n        maven(MavenPublication) {\n            from components.java\n        }\n    }\n}" > library/build.gradle
mv .gitignore library/.gitignore
#rm -r library/gradle
#mv gradle library/
