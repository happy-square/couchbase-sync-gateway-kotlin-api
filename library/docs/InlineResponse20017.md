# InlineResponse20017

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuer** | **String** |  |  [optional]
**authorizationEndpoint** | **String** |  |  [optional]
**tokenEndpoint** | **String** |  |  [optional]
**jwksUri** | **String** |  |  [optional]
**userinfoEndpoint** | **String** |  |  [optional]
**idTokenSigningAlgValuesSupported** | **String** |  |  [optional]
**responseTypesSupported** | **String** |  |  [optional]
**subjectTypesSupported** | **String** |  |  [optional]
**scopesSupported** | **String** |  |  [optional]
**claimsSupported** | **String** |  |  [optional]
**tokenEndpointAuthMethodsSupported** | **String** |  |  [optional]
