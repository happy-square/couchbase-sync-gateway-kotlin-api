# InlineResponse201

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The ID of the document that the operation was performed on. | 
**rev** | **String** | The new revision of the document if the operation was a success. |  [optional]
**error** | **String** | The error type if the operation of the document failed. |  [optional]
**reason** | **String** | The reason the operation failed. |  [optional]
**status** | **Integer** | The HTTP status code for why the operation failed. |  [optional]
