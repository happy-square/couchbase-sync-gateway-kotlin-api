# ExpVarsSyncgatewayPerReplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**$replicationId** | [**ExpVarsSyncgatewayReplicationId**](ExpVarsSyncgatewayReplicationId.md) |  |  [optional]
