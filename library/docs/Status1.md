# Status1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**databases** | [**Map&lt;String, StatusDatabases&gt;**](StatusDatabases.md) | Contains a map of all the databases in the node along with their status. |  [optional]
**version** | **String** | The product version including the build number and edition (ie. &#x60;EE&#x60; or &#x60;CE&#x60;).  Blank if &#x60;api.hide_product_version&#x3D;true&#x60; in the startup configuration. |  [optional]
