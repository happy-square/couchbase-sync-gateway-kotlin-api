# InlineResponse20015

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | [**InlineResponse20015Docid**](InlineResponse20015Docid.md) |  |  [optional]
