# InlineResponse20012Rows

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**value** | [**InlineResponse20012Value**](InlineResponse20012Value.md) |  |  [optional]
