# CollectionConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sync** | **String** | The Javascript function that newly created documents in this collection are ran through. |  [optional]
**importFilter** | **String** | This is the function that all imported documents in this collection are ran through in order to filter out what to import and what not to import. This allows you to control what is made available to Couchbase Mobile clients. If it is not set, then no documents are filtered when imported.  &#x60;import_docs&#x60; in the database config must be true to make this field applicable. |  [optional]
