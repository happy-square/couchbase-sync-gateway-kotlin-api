# KeyspacePurgeBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docId** | [**List&lt;DocIdEnum&gt;**](#List&lt;DocIdEnum&gt;) | The document ID to purge. The array must only be 1 element which is &#x60;*&#x60;.  All revisions will be permanently removed for that document. |  [optional]

<a name="List<DocIdEnum>"></a>
## Enum: List&lt;DocIdEnum&gt;
Name | Value
---- | -----
STAR | &quot;*&quot;
