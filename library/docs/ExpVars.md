# ExpVars

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cmdline** | **Object** | Built-in variables from the Go runtime, lists the command-line arguments |  [optional]
**memstats** | **Object** | Dumps a large amount of information about the memory heap and garbage collector |  [optional]
**cb** | **Object** | Variables reported by the Couchbase SDK (go_couchbase package) |  [optional]
**mc** | **Object** | Variables reported by the low-level memcached API (gomemcached package) |  [optional]
**syncGatewayChangeCache** | [**ExpVarsSyncGatewayChangeCache**](ExpVarsSyncGatewayChangeCache.md) |  |  [optional]
**syncGatewayDb** | [**ExpVarsSyncGatewayDb**](ExpVarsSyncGatewayDb.md) |  |  [optional]
**syncgateway** | [**ExpVarsSyncgateway**](ExpVarsSyncgateway.md) |  |  [optional]
