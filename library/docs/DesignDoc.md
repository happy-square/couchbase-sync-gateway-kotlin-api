# DesignDoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  |  [optional]
**views** | [**Map&lt;String, DbDesignddocViews&gt;**](DbDesignddocViews.md) |  |  [optional]
**options** | [**DbDesignddocOptions**](DbDesignddocOptions.md) |  |  [optional]
