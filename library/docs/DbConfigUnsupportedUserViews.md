# DbConfigUnsupportedUserViews

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether pass-through view query is supported through public API. |  [optional]
