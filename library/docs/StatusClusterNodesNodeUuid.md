# StatusClusterNodesNodeUuid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** | The nodes unique identifier. |  [optional]
**host** | **String** | The nodes host name. |  [optional]
