# HeapBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** | This is the file to output the pprof profile at. |  [optional]
