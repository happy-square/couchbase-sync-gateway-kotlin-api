# UserSessionInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authenticationHandlers** | **List&lt;String&gt;** | The ways authentication can be established to authenticate as the user. |  [optional]
**ok** | **Boolean** |  |  [optional]
**userCtx** | [**UsersessioninformationUserCtx**](UsersessioninformationUserCtx.md) |  |  [optional]
