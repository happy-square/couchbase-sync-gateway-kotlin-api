# Newrevision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The ID of the document. | 
**ok** | **Boolean** | Whether the request completed successfully. | 
**rev** | **String** | The revision of the document. | 
