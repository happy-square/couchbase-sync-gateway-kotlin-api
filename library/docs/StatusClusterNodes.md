# StatusClusterNodes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nodeUuid** | [**StatusClusterNodesNodeUuid**](StatusClusterNodesNodeUuid.md) |  |  [optional]
