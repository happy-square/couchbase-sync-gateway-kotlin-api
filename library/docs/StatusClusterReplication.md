# StatusClusterReplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replicationId** | [**Replication1**](Replication1.md) |  |  [optional]
