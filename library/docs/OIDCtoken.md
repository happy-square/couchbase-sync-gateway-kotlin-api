# OIDCtoken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** |  |  [optional]
**tokenType** | **String** |  |  [optional]
**refreshToken** | **String** |  |  [optional]
**expiresIn** | **String** |  |  [optional]
**idToken** | **String** |  |  [optional]
