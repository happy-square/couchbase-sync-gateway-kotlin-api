# DatabaseManagementApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDb**](DatabaseManagementApi.md#deleteDb) | **DELETE** /{db}/ | Remove a database
[**getAllDbs**](DatabaseManagementApi.md#getAllDbs) | **GET** /_all_dbs | Get a list of all the databases
[**getDb**](DatabaseManagementApi.md#getDb) | **GET** /{db}/ | Get database information
[**getDbCompact**](DatabaseManagementApi.md#getDbCompact) | **GET** /{db}/_compact | Get the status of the most recent compact operation
[**getDbResync**](DatabaseManagementApi.md#getDbResync) | **GET** /{db}/_resync | Get resync status
[**getKeyspaceChanges**](DatabaseManagementApi.md#getKeyspaceChanges) | **GET** /{keyspace}/_changes | Get changes list
[**headAllDbs**](DatabaseManagementApi.md#headAllDbs) | **HEAD** /_all_dbs | /_all_dbs
[**headDb**](DatabaseManagementApi.md#headDb) | **HEAD** /{db}/ | Check if database exists
[**headKeyspaceChanges**](DatabaseManagementApi.md#headKeyspaceChanges) | **HEAD** /{keyspace}/_changes | /{db}/_changes
[**postDbCompact**](DatabaseManagementApi.md#postDbCompact) | **POST** /{db}/_compact | Manage a compact operation
[**postDbEnsureFullCommit**](DatabaseManagementApi.md#postDbEnsureFullCommit) | **POST** /{db}/_ensure_full_commit | /{db}/_ensure_full_commit
[**postDbOffline**](DatabaseManagementApi.md#postDbOffline) | **POST** /{db}/_offline | Take the database offline
[**postDbOnline**](DatabaseManagementApi.md#postDbOnline) | **POST** /{db}/_online | Bring the database online
[**postDbResync**](DatabaseManagementApi.md#postDbResync) | **POST** /{db}/_resync | Start or stop Resync
[**postKeyspaceChanges**](DatabaseManagementApi.md#postKeyspaceChanges) | **POST** /{keyspace}/_changes | Get changes list
[**postKeyspaceRevsDiff**](DatabaseManagementApi.md#postKeyspaceRevsDiff) | **POST** /{keyspace}/_revs_diff | Compare revisions to what is in the database
[**putDb**](DatabaseManagementApi.md#putDb) | **PUT** /{db}/ | Create a new Sync Gateway database

<a name="deleteDb"></a>
# **deleteDb**
> Object deleteDb(db)

Remove a database

Removes a database from the Sync Gateway cluster  **Note:** If running in legacy mode, this will only delete the database from the current node.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    Object result = apiInstance.deleteDb(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#deleteDb");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllDbs"></a>
# **getAllDbs**
> List&lt;String&gt; getAllDbs()

Get a list of all the databases

This retrieves all the database&#x27;s names that are in the current Sync Gateway node.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
try {
    List<String> result = apiInstance.getAllDbs();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#getAllDbs");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDb"></a>
# **getDb**
> InlineResponse2009 getDb(db)

Get database information

Retrieve information about the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    InlineResponse2009 result = apiInstance.getDb(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#getDb");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbCompact"></a>
# **getDbCompact**
> Compactstatus getDbCompact(db, type)

Get the status of the most recent compact operation

This will retrieve the current status of the most recent compact operation.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String type = "tombstone"; // String | This is the type of compaction to use. The type must be either: * `attachment` for cleaning up legacy (pre-3.0) attachments * `tombstone` for purging the JSON bodies of non-leaf revisions.'
try {
    Compactstatus result = apiInstance.getDbCompact(db, type);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#getDbCompact");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **type** | **String**| This is the type of compaction to use. The type must be either: * &#x60;attachment&#x60; for cleaning up legacy (pre-3.0) attachments * &#x60;tombstone&#x60; for purging the JSON bodies of non-leaf revisions.&#x27; | [optional] [default to tombstone] [enum: attachment, tombstone]

### Return type

[**Compactstatus**](Compactstatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbResync"></a>
# **getDbResync**
> Resyncstatus getDbResync(db)

Get resync status

This will retrieve the status of last resync operation (whether it is running or not) in the Sync Gateway cluster.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    Resyncstatus result = apiInstance.getDbResync(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#getDbResync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**Resyncstatus**](Resyncstatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceChanges"></a>
# **getKeyspaceChanges**
> InlineResponse20013 getKeyspaceChanges(keyspace, limit, since, style, activeOnly, includeDocs, revocations, filter, channels, docIds, heartbeat, timeout, feed, requestPlus)

Get changes list

This request retrieves a sorted list of changes made to documents in the database, in time order of application. Each document appears at most once, ordered by its most recent change, regardless of how many times it has been changed.  This request can be used to listen for update and modifications to the database for post processing or synchronization. A continuously connected changes feed is a reasonable approach for generating a real-time log for most applications.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
Integer limit = 56; // Integer | Maximum number of changes to return.
String since = "since_example"; // String | Starts the results from the change immediately after the given sequence ID. Sequence IDs should be considered opaque; they come from the last_seq property of a prior response.
String style = "main_only"; // String | Controls whether to return the current winning revision (`main_only`) or all the leaf revision including conflicts and deleted former conflicts (`all_docs`).
Boolean activeOnly = false; // Boolean | Set true to exclude deleted documents and notifications for documents the user no longer has access to from the changes feed.
String includeDocs = "includeDocs_example"; // String | Include the body associated with each document.
Boolean revocations = true; // Boolean | If true, revocation messages will be sent on the changes feed.
String filter = "filter_example"; // String | Set a filter to either filter by channels or document IDs.
String channels = "channels_example"; // String | A comma-separated list of channel names to filter the response to only the channels specified. To use this option, the `filter` query option must be set to `sync_gateway/bychannels`.
List<String> docIds = Arrays.asList("docIds_example"); // List<String> | A valid JSON array of document IDs to filter the documents in the response to only the documents specified. To use this option, the `filter` query option must be set to `_doc_ids` and the `feed` parameter must be `normal`. Also accepts a comma separated list of document IDs instead.
Integer heartbeat = 0; // Integer | The interval (in milliseconds) to send an empty line (CRLF) in the response. This is to help prevent gateways from deciding the socket is idle and therefore closing it. This is only applicable to `feed=longpoll` or `feed=continuous`. This will override any timeouts to keep the feed alive indefinitely. Setting to 0 results in no heartbeat. The maximum heartbeat can be set in the server replication configuration.
Integer timeout = 300000; // Integer | This is the maximum period (in milliseconds) to wait for a change before the response is sent, even if there are no results. This is only applicable for `feed=longpoll` or `feed=continuous` changes feeds. Setting to 0 results in no timeout.
String feed = "normal"; // String | The type of changes feed to use. 
Boolean requestPlus = false; // Boolean | When true, ensures all valid documents written prior to the request being issued are included in the response.  This is only applicable for non-continuous feeds.
try {
    InlineResponse20013 result = apiInstance.getKeyspaceChanges(keyspace, limit, since, style, activeOnly, includeDocs, revocations, filter, channels, docIds, heartbeat, timeout, feed, requestPlus);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#getKeyspaceChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **limit** | **Integer**| Maximum number of changes to return. | [optional]
 **since** | **String**| Starts the results from the change immediately after the given sequence ID. Sequence IDs should be considered opaque; they come from the last_seq property of a prior response. | [optional]
 **style** | **String**| Controls whether to return the current winning revision (&#x60;main_only&#x60;) or all the leaf revision including conflicts and deleted former conflicts (&#x60;all_docs&#x60;). | [optional] [default to main_only] [enum: main_only, all_docs]
 **activeOnly** | **Boolean**| Set true to exclude deleted documents and notifications for documents the user no longer has access to from the changes feed. | [optional] [default to false]
 **includeDocs** | **String**| Include the body associated with each document. | [optional]
 **revocations** | **Boolean**| If true, revocation messages will be sent on the changes feed. | [optional]
 **filter** | **String**| Set a filter to either filter by channels or document IDs. | [optional] [enum: sync_gateway/bychannel, _doc_ids]
 **channels** | **String**| A comma-separated list of channel names to filter the response to only the channels specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;sync_gateway/bychannels&#x60;. | [optional]
 **docIds** | [**List&lt;String&gt;**](String.md)| A valid JSON array of document IDs to filter the documents in the response to only the documents specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;_doc_ids&#x60; and the &#x60;feed&#x60; parameter must be &#x60;normal&#x60;. Also accepts a comma separated list of document IDs instead. | [optional]
 **heartbeat** | **Integer**| The interval (in milliseconds) to send an empty line (CRLF) in the response. This is to help prevent gateways from deciding the socket is idle and therefore closing it. This is only applicable to &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60;. This will override any timeouts to keep the feed alive indefinitely. Setting to 0 results in no heartbeat. The maximum heartbeat can be set in the server replication configuration. | [optional] [default to 0] [enum: ]
 **timeout** | **Integer**| This is the maximum period (in milliseconds) to wait for a change before the response is sent, even if there are no results. This is only applicable for &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60; changes feeds. Setting to 0 results in no timeout. | [optional] [default to 300000] [enum: ]
 **feed** | **String**| The type of changes feed to use.  | [optional] [default to normal] [enum: normal, longpoll, continuous, websocket]
 **requestPlus** | **Boolean**| When true, ensures all valid documents written prior to the request being issued are included in the response.  This is only applicable for non-continuous feeds. | [optional] [default to false]

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headAllDbs"></a>
# **headAllDbs**
> headAllDbs()

/_all_dbs

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
try {
    apiInstance.headAllDbs();
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#headAllDbs");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="headDb"></a>
# **headDb**
> headDb(db)

Check if database exists

Check if a database exists by using the response status code.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDb(db);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#headDb");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceChanges"></a>
# **headKeyspaceChanges**
> headKeyspaceChanges(keyspace)

/{db}/_changes

Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
try {
    apiInstance.headKeyspaceChanges(keyspace);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#headKeyspaceChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postDbCompact"></a>
# **postDbCompact**
> postDbCompact(db, type, action, reset, dryRun)

Manage a compact operation

This allows a new compact operation to be done on the database, or to stop an existing running compact operation.  The type of compaction that is done depends on what the &#x60;type&#x60; query parameter is set to. The 2 options will: * &#x60;tombstone&#x60; - purge the JSON bodies of non-leaf revisions. This is known as database compaction. Database compaction is done periodically automatically by the system. JSON bodies of leaf nodes (conflicting branches) are not removed therefore it is important to resolve conflicts in order to re-claim disk space. * &#x60;attachment&#x60; - purge all unlinked/unused legacy (pre 3.0) attachments. If the previous attachment compact operation failed, this will attempt to restart the &#x60;compact_id&#x60; at the appropriate phase (if possible).  Both types can each have a maximum of 1 compact operation running at any one point. This means that an attachment compaction can be running at the same time as a tombstone compaction but not 2 tombstone compactions.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String type = "tombstone"; // String | This is the type of compaction to use. The type must be either: * `attachment` for cleaning up legacy (pre-3.0) attachments * `tombstone` for purging the JSON bodies of non-leaf revisions.'
String action = "start"; // String | Defines whether the a compact operation is being started or stopped.
Boolean reset = true; // Boolean | **Attachment compaction only**  This forces a fresh compact start instead of trying to resume the previous failed compact operation.
Boolean dryRun = true; // Boolean | **Attachment compaction only**  This will run through all 3 stages of attachment compact but will not purge any attachments. This can be used to check how many attachments will be purged.'
try {
    apiInstance.postDbCompact(db, type, action, reset, dryRun);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postDbCompact");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **type** | **String**| This is the type of compaction to use. The type must be either: * &#x60;attachment&#x60; for cleaning up legacy (pre-3.0) attachments * &#x60;tombstone&#x60; for purging the JSON bodies of non-leaf revisions.&#x27; | [optional] [default to tombstone] [enum: attachment, tombstone]
 **action** | **String**| Defines whether the a compact operation is being started or stopped. | [optional] [default to start] [enum: start, stop]
 **reset** | **Boolean**| **Attachment compaction only**  This forces a fresh compact start instead of trying to resume the previous failed compact operation. | [optional]
 **dryRun** | **Boolean**| **Attachment compaction only**  This will run through all 3 stages of attachment compact but will not purge any attachments. This can be used to check how many attachments will be purged.&#x27; | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbEnsureFullCommit"></a>
# **postDbEnsureFullCommit**
> InlineResponse2011 postDbEnsureFullCommit(db)

/{db}/_ensure_full_commit

This endpoint is non-functional but is present for CouchDB compatibility.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    InlineResponse2011 result = apiInstance.postDbEnsureFullCommit(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postDbEnsureFullCommit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**InlineResponse2011**](InlineResponse2011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbOffline"></a>
# **postDbOffline**
> postDbOffline(db)

Take the database offline

This will take the database offline meaning actions can be taken without disrupting current operations ungracefully or having the restart the Sync Gateway instance.  This will not take the backing Couchbase Server bucket offline.  Taking a database offline that is in the progress of coming online will take the database offline after it comes online.  Taking the database offline will: * Close all active &#x60;_changes&#x60; feeds for the database. * Reject all access to the database via the Public REST API (returning a 503 Service Unavailable code). * Reject most Admin API requests (by returning a 503 Service Unavailable code). The only endpoints to be available are: the resync endpoints, the configuration endpoints, &#x60;DELETE, GET, HEAD /{db}/&#x60;, &#x60;POST /{db}/_offline&#x60;, and &#x60;POST /{db}/_online&#x60;. * Stops webhook event handlers.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.postDbOffline(db);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postDbOffline");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbOnline"></a>
# **postDbOnline**
> postDbOnline(db, body)

Bring the database online

This will bring the database online so the Public and full Admin REST API requests can be served.  Bringing a database online will: * Close the database connection to the backing Couchbase Server bucket. * Reload the database configuration, and connect to the backing Couchbase Server bucket. * Re-establish access to the database from the Public REST API and accept all Admin API requests.  A specific delay before bringing the database online may be wanted to: * Make the database available for Couchbase Lite clients at a specific time. * Make the databases on several Sync Gateway instances available at the same time.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
DbOnlineBody body = new DbOnlineBody(); // DbOnlineBody | Add an optional delay to wait before bringing the database online
try {
    apiInstance.postDbOnline(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postDbOnline");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**DbOnlineBody**](DbOnlineBody.md)| Add an optional delay to wait before bringing the database online | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDbResync"></a>
# **postDbResync**
> Resyncstatus postDbResync(db, body, action, regenerateSequences)

Start or stop Resync

This can be used to start or stop a resync operation. A resync operation will cause all documents in the keyspace to be reprocessed through the sync function.  Generally, a resync operation might be wanted when the sync function has been modified in such a way that the channel or access mappings for any existing documents would change as a result.  A resync operation cannot be run if the database is online. The database can be taken offline by calling the &#x60;POST /{db}/_offline&#x60; endpoint.  In a multi-node cluster, the resync operation *must* be run on only a single node. Therefore, users should bring other nodes offline before initiating this action. Undefined system behaviour will happen if running resync on more than 1 node.  The &#x60;requireUser()&#x60; and &#x60;requireRole()&#x60; calls in the sync function will always return &#x60;true&#x60;.  - **action&#x3D;start** - This is an asynchronous operation, and will start resync in the background. - **action&#x3D;stop** - This will stop the currently running resync operation.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect 

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
DbResyncBody body = new DbResyncBody(); // DbResyncBody | 
String action = "start"; // String | This is whether to start a new resync job or stop an existing one.
Boolean regenerateSequences = true; // Boolean | **Use this only when requested to do so by the Couchbase support team** This request will regenerate the sequence numbers for each document processed.
try {
    Resyncstatus result = apiInstance.postDbResync(db, body, action, regenerateSequences);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postDbResync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**DbResyncBody**](DbResyncBody.md)|  | [optional]
 **action** | **String**| This is whether to start a new resync job or stop an existing one. | [optional] [default to start] [enum: start, stop]
 **regenerateSequences** | **Boolean**| **Use this only when requested to do so by the Couchbase support team** This request will regenerate the sequence numbers for each document processed. | [optional]

### Return type

[**Resyncstatus**](Resyncstatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspaceChanges"></a>
# **postKeyspaceChanges**
> InlineResponse20013 postKeyspaceChanges(keyspace, body)

Get changes list

This request retrieves a sorted list of changes made to documents in the database, in time order of application. Each document appears at most once, ordered by its most recent change, regardless of how many times it has been changed.  This request can be used to listen for update and modifications to the database for post processing or synchronization. A continuously connected changes feed is a reasonable approach for generating a real-time log for most applications.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
KeyspaceChangesBody body = new KeyspaceChangesBody(); // KeyspaceChangesBody | 
try {
    InlineResponse20013 result = apiInstance.postKeyspaceChanges(keyspace, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postKeyspaceChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**KeyspaceChangesBody**](KeyspaceChangesBody.md)|  | [optional]

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspaceRevsDiff"></a>
# **postKeyspaceRevsDiff**
> InlineResponse20015 postKeyspaceRevsDiff(keyspace, body)

Compare revisions to what is in the database

Takes a set of document IDs, each with a set of revision IDs. For each document, an array of unknown revisions are returned with an array of known revisions that may be recent ancestors.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
KeyspaceRevsDiffBody body = new KeyspaceRevsDiffBody(); // KeyspaceRevsDiffBody | 
try {
    InlineResponse20015 result = apiInstance.postKeyspaceRevsDiff(keyspace, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#postKeyspaceRevsDiff");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**KeyspaceRevsDiffBody**](KeyspaceRevsDiffBody.md)|  | [optional]

### Return type

[**InlineResponse20015**](InlineResponse20015.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDb"></a>
# **putDb**
> putDb(db, body, disableOidcValidation)

Create a new Sync Gateway database

This is to create a new database for Sync Gateway.  The new database name will be the name specified in the URL, not what is specified in the request body database configuration.  If the bucket is not provided in the database configuration, Sync Gateway will attempt to find and use the database name as the bucket.  By default, the new database will be brought online immediately. This can be avoided by including &#x60;\&quot;offline\&quot;: true&#x60; in the configuration in the request body.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseManagementApi;


DatabaseManagementApi apiInstance = new DatabaseManagementApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Databaseconfig3 body = new Databaseconfig3(); // Databaseconfig3 | The configuration to use for the new database
Boolean disableOidcValidation = false; // Boolean | If set, will not attempt to validate the configured OpenID Connect providers are reachable.
try {
    apiInstance.putDb(db, body, disableOidcValidation);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseManagementApi#putDb");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**Databaseconfig3**](Databaseconfig3.md)| The configuration to use for the new database | [optional]
 **disableOidcValidation** | **Boolean**| If set, will not attempt to validate the configured OpenID Connect providers are reachable. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

