# KeyspaceBulkGetBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docs** | [**List&lt;KeyspaceBulkGetDocs&gt;**](KeyspaceBulkGetDocs.md) |  | 
