# DbConfigUnsupportedOidcTestProvider

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether the &#x60;oidc_test_provider&#x60; endpoints should be exposed on the public API. |  [optional]
