# ExpVarsSyncgatewayGlobalResourceUtilization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adminNetBytesRecv** | **Integer** |  |  [optional]
**adminNetBytesSent** | **Integer** |  |  [optional]
**errorCount** | **Integer** |  |  [optional]
**goMemstatsHeapalloc** | **Integer** |  |  [optional]
**goMemstatsHeapidle** | **Integer** |  |  [optional]
**goMemstatsHeapinuse** | **Integer** |  |  [optional]
**goMemstatsHeapreleased** | **Integer** |  |  [optional]
**goMemstatsPausetotalns** | **Integer** |  |  [optional]
**goMemstatsStackinuse** | **Integer** |  |  [optional]
**goMemstatsStacksys** | **Integer** |  |  [optional]
**goMemstatsSys** | **Integer** |  |  [optional]
**goroutinesHighWatermark** | **Integer** |  |  [optional]
**numGoroutines** | **Integer** |  |  [optional]
**processCpuPercentUtilization** | **Integer** |  |  [optional]
**processMemoryResident** | **Integer** |  |  [optional]
**pubNetBytesRecv** | **Integer** |  |  [optional]
**pubNetBytesSent** | **Integer** |  |  [optional]
**systemMemoryTotal** | **Integer** |  |  [optional]
**warnCount** | **Integer** |  |  [optional]
