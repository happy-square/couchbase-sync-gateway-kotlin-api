# Resyncstatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**StatusEnum**](#StatusEnum) | The status of the current operation. | 
**startTime** | **String** | The ISO-8601 date and time the resync operation was started. | 
**lastError** | **String** | The last error that occurred in the resync operation (if any). | 
**docsChanged** | **Integer** | The amount of documents that have been changed as a result of the resync operation. | 
**docsProcessed** | **Integer** | The amount of docs that have been processed so far in the resync operation. | 

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
RUNNING | &quot;running&quot;
COMPLETED | &quot;completed&quot;
STOPPING | &quot;stopping&quot;
STOPPED | &quot;stopped&quot;
ERROR | &quot;error&quot;
