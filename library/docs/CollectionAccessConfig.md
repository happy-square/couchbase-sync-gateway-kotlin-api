# CollectionAccessConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adminChannels** | **List&lt;String&gt;** | A list of channels to explicitly grant to the user. |  [optional]
**allChannels** | **List&lt;String&gt;** | All the channels that the user has been granted access to.  Access could have been granted through the sync function, roles, or explicitly on the user under the &#x60;admin_channels&#x60; property. |  [optional]
**jwtChannels** | **List&lt;String&gt;** | The channels that the user has been granted access to through channels_claim. |  [optional]
**jwtLastUpdated** | [**OffsetDateTime**](OffsetDateTime.md) | The last time that the user&#x27;s JWT roles/channels were updated. |  [optional]
