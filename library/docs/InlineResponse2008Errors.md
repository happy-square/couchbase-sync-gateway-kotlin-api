# InlineResponse2008Errors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** |  |  [optional]
**reason** | **String** |  |  [optional]
