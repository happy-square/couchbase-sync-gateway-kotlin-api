# KeyspaceAllDocsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keys** | **List&lt;String&gt;** | List of the documents to retrieve. | 
