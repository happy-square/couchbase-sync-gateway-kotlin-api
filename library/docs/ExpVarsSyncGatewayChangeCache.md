# ExpVarsSyncGatewayChangeCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxPending** | **Object** | Max number of sequences waiting on a missing earlier sequence number |  [optional]
**lagTap0000ms** | **Object** | Histogram of delay from doc save till it shows up in Tap feed |  [optional]
**lagQueue0000ms** | **Object** | Histogram of delay from Tap feed till doc is posted to changes feed |  [optional]
**lagTotal0000ms** | **Object** | Histogram of total delay from doc save till posted to changes feed |  [optional]
**outOfOrder** | **Object** | Number of out-of-order sequences posted |  [optional]
**viewQueries** | **Object** | Number of queries to channels view |  [optional]
