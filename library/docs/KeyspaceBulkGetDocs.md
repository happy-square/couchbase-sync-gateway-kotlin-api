# KeyspaceBulkGetDocs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | ID of the document to retrieve. | 
