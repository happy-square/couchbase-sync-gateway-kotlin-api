# Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the role.  Role names can only have alphanumeric ASCII characters and underscores. |  [optional]
**adminChannels** | **List&lt;String&gt;** | The channels that users in the role are able to access for the default collection. |  [optional]
**allChannels** | **List&lt;String&gt;** | The channels that the role grants access to for the default collection.  These channels could have been assigned by the Sync function or using the &#x60;admin_channels&#x60; property. |  [optional]
**collectionAccess** | [**Map&lt;String, Map&lt;String, Object&gt;&gt;**](Map.md) | A set of access grants by scope and collection. |  [optional]
