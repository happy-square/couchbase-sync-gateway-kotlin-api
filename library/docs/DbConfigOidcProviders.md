# DbConfigOidcProviders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuer** | **String** | The URL for the OpenID Connect issuer. |  [optional]
**register** | **Boolean** | If to register a new Sync Gateway user account when a user logs in with OpenID Connect. |  [optional]
**clientId** | **String** | The OpenID Connect provider client ID. |  [optional]
**validationKey** | **String** | The OpenID Connect provider client secret. |  [optional]
**callbackUrl** | **String** | The URL that the OpenID Connect will redirect to after authentication.  If not provided, a callback URL will be generated. |  [optional]
**disableSession** | **Boolean** | Disable Sync Gateway session creation on successful OpenID Connect authentication. |  [optional]
**scope** | **List&lt;String&gt;** | The scope sent for the OpenID Connect request. |  [optional]
**includeAccess** | **Boolean** | This is whether the &#x60;_oidc_callback&#x60; response should include the OpenID Connect access token and associated fields (such as &#x60;token_type&#x60;, and &#x60;expires_in&#x60;). |  [optional]
**userPrefix** | **String** | This is the username prefix for all users created through this provider. |  [optional]
**discoveryUrl** | **String** | The non-standard discovery endpoint. |  [optional]
**disableCfgValidation** | **Boolean** | This bypasses the configuration validation based on the OpenID Connect specifications. This may be required for some OpenID providers that don&#x27;t strictly adhere to the specifications. |  [optional]
**disableCallbackState** | **Boolean** | Controls whether to maintain state between the auth request and callback endpoints (&#x60;/_oidc&#x60; and &#x60;/_oidc_callback&#x60;).  **This is not recommended as it would cause OpenID Connect authentication to be vulnerable to Cross-Site Request Forgery (CSRF, XSRF).** |  [optional]
**usernameClaim** | **String** | Allows a different OpenID Connect field to be specified instead of the Subject (&#x60;sub&#x60;).  The field name to use can be specified here. |  [optional]
**rolesClaim** | **String** | If set, the value(s) of the given OpenID Connect authentication token claim will be added to the user&#x27;s roles.  The value of this claim must be either a string or an array of strings, any other type will result in an error. |  [optional]
**channelsClaim** | **String** | If set, the value(s) of the given OpenID Connect authentication token claim will be added to the user&#x27;s channels.  The value of this claim must be either a string or an array of strings, any other type will result in an error. |  [optional]
**allowUnsignedProviderTokens** | **Boolean** | Allows users accept unsigned tokens from providers. |  [optional]
**isDefault** | **Boolean** | Indicates if this is the default OpenID Connect provider. |  [optional]
**name** | **String** | The name of the OpenID Connect Provider. |  [optional]
**insecureSkipVerify** | **Boolean** | Determines whether the TLS certificate verification should be disabled for this provider.  |  [optional]
