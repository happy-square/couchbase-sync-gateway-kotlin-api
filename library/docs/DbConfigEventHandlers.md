# DbConfigEventHandlers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxProcesses** | **String** | The maximum amount of concurrent event handling independent functions that can be running at the same time. |  [optional]
**waitForProcess** | **String** | The maximum amount of time (in milliseconds) to wait when the even queue is full. |  [optional]
**documentChanged** | [**Eventconfig**](Eventconfig.md) |  |  [optional]
**dbStateChanged** | [**Eventconfig**](Eventconfig.md) |  |  [optional]
