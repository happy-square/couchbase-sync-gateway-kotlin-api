# DbConfigUnsupportedApiEndpoints

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enableCouchbaseBucketFlush** | **Boolean** | **Setting for test purposes only**  Whether Couchbase buckets can be flushed via Admin REST API. |  [optional]
