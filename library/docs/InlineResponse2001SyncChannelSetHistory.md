# InlineResponse2001SyncChannelSetHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**start** | **String** |  |  [optional]
**end** | **String** |  |  [optional]
