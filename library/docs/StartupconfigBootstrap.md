# StartupconfigBootstrap

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** | The config group ID to use when discovering databases. Allows for non-homogenous configuration. |  [optional]
**configUpdateFrequency** | **String** | How often to poll Couchbase Server for new config changes.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**server** | **String** | Couchbase Server connection string/URL. | 
**username** | **String** | Username for authenticating to server. | 
**password** | **String** | Password for authenticating to server | 
**caCertPath** | **String** | Root CA cert path for TLS connection |  [optional]
**serverTlsSkipVerify** | **Boolean** | Allow empty server CA Cert Path without attempting to use system root pool |  [optional]
**x509CertPath** | **String** | Cert path (public key) for X.509 bucket auth |  [optional]
**x509KeyPath** | **String** | Key path (private key) for X.509 bucket auth |  [optional]
**useTlsServer** | **Boolean** | Enforces a secure or non-secure server scheme |  [optional]
