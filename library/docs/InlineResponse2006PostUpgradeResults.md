# InlineResponse2006PostUpgradeResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**removedDesignDocs** | **List&lt;String&gt;** | The design documents that have or will be removed. | 
**removedIndexes** | **List&lt;String&gt;** | The indexes that have or will be removed. | 
