# ExpVarsSyncgateway

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global** | [**ExpVarsSyncgatewayGlobal**](ExpVarsSyncgatewayGlobal.md) |  |  [optional]
**perDb** | [**List&lt;ExpVarsSyncgatewayPerDb&gt;**](ExpVarsSyncgatewayPerDb.md) | This array contains stats for all databases declared in the config file -- see the [Sync Gateway Statistics Schema](./../stats-monitoring.html) for more details on the metrics collected and reported by Sync Gateway. The statistics for each {$db_name} database are grouped into: - cache related statistics - collections statistics - cbl_replication_push - cbl_replication_pull - database_related_statistics - delta_sync - gsi_views - security_related_statistics - shared_bucket_import - per_replication statistics for each &#x60;replication_id&#x60; |  [optional]
**perReplication** | [**List&lt;ExpVarsSyncgatewayPerReplication&gt;**](ExpVarsSyncgatewayPerReplication.md) | An array of stats for each replication declared in the config file **Deprecated @ 2.8**: used only by inter-sync-gateway replications version 1. |  [optional]
