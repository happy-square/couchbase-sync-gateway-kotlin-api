# ExpVarsSyncgatewayReplicationId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sgrActive** | **Boolean** | Whether the replication is active at this time. **Deprecated @ 2.8**: used only by inter-sync-gateway replications version 1. |  [optional]
**sgrDocsCheckedSent** | **Integer** | The total number of documents checked for changes since replication started. This represents the number of potential change notifications pushed by Sync Gateway. **Constraints**   This is not necessarily the number of documents pushed, as a given target might already have the change.   Used by versions 1 and 2. |  [optional]
**sgrNumAttachmentsTransferred** | **Integer** | The total number of attachments transferred since replication started. **Deprecated @ 2.8**: used only by inter-sync-gateway replications version 1. |  [optional]
**sgrNumAttachmentBytesTransferred** | **Integer** | The total number of attachment bytes transferred since replication started. **Deprecated @ 2.8**: used only by inter-sync-gateway replications version 1. |  [optional]
**sgrNumDocsFailedToPush** | **Integer** | The total number of documents that failed to be pushed since replication started. Used by versions 1 and 2. |  [optional]
**sgrNumDocsPushed** | **Integer** | The total number of documents that were pushed since replication started. Used by versions 1 and 2. |  [optional]
