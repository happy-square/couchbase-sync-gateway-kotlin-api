# InlineResponse2001SyncHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revs** | **List&lt;String&gt;** | The past revision IDs. |  [optional]
**parents** | [**List&lt;BigDecimal&gt;**](BigDecimal.md) |  |  [optional]
**channels** | [**List&lt;List&lt;String&gt;&gt;**](List.md) | The past channel history. Can contain string arrays, strings, or be null depending on if and how the channels where set. |  [optional]
