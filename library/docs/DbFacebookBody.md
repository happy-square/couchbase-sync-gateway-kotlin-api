# DbFacebookBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | Facebook access token to base the new session on. | 
