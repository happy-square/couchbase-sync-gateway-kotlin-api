# DbConfigDeltaSync

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether delta sync is enabled.  **This is an enterprise-edition feature only** |  [optional]
**revMaxAgeSeconds** | [**BigDecimal**](BigDecimal.md) | The number of seconds deltas for old revisions are available for.  This defaults to 24 hours (in seconds). |  [optional]
