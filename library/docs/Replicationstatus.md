# Replicationstatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replicationId** | **String** | The ID of the replication. | 
**config** | [**UserConfigurableReplicationProperties1**](UserConfigurableReplicationProperties1.md) |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | The status of the replication. |  [optional]
**errorMessage** | **String** | The error message of the replication if an error has occurred. |  [optional]
**docsRead** | **Integer** | The number of documents that have been read (fetched) from the source database. |  [optional]
**docsCheckedPull** | **Integer** |  |  [optional]
**docsPurged** | **Integer** | The number of documents that have been purged. |  [optional]
**rejectedByLocal** | **Integer** | The number of documents that were received by the local but did not get replicated due to getting rejected by the sync function on the local. |  [optional]
**lastSeqPull** | **String** | The last changes sequence number that was pulled from the remote. |  [optional]
**deltasRecv** | **Integer** | The number of deltas that have been received from the remote. |  [optional]
**deltasRequested** | **Integer** |  |  [optional]
**docsWritten** | **Integer** | The number of documents that have been wrote (pushed) to the target database. |  [optional]
**docsCheckedPush** | **Integer** |  |  [optional]
**docsWriteFailures** | **Integer** | The number of documents that have failed to be wrote (pushed) to the target database. There will be no attempt to try to push these docs again. |  [optional]
**docsWriteConflicts** | **Integer** | The number of documents that had a conflict. |  [optional]
**rejectedByRemote** | **Integer** | The number of documents that were received by the remote but did not get replicated due to getting rejected by the sync function on the remote. |  [optional]
**lastSeqPush** | **String** | The last changes sequence number that was pushed to the remote. |  [optional]
**deltasSent** | **Integer** | The number of deltas that have been sent to the remote.  |  [optional]

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
STOPPED | &quot;stopped&quot;
RUNNING | &quot;running&quot;
RECONNECTING | &quot;reconnecting&quot;
RESETTING | &quot;resetting&quot;
ERROR | &quot;error&quot;
STARTING | &quot;starting&quot;
