# DbGoogleBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idToken** | **String** | Google ID token to base the new session on. | 
