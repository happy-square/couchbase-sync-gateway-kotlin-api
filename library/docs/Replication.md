# Replication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replicationId** | **String** | This is the ID of the replication.  When creating a new replication using a POST request, this will be set to a random UUID if not explicitly set.  When the replication ID is specified in the URL, this must be set to the same replication ID if specifying it at all. |  [optional]
**remote** | **String** | This is the endpoint of the database for the remote Sync Gateway that is the subject of this replication&#x27;s &#x60;push&#x60;, &#x60;pull&#x60;, or &#x60;pushAndPull&#x60; action.  Typically this would include the URI, port, and database name. For example, &#x60;http://localhost:4985/db&#x60;.  How this remote is used depends on the &#x60;direction&#x60; of the replication: * &#x60;pull&#x60; - this replicator _pulls_ changes from the &#x60;remote&#x60; * &#x60;push&#x60; - this replicator _pushes_ changes to this &#x60;remote&#x60; * &#x60;pushAndPull&#x60; - this replicator _pushes_ changes to this &#x60;remote&#x60;, while also pulling receiving changes |  [optional]
**username** | **String** | **This has been deprecated in favour of &#x60;remote_username&#x60;.**  This is the username to use to authenticate with the remote.  This can only be used for a pull replication. |  [optional]
**password** | **String** | **This has been deprecated in favour of &#x60;remote_password&#x60;.**  This is the password to use to authenticate with the remote.  This password will be redacted in the replication config.  This can only be used for a pull replication. |  [optional]
**remoteUsername** | **String** | The username to use to authenticate with the remote.  This can only be used for a pull replication. |  [optional]
**remotePassword** | **String** | The password to use to authenticate with the remote.  This password will be redacted in the replication config.  This can only be used for a pull replication. |  [optional]
**direction** | [**DirectionEnum**](#DirectionEnum) | This specifies which direction the replication will be replicating with the &#x60;remote&#x60; replicator.  The directions are: * &#x60;pull&#x60; - changes are pulled from the remote database * &#x60;push&#x60; - changes are pushed to the remote database * &#x60;pushAndPull&#x60; - changes are both push-to and pulled-from the remote database  Replications created prior to Sync Gateway 2.8 derive their &#x60;direction&#x60; from the source/target URL of the replication. | 
**conflictResolutionType** | [**ConflictResolutionTypeEnum**](#ConflictResolutionTypeEnum) | This defines what conflict resolution policy Sync Gateway should use to apply when resolving conflicting revisions.  Changing this is an enterprise-edition only feature.  **Behaviour** * *default* - In priority order, this will cause   - Deletes to always win (the delete with the logest revision history wins if both revisions are deletes)   - The revision with the longest revision history to win. This means the the revision with the most changes and therefore the highest revision ID will win. * *localWins* - This will result in local revisions always being the winner in any conflict. * *remoteWins* - This will result in remote revisions always being the winner in any conflict. * *custom* - This will result in conflicts going through your own custom conflict resolver. You must provide this logic as a Javascript function in the &#x60;custom_conflict_resolver&#x60; parameter. This is an enterprise-edition only feature.   Note: replications created prior to Sync Gateway 2.8 will default to &#x60;default&#x60;. |  [optional]
**customConflictResolver** | **String** | This specifies the Javascript function to use to resolve conflicts between conflicting revisions.   This **must** be used when &#x60;conflict_resolution_type&#x3D;custom&#x60;. This property will be ignored when &#x60;conflict_resolution_type&#x60; is not &#x60;custom&#x60;.  The Javascript function to provide this property should be in backticks (like the sync function). The function takes 1 parameter which is a struct that represents the conflict. This struct has 2 properties: * &#x60;LocalDocument&#x60; - The local document. This contains the document ID under the &#x60;_id&#x60; key. * &#x60;RemoteDocument&#x60; - The remote document The function should return the new documents body. This can be the winning revision (for example, &#x60;return conflict.LocalDocument&#x60;), a new body, or &#x60;nil&#x60; to resolve as a delete.  Example: &#x60;&#x60;&#x60; \&quot;custom_conflict_resolver\&quot;:\\&#x60;  function(conflict) {   console.log(\&quot;Doc ID: \&quot;+conflict.LocalDocument._id);   console.log(\&quot;Full remote doc: \&quot;+JSON.stringify(conflict.RemoteDocument));   return conflict.RemoteDocument;  } \\&#x60; &#x60;&#x60;&#x60;  Using complex &#x60;custom_conflict_resolver&#x60; functions can noticeably degrade performance. Use a built-in resolver whenever possible.  This is an enterprise-edition only feature.  |  [optional]
**purgeOnRemoval** | **Boolean** | Specifies whether to purge a document if the remote user loses access to all of the channels on the document when attempting to pull it from the remote.  If false, documents will not be replicated and not be purged when the user loses access. |  [optional]
**enableDeltaSync** | **Boolean** | This will turn on delta- sync for the replication. This works in conjunction with the database level setting &#x60;delta_sync.enabled&#x60;  If set to true, delta-sync will be used as long as both databases involved in the replication have delta-sync enabled. If a database does not have delta-sync enabled, then the replication will run without delta-sync.  Replications created prior to Sync Gateway 2.8 must have delta-sync disabled.  Enabling this is an enterprise-edition only feature. |  [optional]
**maxBackoffTime** | **Integer** | Specifies the maximum time-period (in minutes) that Sync Gateway will attempt to reconnect to a lost or unreachable remote.  When a disconnection happens, Sync Gateway will do an exponential backoff up to this specified value. When this value is met, it will attempt to reconnect indefinitely every &#x60;max_backoff_time&#x60; minutes.  If this is set to 0, Sync Gateway will do the normal exponential backoff after the disconnect happens but then attempting 10 minutes and stop the replication.  Note: this defaults to 5 minutes for replications created prior to Sync Gateway 2.8. |  [optional]
**initialState** | [**InitialStateEnum**](#InitialStateEnum) | This is what state to start the replication in when creating a new replication.  This allows you to control if the replication starts in a &#x60;stopped&#x60; start or &#x60;running&#x60; state.  Replications prior to Sync Gateway 2.8 will run in the default state &#x60;running&#x60;. |  [optional]
**continuous** | **Boolean** | If true, changes will be immediately synced when they happen. This is known as a continuous replication.  If false, all changes will be synced until they have been processed. The replication will then cease and not process any future changes (unless started again by the user). This is known as a one-shot replication. |  [optional]
**filter** | [**FilterEnum**](#FilterEnum) | This defines whether to filter documents by their channels or not.  If set to &#x60;sync_gateway/bychannel&#x60; then a **pull** replication will be limited to a specific set of channels specified by the &#x60;query_params.channels&#x60; property.  This only can be used with pull replications. |  [optional]
**queryParams** | **List&lt;String&gt;** | This is a set of key/value pairs used in the query string of the replication.  If &#x60;filters&#x3D;sync_gateway/bychannel&#x60; then this can be used to set the channels to filter by in a pull replication. To do this, set the &#x60;channels&#x60; key to a string array of the channels to filter by. For example: &#x60;&#x60;&#x60; \&quot;filter\&quot;:\&quot;sync_gateway/bychannel\&quot;, \&quot;query_params\&quot;: {   \&quot;channels\&quot;:[\&quot;chanUser1\&quot;] }, &#x60;&#x60;&#x60; |  [optional]
**adhoc** | **Boolean** | Set to true to run the replication as an adhoc replication instead of a persistent one.  This means that the replication will only last the period of the replication until the status is changed to &#x60;stopped&#x60; and then it will be removed automatically. It will also be removed if Sync Gateway restarts or if removed due to user action. |  [optional]
**batchSize** | **Integer** | The amount of changes to be sent in one batch of replications. Changing this is an enterprise-edition only feature. |  [optional]
**runAs** | **String** | This is used if you want to specify a user to run the replication as. This means that the replication will only be able to replicate what the user  access to what the user has access to. |  [optional]
**collectionsEnabled** | **Boolean** | If true, the replicator will run with collections, and will replicate all collections, unless otherwise limited by &#x60;keyspace_map&#x60;.  If false, the replicator will only replicate the default collection. |  [optional]
**collectionsLocal** | **List&lt;String&gt;** | Limits the set of collections replicated to those listed in this array.  The replication will use all collections defined on the database if this list is empty. |  [optional]
**collectionsRemote** | **List&lt;String&gt;** | Remaps the local collection name to the one specified in this array when replicating with the remote.  If only a subset of collections need remapping, elements in this array can be specified as &#x60;null&#x60; to preserve the local collection name.  The same index is used for both &#x60;collections_remote&#x60; and &#x60;collections_local&#x60;, and both arrays must be the same length. |  [optional]

<a name="DirectionEnum"></a>
## Enum: DirectionEnum
Name | Value
---- | -----
PUSH | &quot;push&quot;
PULL | &quot;pull&quot;
PUSHANDPULL | &quot;pushAndPull&quot;

<a name="ConflictResolutionTypeEnum"></a>
## Enum: ConflictResolutionTypeEnum
Name | Value
---- | -----
DEFAULT | &quot;default&quot;
REMOTEWINS | &quot;remoteWins&quot;
LOCALWINS | &quot;localWins&quot;
CUSTOM | &quot;custom&quot;

<a name="InitialStateEnum"></a>
## Enum: InitialStateEnum
Name | Value
---- | -----
RUNNING | &quot;running&quot;
STOPPED | &quot;stopped&quot;

<a name="FilterEnum"></a>
## Enum: FilterEnum
Name | Value
---- | -----
SYNC_GATEWAY_BYCHANNEL | &quot;sync_gateway/bychannel&quot;
