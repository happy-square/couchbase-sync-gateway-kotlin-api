# Runtimeconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logging** | [**ConfigLogging**](ConfigLogging.md) |  |  [optional]
**maxConcurrentReplications** | **Integer** | Maximum number of concurrent replication connections allowed. If set to 0 this limit will be ignored. |  [optional]
