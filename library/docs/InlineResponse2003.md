# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**StatusEnum**](#StatusEnum) | The status of sgcollect_info. | 

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
STOPPED | &quot;stopped&quot;
RUNNING | &quot;running&quot;
