# CredentialsConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | Username for authenticating to the bucket |  [optional]
**password** | **String** | Password for authenticating to the bucket. This value is always redacted. |  [optional]
**x509CertPath** | **String** | Cert path (public key) for X.509 bucket auth |  [optional]
**x509KeyPath** | **String** | Key path (private key) for X.509 bucket auth |  [optional]
