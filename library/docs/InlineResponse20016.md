# InlineResponse20016

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **String** | The ID of the document. |  [optional]
**_rev** | **String** | The revision ID of the document. |  [optional]
