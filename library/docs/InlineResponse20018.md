# InlineResponse20018

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keys** | [**List&lt;InlineResponse20018Keys&gt;**](InlineResponse20018Keys.md) |  | 
