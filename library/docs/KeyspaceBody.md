# KeyspaceBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **String** | The ID of the document. |  [optional]
**_rev** | **String** | The revision of the document. |  [optional]
**_exp** | **String** | Expiry time after which the document will be purged. The expiration time is set and managed on the Couchbase Server document. The value can be specified in two ways; in ISO-8601 format, for example the 6th of July 2022 at 17:00 in the BST timezone would be &#x60;2016-07-06T17:00:00+01:00&#x60;; it can also be specified as a numeric Couchbase Server expiry value. Couchbase Server expiries are specified as Unix time, and if the desired TTL is below 30 days then it can also represent an interval in seconds from the current time (for example, a value of 5 will remove the document 5 seconds after it is written to Couchbase Server). The document expiration time is returned in the response of &#x60;GET /{db}/{doc} &#x60; when &#x60;show_exp&#x3D;true&#x60; is included in the query.  As with the existing explicit purge mechanism, this applies only to the local database; it has nothing to do with replication. This expiration time is not propagated when the document is replicated. The purge of the document does not cause it to be deleted on any other database. |  [optional]
**_deleted** | **Boolean** | Whether the document is a tombstone or not. If true, it is a tombstone. |  [optional]
**_revisions** | [**KeyspaceRevisions**](KeyspaceRevisions.md) |  |  [optional]
**_attachments** | [**Map&lt;String, KeyspaceAttachments&gt;**](KeyspaceAttachments.md) |  |  [optional]
