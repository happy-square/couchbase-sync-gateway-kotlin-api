# User1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the user.  User names can only have alphanumeric ASCII characters and underscores. |  [optional]
**password** | **String** | The password of the user.  Mandatory. unless &#x60;allow_empty_password&#x60; is &#x60;true&#x60; in the database configs. |  [optional]
**adminChannels** | **List&lt;String&gt;** | A list of channels to explicitly grant to the user for the default collection. |  [optional]
**allChannels** | **List&lt;String&gt;** | All the channels that the user has been granted access to for the default collection.  Access could have been granted through the sync function, roles, or explicitly on the user under the &#x60;admin_channels&#x60; property. |  [optional]
**collectionAccess** | [**Map&lt;String, Map&lt;String, Object&gt;&gt;**](Map.md) | A set of access grants by scope and collection. |  [optional]
**email** | **String** | The email address of the user. |  [optional]
**disabled** | **Boolean** | If true, the user will not be able to login to the account as it is disabled. |  [optional]
**adminRoles** | **List&lt;String&gt;** | A list of roles to explicitly grant to the user. |  [optional]
**roles** | **List&lt;String&gt;** | All the roles that the user has been granted access to.  Access could have been granted through the sync function, roles_claim, or explicitly on the user under the &#x60;admin_roles&#x60; property. |  [optional]
**jwtRoles** | **List&lt;String&gt;** | The roles that the user has been added to through roles_claim. |  [optional]
**jwtChannels** | **List&lt;String&gt;** | The channels that the user has been granted access to through channels_claim. |  [optional]
**jwtIssuer** | **String** | The issuer of the last JSON Web Token that the user last used to sign in. |  [optional]
**jwtLastUpdated** | [**OffsetDateTime**](OffsetDateTime.md) | The last time that the user&#x27;s JWT roles/channels were updated. |  [optional]
