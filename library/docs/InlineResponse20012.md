# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rows** | [**List&lt;InlineResponse20012Rows&gt;**](InlineResponse20012Rows.md) |  | 
**totalRows** | [**BigDecimal**](BigDecimal.md) |  | 
**updateSeq** | [**BigDecimal**](BigDecimal.md) |  | 
