# UnsupportedApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDbDesignDdoc**](UnsupportedApi.md#deleteDbDesignDdoc) | **DELETE** /{db}/_design/{ddoc} | Delete a design document | Unsupported
[**getDbDesignDdoc**](UnsupportedApi.md#getDbDesignDdoc) | **GET** /{db}/_design/{ddoc} | Get views of a design document | Unsupported
[**getDbDesignDdocViewView**](UnsupportedApi.md#getDbDesignDdocViewView) | **GET** /{db}/_design/{ddoc}/_view/{view} | Query a view on a design document | Unsupported
[**getDbDumpView**](UnsupportedApi.md#getDbDumpView) | **GET** /{db}/_dump/{view} | Dump a view | Unsupported
[**getDbViewView**](UnsupportedApi.md#getDbViewView) | **GET** /{db}/_view/{view} | Query a view on the default design document | Unsupported
[**getKeyspaceDumpchannelChannel**](UnsupportedApi.md#getKeyspaceDumpchannelChannel) | **GET** /{keyspace}/_dumpchannel/{channel} | Dump all the documents in a channel | Unsupported
[**getKeyspaceRevtreeDocid**](UnsupportedApi.md#getKeyspaceRevtreeDocid) | **GET** /{keyspace}/_revtree/{docid} | Revision tree structure in Graphviz Dot format | Unsupported
[**headDbDesignDdoc**](UnsupportedApi.md#headDbDesignDdoc) | **HEAD** /{db}/_design/{ddoc} | Check if view of design document exists | Unsupported
[**postDbFlush**](UnsupportedApi.md#postDbFlush) | **POST** /{db}/_flush | Flush the entire database bucket | Unsupported
[**postDbRepair**](UnsupportedApi.md#postDbRepair) | **POST** /{db}/_repair | Disabled endpoint
[**putDbDesignDdoc**](UnsupportedApi.md#putDbDesignDdoc) | **PUT** /{db}/_design/{ddoc} | Update views of a design document | Unsupported

<a name="deleteDbDesignDdoc"></a>
# **deleteDbDesignDdoc**
> deleteDbDesignDdoc(db, ddoc)

Delete a design document | Unsupported

**This is unsupported**  Delete a design document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String ddoc = "ddoc_example"; // String | The design document name.
try {
    apiInstance.deleteDbDesignDdoc(db, ddoc);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#deleteDbDesignDdoc");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **ddoc** | **String**| The design document name. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbDesignDdoc"></a>
# **getDbDesignDdoc**
> InlineResponse20014 getDbDesignDdoc(db, ddoc)

Get views of a design document | Unsupported

**This is unsupported**  Query a design document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String ddoc = "ddoc_example"; // String | The design document name.
try {
    InlineResponse20014 result = apiInstance.getDbDesignDdoc(db, ddoc);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getDbDesignDdoc");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **ddoc** | **String**| The design document name. |

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbDesignDdocViewView"></a>
# **getDbDesignDdocViewView**
> InlineResponse2008 getDbDesignDdocViewView(db, ddoc, view, inclusiveEnd, descending, includeDocs, reduce, group, skip, limit, groupLevel, startkeyDocid, endkeyDocid, stale, startkey, endkey, key, keys)

Query a view on a design document | Unsupported

**This is unsupported**  Query a view on a design document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String ddoc = "ddoc_example"; // String | The design document name.
String view = "view_example"; // String | The view to target.
Boolean inclusiveEnd = true; // Boolean | Indicates whether the specified end key should be included in the result.
Boolean descending = true; // Boolean | Return documents in descending order.
Boolean includeDocs = true; // Boolean | Only works when using Couchbase Server 3.0 and earlier. Indicates whether to include the full content of the documents in the response.
Boolean reduce = true; // Boolean | Whether to execute a reduce function on the response or not.
Boolean group = true; // Boolean | Group the results using the reduce function to a group or single row.
Integer skip = 56; // Integer | Skip the specified number of documents before starting to return results.
Integer limit = 56; // Integer | Return only the specified number of documents
Integer groupLevel = 56; // Integer | Specify the group level to be used.
String startkeyDocid = "startkeyDocid_example"; // String | Return documents starting with the specified document identifier.
String endkeyDocid = "endkeyDocid_example"; // String | Stop returning records when the specified document identifier is reached.
String stale = "stale_example"; // String | Allow the results from a stale view to be used, without triggering a rebuild of all views within the encompassing design document.
String startkey = "startkey_example"; // String | Return records starting with the specified key.
String endkey = "endkey_example"; // String | Stop returning records when this key is reached.
String key = "key_example"; // String | Return only the document that matches the specified key.
List<String> keys = Arrays.asList("keys_example"); // List<String> | An array of document ID strings to filter by.
try {
    InlineResponse2008 result = apiInstance.getDbDesignDdocViewView(db, ddoc, view, inclusiveEnd, descending, includeDocs, reduce, group, skip, limit, groupLevel, startkeyDocid, endkeyDocid, stale, startkey, endkey, key, keys);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getDbDesignDdocViewView");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **ddoc** | **String**| The design document name. |
 **view** | **String**| The view to target. |
 **inclusiveEnd** | **Boolean**| Indicates whether the specified end key should be included in the result. | [optional]
 **descending** | **Boolean**| Return documents in descending order. | [optional]
 **includeDocs** | **Boolean**| Only works when using Couchbase Server 3.0 and earlier. Indicates whether to include the full content of the documents in the response. | [optional]
 **reduce** | **Boolean**| Whether to execute a reduce function on the response or not. | [optional]
 **group** | **Boolean**| Group the results using the reduce function to a group or single row. | [optional]
 **skip** | **Integer**| Skip the specified number of documents before starting to return results. | [optional]
 **limit** | **Integer**| Return only the specified number of documents | [optional]
 **groupLevel** | **Integer**| Specify the group level to be used. | [optional]
 **startkeyDocid** | **String**| Return documents starting with the specified document identifier. | [optional]
 **endkeyDocid** | **String**| Stop returning records when the specified document identifier is reached. | [optional]
 **stale** | **String**| Allow the results from a stale view to be used, without triggering a rebuild of all views within the encompassing design document. | [optional] [enum: ok, update_after]
 **startkey** | **String**| Return records starting with the specified key. | [optional]
 **endkey** | **String**| Stop returning records when this key is reached. | [optional]
 **key** | **String**| Return only the document that matches the specified key. | [optional]
 **keys** | [**List&lt;String&gt;**](String.md)| An array of document ID strings to filter by. | [optional]

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbDumpView"></a>
# **getDbDumpView**
> String getDbDumpView(db, view)

Dump a view | Unsupported

**This is unsupported**  This queries the view and outputs it as HTML.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String view = "view_example"; // String | The view to target.
try {
    String result = apiInstance.getDbDumpView(db, view);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getDbDumpView");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **view** | **String**| The view to target. |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html, application/json

<a name="getDbViewView"></a>
# **getDbViewView**
> InlineResponse2008 getDbViewView(db, view, inclusiveEnd, descending, includeDocs, reduce, group, skip, limit, groupLevel, startkeyDocid, endkeyDocid, stale, startkey, endkey, key, keys)

Query a view on the default design document | Unsupported

**This is unsupported**  Query a view on the default Sync Gateway design document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String view = "view_example"; // String | The view to target.
Boolean inclusiveEnd = true; // Boolean | Indicates whether the specified end key should be included in the result.
Boolean descending = true; // Boolean | Return documents in descending order.
Boolean includeDocs = true; // Boolean | Only works when using Couchbase Server 3.0 and earlier. Indicates whether to include the full content of the documents in the response.
Boolean reduce = true; // Boolean | Whether to execute a reduce function on the response or not.
Boolean group = true; // Boolean | Group the results using the reduce function to a group or single row.
Integer skip = 56; // Integer | Skip the specified number of documents before starting to return results.
Integer limit = 56; // Integer | Return only the specified number of documents
Integer groupLevel = 56; // Integer | Specify the group level to be used.
String startkeyDocid = "startkeyDocid_example"; // String | Return documents starting with the specified document identifier.
String endkeyDocid = "endkeyDocid_example"; // String | Stop returning records when the specified document identifier is reached.
String stale = "stale_example"; // String | Allow the results from a stale view to be used, without triggering a rebuild of all views within the encompassing design document.
String startkey = "startkey_example"; // String | Return records starting with the specified key.
String endkey = "endkey_example"; // String | Stop returning records when this key is reached.
String key = "key_example"; // String | Return only the document that matches the specified key.
List<String> keys = Arrays.asList("keys_example"); // List<String> | An array of document ID strings to filter by.
try {
    InlineResponse2008 result = apiInstance.getDbViewView(db, view, inclusiveEnd, descending, includeDocs, reduce, group, skip, limit, groupLevel, startkeyDocid, endkeyDocid, stale, startkey, endkey, key, keys);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getDbViewView");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **view** | **String**| The view to target. |
 **inclusiveEnd** | **Boolean**| Indicates whether the specified end key should be included in the result. | [optional]
 **descending** | **Boolean**| Return documents in descending order. | [optional]
 **includeDocs** | **Boolean**| Only works when using Couchbase Server 3.0 and earlier. Indicates whether to include the full content of the documents in the response. | [optional]
 **reduce** | **Boolean**| Whether to execute a reduce function on the response or not. | [optional]
 **group** | **Boolean**| Group the results using the reduce function to a group or single row. | [optional]
 **skip** | **Integer**| Skip the specified number of documents before starting to return results. | [optional]
 **limit** | **Integer**| Return only the specified number of documents | [optional]
 **groupLevel** | **Integer**| Specify the group level to be used. | [optional]
 **startkeyDocid** | **String**| Return documents starting with the specified document identifier. | [optional]
 **endkeyDocid** | **String**| Stop returning records when the specified document identifier is reached. | [optional]
 **stale** | **String**| Allow the results from a stale view to be used, without triggering a rebuild of all views within the encompassing design document. | [optional] [enum: ok, update_after]
 **startkey** | **String**| Return records starting with the specified key. | [optional]
 **endkey** | **String**| Stop returning records when this key is reached. | [optional]
 **key** | **String**| Return only the document that matches the specified key. | [optional]
 **keys** | [**List&lt;String&gt;**](String.md)| An array of document ID strings to filter by. | [optional]

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceDumpchannelChannel"></a>
# **getKeyspaceDumpchannelChannel**
> String getKeyspaceDumpchannelChannel(keyspace, channel, since)

Dump all the documents in a channel | Unsupported

**This is unsupported**  This queries a channel and displays all the document IDs and revisions that are in that channel.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String channel = "channel_example"; // String | The channel to dump all the documents from.
String since = "since_example"; // String | Starts the results from the change immediately after the given sequence ID. Sequence IDs should be considered opaque; they come from the last_seq property of a prior response.
try {
    String result = apiInstance.getKeyspaceDumpchannelChannel(keyspace, channel, since);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getKeyspaceDumpchannelChannel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **channel** | **String**| The channel to dump all the documents from. |
 **since** | **String**| Starts the results from the change immediately after the given sequence ID. Sequence IDs should be considered opaque; they come from the last_seq property of a prior response. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html, application/json

<a name="getKeyspaceRevtreeDocid"></a>
# **getKeyspaceRevtreeDocid**
> String getKeyspaceRevtreeDocid(keyspace, docid)

Revision tree structure in Graphviz Dot format | Unsupported

This returns the Dot syntax of the revision tree for the document so that it can be rendered in to a PNG image using the [Graphviz CLI tool](https://www.graphviz.org/).  To use: 1. Install the Graphviz tool. Using Brew, this can be done by calling &#x60;brew install graphviz&#x60;. 2. Save the response text from this endpoint to a file (for example, &#x60;revtree.dot&#x60;). 3. Render the PNG by calling &#x60;dot -Tpng revtree.dot &gt; revtree.png&#x60;.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only  **Note: This endpoint is useful for debugging purposes only. It is not officially supported.**

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
try {
    String result = apiInstance.getKeyspaceRevtreeDocid(keyspace, docid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#getKeyspaceRevtreeDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbDesignDdoc"></a>
# **headDbDesignDdoc**
> headDbDesignDdoc(db, ddoc)

Check if view of design document exists | Unsupported

**This is unsupported**  Check if a design document can be queried.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String ddoc = "ddoc_example"; // String | The design document name.
try {
    apiInstance.headDbDesignDdoc(db, ddoc);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#headDbDesignDdoc");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **ddoc** | **String**| The design document name. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbFlush"></a>
# **postDbFlush**
> postDbFlush(db)

Flush the entire database bucket | Unsupported

**This is unsupported**  This will purge *all* documents.  The bucket will only be flushed if the unsupported database configuration option &#x60;enable_couchbase_bucket_flush&#x60; is set.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.postDbFlush(db);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#postDbFlush");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbRepair"></a>
# **postDbRepair**
> postDbRepair(db)

Disabled endpoint

This endpoint is disabled.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.postDbRepair(db);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#postDbRepair");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putDbDesignDdoc"></a>
# **putDbDesignDdoc**
> putDbDesignDdoc(db, ddoc, body)

Update views of a design document | Unsupported

**This is unsupported**  Update the views of a design document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.UnsupportedApi;


UnsupportedApi apiInstance = new UnsupportedApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String ddoc = "ddoc_example"; // String | The design document name.
DesignDdocBody body = new DesignDdocBody(); // DesignDdocBody | 
try {
    apiInstance.putDbDesignDdoc(db, ddoc, body);
} catch (ApiException e) {
    System.err.println("Exception when calling UnsupportedApi#putDbDesignDdoc");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **ddoc** | **String**| The design document name. |
 **body** | [**DesignDdocBody**](DesignDdocBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

