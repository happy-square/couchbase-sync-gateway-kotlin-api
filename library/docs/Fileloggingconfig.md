# Fileloggingconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Toggle for this log output |  [optional]
**rotation** | [**Logrotationconfig**](Logrotationconfig.md) |  |  [optional]
**collationBufferSize** | **Integer** | The size of the log collation buffer |  [optional]
