# InlineResponse2009Collections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateSeq** | **Integer** | The last sequence number that was committed to the collection. |  [optional]
