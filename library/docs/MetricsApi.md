# MetricsApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExpvar**](MetricsApi.md#getExpvar) | **GET** /_expvar | Get all Sync Gateway statistics
[**getStats**](MetricsApi.md#getStats) | **GET** /_stats | Get memory statistics

<a name="getExpvar"></a>
# **getExpvar**
> InlineResponse20010 getExpvar()

Get all Sync Gateway statistics

This returns a snapshot of all metrics in Sync Gateway for debugging and monitoring purposes.  This includes per database stats, replication stats, and server stats.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Dev Ops * External Stats Reader

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.MetricsApi;


MetricsApi apiInstance = new MetricsApi();
try {
    InlineResponse20010 result = apiInstance.getExpvar();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetricsApi#getExpvar");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/javascript

<a name="getStats"></a>
# **getStats**
> InlineResponse2002 getStats()

Get memory statistics

This will return the current Sync Gateway nodes memory statistics such as current memory usage.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Dev Ops * External Stats Reader

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.MetricsApi;


MetricsApi apiInstance = new MetricsApi();
try {
    InlineResponse2002 result = apiInstance.getStats();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MetricsApi#getStats");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

