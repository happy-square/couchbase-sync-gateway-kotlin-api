# AuthenticationApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDbOidc**](AuthenticationApi.md#getDbOidc) | **GET** /{db}/_oidc | OpenID Connect authentication initiation via Location header redirect
[**getDbOidcCallback**](AuthenticationApi.md#getDbOidcCallback) | **GET** /{db}/_oidc_callback | OpenID Connect authentication callback
[**getDbOidcChallenge**](AuthenticationApi.md#getDbOidcChallenge) | **GET** /{db}/_oidc_challenge | OpenID Connect authentication initiation via WWW-Authenticate header
[**getDbOidcRefresh**](AuthenticationApi.md#getDbOidcRefresh) | **GET** /{db}/_oidc_refresh | OpenID Connect token refresh
[**getDbOidcTestingAuthenticate**](AuthenticationApi.md#getDbOidcTestingAuthenticate) | **GET** /{db}/_oidc_testing/authenticate | OpenID Connect mock login page handler
[**getDbOidcTestingAuthorize**](AuthenticationApi.md#getDbOidcTestingAuthorize) | **GET** /{db}/_oidc_testing/authorize | OpenID Connect mock login page
[**getDbOidcTestingCerts**](AuthenticationApi.md#getDbOidcTestingCerts) | **GET** /{db}/_oidc_testing/certs | OpenID Connect public certificates for signing keys
[**getDbOidcTestingWellKnownOpenidConfiguration**](AuthenticationApi.md#getDbOidcTestingWellKnownOpenidConfiguration) | **GET** /{db}/_oidc_testing/.well-known/openid-configuration | OpenID Connect mock provider
[**postDbFacebook**](AuthenticationApi.md#postDbFacebook) | **POST** /{db}/_facebook | Create a new Facebook-based session
[**postDbGoogle**](AuthenticationApi.md#postDbGoogle) | **POST** /{db}/_google | Create a new Google-based session
[**postDbOidcTestingAuthenticate**](AuthenticationApi.md#postDbOidcTestingAuthenticate) | **POST** /{db}/_oidc_testing/authenticate | OpenID Connect mock login page handler
[**postDbOidcTestingAuthorize**](AuthenticationApi.md#postDbOidcTestingAuthorize) | **POST** /{db}/_oidc_testing/authorize | OpenID Connect mock login page
[**postDbOidcTestingToken**](AuthenticationApi.md#postDbOidcTestingToken) | **POST** /{db}/_oidc_testing/token | OpenID Connect mock token

<a name="getDbOidc"></a>
# **getDbOidc**
> getDbOidc(db, provider, offline)

OpenID Connect authentication initiation via Location header redirect

Called by clients to initiate the OpenID Connect Authorization Code Flow. Redirects to the OpenID Connect provider if successful. 

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String provider = "provider_example"; // String | The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used.
String offline = "offline_example"; // String | If true, the OpenID Connect provider is requested to confirm with the user the permissions requested and refresh the OIDC token. To do this, access_type=offline and prompt=consent is set on the redirection link.
try {
    apiInstance.getDbOidc(db, provider, offline);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidc");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **provider** | **String**| The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used. | [optional]
 **offline** | **String**| If true, the OpenID Connect provider is requested to confirm with the user the permissions requested and refresh the OIDC token. To do this, access_type&#x3D;offline and prompt&#x3D;consent is set on the redirection link. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcCallback"></a>
# **getDbOidcCallback**
> OpenIDConnectCallbackProperties getDbOidcCallback(db, code, error, provider, state)

OpenID Connect authentication callback

The callback URL that the client is redirected to after authenticating with the OpenID Connect provider.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String code = "code_example"; // String | The OpenID Connect authentication code.
String error = "error_example"; // String | The OpenID Connect error, if any occurred.
String provider = "provider_example"; // String | The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used.
String state = "state_example"; // String | The OpenID Connect state to verify against the state cookie. This is used to prevent cross-site request forgery (CSRF). This is not required if `disable_callback_state=true` for the provider config (NOT recommended).
try {
    OpenIDConnectCallbackProperties result = apiInstance.getDbOidcCallback(db, code, error, provider, state);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcCallback");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **code** | **String**| The OpenID Connect authentication code. |
 **error** | **String**| The OpenID Connect error, if any occurred. | [optional]
 **provider** | **String**| The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used. | [optional]
 **state** | **String**| The OpenID Connect state to verify against the state cookie. This is used to prevent cross-site request forgery (CSRF). This is not required if &#x60;disable_callback_state&#x3D;true&#x60; for the provider config (NOT recommended). | [optional]

### Return type

[**OpenIDConnectCallbackProperties**](OpenIDConnectCallbackProperties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcChallenge"></a>
# **getDbOidcChallenge**
> getDbOidcChallenge(db, provider, offline)

OpenID Connect authentication initiation via WWW-Authenticate header

Called by clients to initiate the OpenID Connect Authorization Code Flow. This will establish a connection with the provider, then put the redirect URL in the &#x60;WWW-Authenticate&#x60; header.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String provider = "provider_example"; // String | The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used.
String offline = "offline_example"; // String | If true, the OpenID Connect provider is requested to confirm with the user the permissions requested and refresh the OIDC token. To do this, access_type=offline and prompt=consent is set on the redirection link.
try {
    apiInstance.getDbOidcChallenge(db, provider, offline);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcChallenge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **provider** | **String**| The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used. | [optional]
 **offline** | **String**| If true, the OpenID Connect provider is requested to confirm with the user the permissions requested and refresh the OIDC token. To do this, access_type&#x3D;offline and prompt&#x3D;consent is set on the redirection link. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcRefresh"></a>
# **getDbOidcRefresh**
> OpenIDConnectCallbackProperties getDbOidcRefresh(db, refreshToken, provider)

OpenID Connect token refresh

Refresh the OpenID Connect token based on the provided refresh token.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String refreshToken = "refreshToken_example"; // String | The OpenID Connect refresh token.
String provider = "provider_example"; // String | The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used.
try {
    OpenIDConnectCallbackProperties result = apiInstance.getDbOidcRefresh(db, refreshToken, provider);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcRefresh");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **refreshToken** | **String**| The OpenID Connect refresh token. |
 **provider** | **String**| The OpenID Connect provider to use for authentication.  The list of providers are defined in the Sync Gateway config. If left empty, the default provider will be used. | [optional]

### Return type

[**OpenIDConnectCallbackProperties**](OpenIDConnectCallbackProperties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcTestingAuthenticate"></a>
# **getDbOidcTestingAuthenticate**
> getDbOidcTestingAuthenticate(db, scope, username, tokenttl, identityTokenFormats, authenticated, redirectUri)

OpenID Connect mock login page handler

Used to handle the login page displayed for the &#x60;GET /{db}/_oidc_testing/authorize&#x60; endpoint.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String scope = "scope_example"; // String | The OpenID Connect authentication scope.
String username = "username_example"; // String | 
Integer tokenttl = 56; // Integer | 
String identityTokenFormats = "identityTokenFormats_example"; // String | 
String authenticated = "authenticated_example"; // String | 
String redirectUri = "redirectUri_example"; // String | The Sync Gateway OpenID Connect callback URL.
try {
    apiInstance.getDbOidcTestingAuthenticate(db, scope, username, tokenttl, identityTokenFormats, authenticated, redirectUri);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcTestingAuthenticate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **scope** | **String**| The OpenID Connect authentication scope. |
 **username** | **String**|  |
 **tokenttl** | **Integer**|  |
 **identityTokenFormats** | **String**|  |
 **authenticated** | **String**|  |
 **redirectUri** | **String**| The Sync Gateway OpenID Connect callback URL. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcTestingAuthorize"></a>
# **getDbOidcTestingAuthorize**
> getDbOidcTestingAuthorize(db, scope)

OpenID Connect mock login page

Show a mock OpenID Connect login page for the client to log in to.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String scope = "scope_example"; // String | The OpenID Connect authentication scope.
try {
    apiInstance.getDbOidcTestingAuthorize(db, scope);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcTestingAuthorize");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **scope** | **String**| The OpenID Connect authentication scope. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcTestingCerts"></a>
# **getDbOidcTestingCerts**
> InlineResponse20018 getDbOidcTestingCerts(db)

OpenID Connect public certificates for signing keys

Return a mock OpenID Connect public key to be used as signing keys.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    InlineResponse20018 result = apiInstance.getDbOidcTestingCerts(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcTestingCerts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**InlineResponse20018**](InlineResponse20018.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbOidcTestingWellKnownOpenidConfiguration"></a>
# **getDbOidcTestingWellKnownOpenidConfiguration**
> InlineResponse20017 getDbOidcTestingWellKnownOpenidConfiguration(db)

OpenID Connect mock provider

Mock an OpenID Connect provider response for testing purposes. This returns a response that is the same structure as what Sync Gateway expects from an OIDC provider after initiating OIDC authentication.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    InlineResponse20017 result = apiInstance.getDbOidcTestingWellKnownOpenidConfiguration(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#getDbOidcTestingWellKnownOpenidConfiguration");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**InlineResponse20017**](InlineResponse20017.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbFacebook"></a>
# **postDbFacebook**
> postDbFacebook(db, body)

Create a new Facebook-based session

Creates a new session based on a Facebook user. On a successful session creation, a session cookie is stored to keep the user authenticated for future API calls.  If CORS is enabled, the origin must match an allowed login origin otherwise an error will be returned.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
DbFacebookBody body = new DbFacebookBody(); // DbFacebookBody | 
try {
    apiInstance.postDbFacebook(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#postDbFacebook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**DbFacebookBody**](DbFacebookBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDbGoogle"></a>
# **postDbGoogle**
> postDbGoogle(db, body)

Create a new Google-based session

Creates a new session based on a Google user. On a successful session creation, a session cookie is stored to keep the user authenticated for future API calls.  If CORS is enabled, the origin must match an allowed login origin otherwise an error will be returned.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
DbGoogleBody body = new DbGoogleBody(); // DbGoogleBody | 
try {
    apiInstance.postDbGoogle(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#postDbGoogle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**DbGoogleBody**](DbGoogleBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDbOidcTestingAuthenticate"></a>
# **postDbOidcTestingAuthenticate**
> postDbOidcTestingAuthenticate(scope, db, body, redirectUri)

OpenID Connect mock login page handler

Used to handle the login page displayed for the &#x60;GET /{db}/_oidc_testing/authorize&#x60; endpoint.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String scope = "scope_example"; // String | The OpenID Connect authentication scope.
String db = "db_example"; // String | The name of the database to run the operation against.
OidcTestingAuthenticateBody body = new OidcTestingAuthenticateBody(); // OidcTestingAuthenticateBody | Properties passed from the OpenID Connect mock login page to the handler
String redirectUri = "redirectUri_example"; // String | The Sync Gateway OpenID Connect callback URL.
try {
    apiInstance.postDbOidcTestingAuthenticate(scope, db, body, redirectUri);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#postDbOidcTestingAuthenticate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scope** | **String**| The OpenID Connect authentication scope. |
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**OidcTestingAuthenticateBody**](OidcTestingAuthenticateBody.md)| Properties passed from the OpenID Connect mock login page to the handler | [optional]
 **redirectUri** | **String**| The Sync Gateway OpenID Connect callback URL. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDbOidcTestingAuthorize"></a>
# **postDbOidcTestingAuthorize**
> postDbOidcTestingAuthorize(db, scope)

OpenID Connect mock login page

Show a mock OpenID Connect login page for the client to log in to.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String scope = "scope_example"; // String | The OpenID Connect authentication scope.
try {
    apiInstance.postDbOidcTestingAuthorize(db, scope);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#postDbOidcTestingAuthorize");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **scope** | **String**| The OpenID Connect authentication scope. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbOidcTestingToken"></a>
# **postDbOidcTestingToken**
> OIDCtoken postDbOidcTestingToken(db, body)

OpenID Connect mock token

Return a mock OpenID Connect token for the OIDC authentication flow.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
OidcTestingTokenBody body = new OidcTestingTokenBody(); // OidcTestingTokenBody | 
try {
    OIDCtoken result = apiInstance.postDbOidcTestingToken(db, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#postDbOidcTestingToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**OidcTestingTokenBody**](OidcTestingTokenBody.md)|  | [optional]

### Return type

[**OIDCtoken**](OIDCtoken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

