# StatusDatabases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seq** | [**BigDecimal**](BigDecimal.md) | The latest sequence number in the database. |  [optional]
**serverUuid** | **String** | The server unique identifier. |  [optional]
**state** | [**StateEnum**](#StateEnum) | Whether the database is online or offline. |  [optional]
**replicationStatus** | [**List&lt;Replicationstatus&gt;**](Replicationstatus.md) |  |  [optional]
**cluster** | [**StatusCluster**](StatusCluster.md) |  |  [optional]

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
ONLINE | &quot;Online&quot;
OFFLINE | &quot;Offline&quot;
