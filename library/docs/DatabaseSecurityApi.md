# DatabaseSecurityApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDbRoleName**](DatabaseSecurityApi.md#deleteDbRoleName) | **DELETE** /{db}/_role/{name} | Delete a role
[**deleteDbUserName**](DatabaseSecurityApi.md#deleteDbUserName) | **DELETE** /{db}/_user/{name} | Delete a user
[**getDbRole**](DatabaseSecurityApi.md#getDbRole) | **GET** /{db}/_role/ | Get all names of the roles
[**getDbRoleName**](DatabaseSecurityApi.md#getDbRoleName) | **GET** /{db}/_role/{name} | Get a role
[**getDbUser**](DatabaseSecurityApi.md#getDbUser) | **GET** /{db}/_user/ | Get all the names of the users
[**getDbUserName**](DatabaseSecurityApi.md#getDbUserName) | **GET** /{db}/_user/{name} | Get a user
[**headDbRole**](DatabaseSecurityApi.md#headDbRole) | **HEAD** /{db}/_role/ | /{db}/_role/
[**headDbRoleName**](DatabaseSecurityApi.md#headDbRoleName) | **HEAD** /{db}/_role/{name} | Check if role exists
[**headDbUser**](DatabaseSecurityApi.md#headDbUser) | **HEAD** /{db}/_user/ | /{db}/_user/
[**headDbUserName**](DatabaseSecurityApi.md#headDbUserName) | **HEAD** /{db}/_user/{name} | Check if user exists
[**postDbRole**](DatabaseSecurityApi.md#postDbRole) | **POST** /{db}/_role/ | Create a new role
[**postDbUser**](DatabaseSecurityApi.md#postDbUser) | **POST** /{db}/_user/ | Create a new user
[**putDbRoleName**](DatabaseSecurityApi.md#putDbRoleName) | **PUT** /{db}/_role/{name} | Upsert a role
[**putDbUserName**](DatabaseSecurityApi.md#putDbUserName) | **PUT** /{db}/_user/{name} | Upsert a user

<a name="deleteDbRoleName"></a>
# **deleteDbRoleName**
> deleteDbRoleName(db, name)

Delete a role

Delete a role from the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the role.
try {
    apiInstance.deleteDbRoleName(db, name);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#deleteDbRoleName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the role. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteDbUserName"></a>
# **deleteDbUserName**
> deleteDbUserName(db, name)

Delete a user

Delete a user from the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
try {
    apiInstance.deleteDbUserName(db, name);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#deleteDbUserName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbRole"></a>
# **getDbRole**
> List&lt;String&gt; getDbRole(db, deleted)

Get all names of the roles

Retrieves all the roles that are in the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Boolean deleted = false; // Boolean | Indicates that roles marked as deleted should be included in the result.
try {
    List<String> result = apiInstance.getDbRole(db, deleted);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#getDbRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **deleted** | **Boolean**| Indicates that roles marked as deleted should be included in the result. | [optional] [default to false]

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbRoleName"></a>
# **getDbRoleName**
> Role1 getDbRoleName(db, name)

Get a role

Retrieve a single roles properties.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the role.
try {
    Role1 result = apiInstance.getDbRoleName(db, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#getDbRoleName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the role. |

### Return type

[**Role1**](Role1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbUser"></a>
# **getDbUser**
> List&lt;String&gt; getDbUser(db, nameOnly, limit)

Get all the names of the users

Retrieves all the names of the users that are in the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Boolean nameOnly = true; // Boolean | Whether to return user names only, or more detailed information for each user.
Integer limit = 56; // Integer | How many results to return. Using a value of `0` results in no limit.
try {
    List<String> result = apiInstance.getDbUser(db, nameOnly, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#getDbUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **nameOnly** | **Boolean**| Whether to return user names only, or more detailed information for each user. | [optional] [default to true]
 **limit** | **Integer**| How many results to return. Using a value of &#x60;0&#x60; results in no limit. | [optional]

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbUserName"></a>
# **getDbUserName**
> User1 getDbUserName(db, name)

Get a user

Retrieve a single users information.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
try {
    User1 result = apiInstance.getDbUserName(db, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#getDbUserName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |

### Return type

[**User1**](User1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbRole"></a>
# **headDbRole**
> headDbRole(db)

/{db}/_role/

Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDbRole(db);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#headDbRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbRoleName"></a>
# **headDbRoleName**
> headDbRoleName(db, name)

Check if role exists

Check if the role exists by checking the status code.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the role.
try {
    apiInstance.headDbRoleName(db, name);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#headDbRoleName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the role. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbUser"></a>
# **headDbUser**
> headDbUser(db)

/{db}/_user/

Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDbUser(db);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#headDbUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbUserName"></a>
# **headDbUserName**
> headDbUserName(db, name)

Check if user exists

Check if the user exists by checking the status code.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
try {
    apiInstance.headDbUserName(db, name);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#headDbUserName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postDbRole"></a>
# **postDbRole**
> postDbRole(db, body)

Create a new role

Create a new role using the request body to specify the properties on the role.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Role1 body = new Role1(); // Role1 | Properties associated with a role
try {
    apiInstance.postDbRole(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#postDbRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**Role1**](Role1.md)| Properties associated with a role | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postDbUser"></a>
# **postDbUser**
> postDbUser(db, body)

Create a new user

Create a new user using the request body to specify the properties on the user.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
User1 body = new User1(); // User1 | Properties associated with a user
try {
    apiInstance.postDbUser(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#postDbUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**User1**](User1.md)| Properties associated with a user | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDbRoleName"></a>
# **putDbRoleName**
> putDbRoleName(db, name, body)

Upsert a role

If the role does not exist, create a new role otherwise update the existing role.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the role.
Role2 body = new Role2(); // Role2 | Properties associated with a role
try {
    apiInstance.putDbRoleName(db, name, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#putDbRoleName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the role. |
 **body** | [**Role2**](Role2.md)| Properties associated with a role | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDbUserName"></a>
# **putDbUserName**
> putDbUserName(db, name, body)

Upsert a user

If the user does not exist, create a new user otherwise update the existing user.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseSecurityApi;


DatabaseSecurityApi apiInstance = new DatabaseSecurityApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
User2 body = new User2(); // User2 | Properties associated with a user
try {
    apiInstance.putDbUserName(db, name, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseSecurityApi#putDbUserName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |
 **body** | [**User2**](User2.md)| Properties associated with a user | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

