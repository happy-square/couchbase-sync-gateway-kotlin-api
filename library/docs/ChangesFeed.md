# ChangesFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List&lt;ChangesfeedResults&gt;**](ChangesfeedResults.md) |  |  [optional]
**lastSeq** | **String** | The last change sequence number. |  [optional]
