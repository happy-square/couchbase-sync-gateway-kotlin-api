# ChangesfeedChanges

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rev** | **String** | The new revision that was caused by that change. |  [optional]
