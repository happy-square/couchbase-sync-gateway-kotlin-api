# ChangesfeedResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seq** | [**BigDecimal**](BigDecimal.md) | The change sequence number. |  [optional]
**id** | **String** | The document ID the change happened on. |  [optional]
**changes** | [**List&lt;ChangesfeedChanges&gt;**](ChangesfeedChanges.md) | List of document leafs with each leaf containing only a &#x60;rev&#x60; field. |  [optional]
