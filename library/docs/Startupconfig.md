# Startupconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bootstrap** | [**StartupconfigBootstrap**](StartupconfigBootstrap.md) |  |  [optional]
**api** | [**StartupconfigApi**](StartupconfigApi.md) |  |  [optional]
**logging** | [**ConfigLogging**](ConfigLogging.md) |  |  [optional]
**auth** | [**StartupconfigAuth**](StartupconfigAuth.md) |  |  [optional]
**replicator** | [**StartupconfigReplicator**](StartupconfigReplicator.md) |  |  [optional]
**unsupported** | [**StartupconfigUnsupported**](StartupconfigUnsupported.md) |  |  [optional]
**databaseCredentials** | [**Map&lt;String, CredentialsConfig&gt;**](CredentialsConfig.md) | A map of database name to credentials, that can be used instead of the bootstrap ones. |  [optional]
**bucketCredentials** | [**Map&lt;String, CredentialsConfig&gt;**](CredentialsConfig.md) | A map of bucket names to credentials, that can be used instead of the bootstrap ones. |  [optional]
**maxFileDescriptors** | [**BigDecimal**](BigDecimal.md) | Max of open file descriptors (RLIMIT_NOFILE) |  [optional]
**couchbaseKeepaliveInterval** | **Integer** | TCP keep-alive interval between SG and Couchbase server |  [optional]
