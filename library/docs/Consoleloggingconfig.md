# Consoleloggingconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logLevel** | [**LogLevelEnum**](#LogLevelEnum) | Log Level for the console output |  [optional]
**logKeys** | **List&lt;String&gt;** | Log Keys for the console output |  [optional]
**colorEnabled** | **Boolean** | Log with color for the console output |  [optional]
**fileOutput** | **String** | Override the default stderr output, and write to the file specified instead |  [optional]
**enabled** | **Boolean** | Toggle for this log output |  [optional]
**rotation** | [**Logrotationconfig**](Logrotationconfig.md) |  |  [optional]
**collationBufferSize** | **Integer** | The size of the log collation buffer. |  [optional]

<a name="LogLevelEnum"></a>
## Enum: LogLevelEnum
Name | Value
---- | -----
NONE | &quot;none&quot;
ERROR | &quot;error&quot;
WARN | &quot;warn&quot;
INFO | &quot;info&quot;
DEBUG | &quot;debug&quot;
TRACE | &quot;trace&quot;
