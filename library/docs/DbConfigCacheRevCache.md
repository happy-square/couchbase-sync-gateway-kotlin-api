# DbConfigCacheRevCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **String** | The maximum number of revisions that can be stored in the revision cache. |  [optional]
**shardCount** | **String** | The number of shards the revision cache should be split into. |  [optional]
