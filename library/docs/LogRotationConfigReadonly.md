# LogRotationConfigReadonly

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxSize** | **Integer** | The maximum size in MB of the log file before it gets rotated. |  [optional]
**maxAge** | **Integer** | The maximum number of days to retain old log files. |  [optional]
**localtime** | **Boolean** | If true, it uses the computer&#x27;s local time to format the backup timestamp. |  [optional]
**rotatedLogsSize** | **Integer** | Max Size (in mb) of log files before deletion |  [optional]
