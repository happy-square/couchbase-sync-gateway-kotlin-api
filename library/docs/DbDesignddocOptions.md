# DbDesignddocOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localSeq** | **String** |  |  [optional]
**includeDesign** | **String** |  |  [optional]
**raw** | **String** |  |  [optional]
**indexXattrOnDeletedDocs** | **String** |  |  [optional]
