# Compactstatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | The status of the current operation. | 
**startTime** | **String** | The ISO-8601 date and time the compact operation was started. | 
**lastError** | **String** | The last error that occurred in the compact operation (if any). | 
**docsPurged** | **String** | **Applicable to tombstone compaction only**  This is the amount of documents that have been purged so far. |  [optional]
**markedAttachments** | **String** | **Applicable to attachment compaction only**  This is the number of references there are to legacy attachments. |  [optional]
**purgedAttachments** | **String** | **Applicable to attachment compaction only**  This is the amount of attachments that have been purged so far. |  [optional]
**compactId** | **String** | **Applicable to attachment compaction only**  This is the ID of the compaction. |  [optional]
**phase** | **String** | **Applicable to attachment compaction only**  This indicates the current phase of running attachment compact processes.  For failed processes, this indicates the phase at which a compact_id restart will commence (where relevant). |  [optional]
**dryRun** | [**DryRunEnum**](#DryRunEnum) | **Applicable to attachment compaction only**  |  [optional]

<a name="DryRunEnum"></a>
## Enum: DryRunEnum
Name | Value
---- | -----
MARK | &quot;mark&quot;
SWEEP | &quot;sweep&quot;
CLEANUP | &quot;cleanup&quot;
