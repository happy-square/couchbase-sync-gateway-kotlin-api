# Scopes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | [**Map&lt;String, CollectionConfig&gt;**](CollectionConfig.md) | An object keyed by collection name containing config for the specific collection. |  [optional]
