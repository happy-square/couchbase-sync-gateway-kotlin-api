# StartupconfigUnsupportedHttp2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether HTTP2 support is enabled |  [optional]
