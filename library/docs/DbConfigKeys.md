# DbConfigKeys

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kty** | [**KtyEnum**](#KtyEnum) | The cryptographic algorithm family used with the key, such as \&quot;RSA\&quot; or \&quot;EC\&quot; |  [optional]
**use** | [**UseEnum**](#UseEnum) | The intended use of the public key. Only &#x27;sig&#x27; is accepted. |  [optional]
**alg** | **String** | The algorithm intended for use with the key. |  [optional]
**kid** | **String** | The Key ID, used to identify the key to use. |  [optional]
**crv** | [**CrvEnum**](#CrvEnum) | For Elliptic Curve keys, the name of the curve to use. |  [optional]
**x** | **String** | For Elliptic Curve keys, the X coordinate of the point, as a base64url string. |  [optional]
**y** | **String** | For Elliptic Curve keys, the Y coordinate of the point, as a base64url string. |  [optional]
**n** | **String** | For RSA keys, the modulus value of the key, as a Base64urlUInt-encoded value. |  [optional]
**e** | **String** | For RSA keys, the exponent of the public key, as a Base64urlUInt-encoded value. |  [optional]

<a name="KtyEnum"></a>
## Enum: KtyEnum
Name | Value
---- | -----
RSA | &quot;RSA&quot;
EC | &quot;EC&quot;

<a name="UseEnum"></a>
## Enum: UseEnum
Name | Value
---- | -----
SIG | &quot;sig&quot;

<a name="CrvEnum"></a>
## Enum: CrvEnum
Name | Value
---- | -----
_256 | &quot;P-256&quot;
_384 | &quot;P-384&quot;
_521 | &quot;P-521&quot;
