# InlineResponse20011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ADMIN** | **Boolean** | &#x60;true&#x60; if the request is from the Admin API - otherwise omitted. |  [optional]
**couchdb** | **String** | CouchDB welcome | 
**vendor** | [**NodeInfoVendor**](NodeInfoVendor.md) |  | 
**version** | **String** | Product version, including the build number and edition (i.e. &#x60;EE&#x60; or &#x60;CE&#x60;) Omitted if &#x60;api.hide_product_version&#x3D;true&#x60; |  [optional]
**persistentConfig** | **Boolean** | Indication for whether sync gateway is running in persistent config mode or legacy config mode. &#x60;true&#x60; if the sync gateway node is running in persistent config mode. |  [optional]
