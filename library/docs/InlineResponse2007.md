# InlineResponse2007

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purged** | [**Map&lt;String, List&lt;String&gt;&gt;**](#Map&lt;String, List&lt;String&gt;&gt;) |  | 

<a name="Map<String, List<String>>"></a>
## Enum: Map&lt;String, List&lt;String&gt;&gt;
Name | Value
---- | -----
STAR | &quot;*&quot;
