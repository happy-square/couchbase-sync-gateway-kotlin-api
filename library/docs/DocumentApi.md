# DocumentApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteKeyspaceDocid**](DocumentApi.md#deleteKeyspaceDocid) | **DELETE** /{keyspace}/{docid} | Delete a document
[**deleteKeyspaceDocidAttach**](DocumentApi.md#deleteKeyspaceDocidAttach) | **DELETE** /{keyspace}/{docid}/{attach} | Delete an attachment on a document
[**deleteKeyspaceLocalDocid**](DocumentApi.md#deleteKeyspaceLocalDocid) | **DELETE** /{keyspace}/_local/{docid} | Delete a local document
[**getKeyspaceAllDocs**](DocumentApi.md#getKeyspaceAllDocs) | **GET** /{keyspace}/_all_docs | Gets all the documents in the database with the given parameters
[**getKeyspaceDocid**](DocumentApi.md#getKeyspaceDocid) | **GET** /{keyspace}/{docid} | Get a document
[**getKeyspaceDocidAttach**](DocumentApi.md#getKeyspaceDocidAttach) | **GET** /{keyspace}/{docid}/{attach} | Get an attachment from a document
[**getKeyspaceLocalDocid**](DocumentApi.md#getKeyspaceLocalDocid) | **GET** /{keyspace}/_local/{docid} | Get local document
[**getKeyspaceRawDocid**](DocumentApi.md#getKeyspaceRawDocid) | **GET** /{keyspace}/_raw/{docid} | Get a document with the corresponding metadata
[**headKeyspaceAllDocs**](DocumentApi.md#headKeyspaceAllDocs) | **HEAD** /{keyspace}/_all_docs | /{db}/_all_docs
[**headKeyspaceDocid**](DocumentApi.md#headKeyspaceDocid) | **HEAD** /{keyspace}/{docid} | Check if a document exists
[**headKeyspaceDocidAttach**](DocumentApi.md#headKeyspaceDocidAttach) | **HEAD** /{keyspace}/{docid}/{attach} | Check if attachment exists
[**headKeyspaceLocalDocid**](DocumentApi.md#headKeyspaceLocalDocid) | **HEAD** /{keyspace}/_local/{docid} | Check if local document exists
[**headKeyspaceRawDocid**](DocumentApi.md#headKeyspaceRawDocid) | **HEAD** /{keyspace}/_raw/{docid} | /{keyspace}/_raw/{docid}
[**postKeyspace**](DocumentApi.md#postKeyspace) | **POST** /{keyspace}/ | Create a new document
[**postKeyspaceAllDocs**](DocumentApi.md#postKeyspaceAllDocs) | **POST** /{keyspace}/_all_docs | Get all the documents in the database using a built-in view
[**postKeyspaceBulkDocs**](DocumentApi.md#postKeyspaceBulkDocs) | **POST** /{keyspace}/_bulk_docs | Bulk document operations
[**postKeyspaceBulkGet**](DocumentApi.md#postKeyspaceBulkGet) | **POST** /{keyspace}/_bulk_get | Get multiple documents in a MIME multipart response
[**postKeyspacePurge**](DocumentApi.md#postKeyspacePurge) | **POST** /{keyspace}/_purge | Purge a document
[**putKeyspaceDocid**](DocumentApi.md#putKeyspaceDocid) | **PUT** /{keyspace}/{docid} | Upsert a document
[**putKeyspaceDocidAttach**](DocumentApi.md#putKeyspaceDocidAttach) | **PUT** /{keyspace}/{docid}/{attach} | Create or update an attachment on a document
[**putKeyspaceLocalDocid**](DocumentApi.md#putKeyspaceLocalDocid) | **PUT** /{keyspace}/_local/{docid} | Upsert a local document

<a name="deleteKeyspaceDocid"></a>
# **deleteKeyspaceDocid**
> Newrevision deleteKeyspaceDocid(keyspace, docid, rev, ifMatch)

Delete a document

Delete a document from the database. A new revision is created so the database can track the deletion in synchronized copies.  A revision ID either in the header or on the query parameters is required.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String rev = "rev_example"; // String | The document revision to target.
String ifMatch = "ifMatch_example"; // String | The revision ID to target.
try {
    Newrevision result = apiInstance.deleteKeyspaceDocid(keyspace, docid, rev, ifMatch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#deleteKeyspaceDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **rev** | **String**| The document revision to target. | [optional]
 **ifMatch** | **String**| The revision ID to target. | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteKeyspaceDocidAttach"></a>
# **deleteKeyspaceDocidAttach**
> Newrevision deleteKeyspaceDocidAttach(keyspace, docid, attach, rev, ifMatch)

Delete an attachment on a document

This request deletes an attachment associated with the document.  If the attachment exists, the attachment will be removed from the document.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String attach = "attach_example"; // String | The attachment name. This value must be URL encoded. For example, if the attachment name is `blob_/avatar`, the path component passed to the URL should be `blob_%2Favatar` (tested with [URLEncoder](https://www.urlencoder.org/)).
String rev = "rev_example"; // String | The existing document revision ID to modify.
String ifMatch = "ifMatch_example"; // String | An alternative way of specifying the document revision ID.
try {
    Newrevision result = apiInstance.deleteKeyspaceDocidAttach(keyspace, docid, attach, rev, ifMatch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#deleteKeyspaceDocidAttach");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **attach** | **String**| The attachment name. This value must be URL encoded. For example, if the attachment name is &#x60;blob_/avatar&#x60;, the path component passed to the URL should be &#x60;blob_%2Favatar&#x60; (tested with [URLEncoder](https://www.urlencoder.org/)). |
 **rev** | **String**| The existing document revision ID to modify. | [optional]
 **ifMatch** | **String**| An alternative way of specifying the document revision ID. | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteKeyspaceLocalDocid"></a>
# **deleteKeyspaceLocalDocid**
> deleteKeyspaceLocalDocid(keyspace, docid, rev)

Delete a local document

This request deletes a local document.  Local document IDs begin with &#x60;_local/&#x60;. Local documents are not replicated or indexed, don&#x27;t support attachments, and don&#x27;t save revision histories. In practice they are almost only used by Couchbase Lite&#x27;s replicator, as a place to store replication checkpoint data.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The name of the local document ID excluding the `_local/` prefix.
String rev = "rev_example"; // String | The revision ID of the revision to delete.
try {
    apiInstance.deleteKeyspaceLocalDocid(keyspace, docid, rev);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#deleteKeyspaceLocalDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The name of the local document ID excluding the &#x60;_local/&#x60; prefix. |
 **rev** | **String**| The revision ID of the revision to delete. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceAllDocs"></a>
# **getKeyspaceAllDocs**
> InlineResponse20012 getKeyspaceAllDocs(keyspace, includeDocs, channels, access, revs, updateSeq, keys, startkey, endkey, limit)

Gets all the documents in the database with the given parameters

Returns all documents in the databased based on the specified parameters.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String includeDocs = "includeDocs_example"; // String | Include the body associated with each document.
String channels = "channels_example"; // String | Include the channels each document is part of that the calling user also has access too.
String access = "access_example"; // String | Include what user/roles that each document grants access too.
String revs = "revs_example"; // String | Include all the revisions for each document under the `_revisions` property.
String updateSeq = "updateSeq_example"; // String | Include the document sequence number `update_seq` property for each document.
List<String> keys = Arrays.asList("keys_example"); // List<String> | An array of document ID strings to filter by.
String startkey = "startkey_example"; // String | Return records starting with the specified key.
String endkey = "endkey_example"; // String | Stop returning records when this key is reached.
BigDecimal limit = new BigDecimal(); // BigDecimal | This limits the number of result rows returned. Using a value of `0` has the same effect as the value `1`.
try {
    InlineResponse20012 result = apiInstance.getKeyspaceAllDocs(keyspace, includeDocs, channels, access, revs, updateSeq, keys, startkey, endkey, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#getKeyspaceAllDocs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **includeDocs** | **String**| Include the body associated with each document. | [optional]
 **channels** | **String**| Include the channels each document is part of that the calling user also has access too. | [optional]
 **access** | **String**| Include what user/roles that each document grants access too. | [optional]
 **revs** | **String**| Include all the revisions for each document under the &#x60;_revisions&#x60; property. | [optional]
 **updateSeq** | **String**| Include the document sequence number &#x60;update_seq&#x60; property for each document. | [optional]
 **keys** | [**List&lt;String&gt;**](String.md)| An array of document ID strings to filter by. | [optional]
 **startkey** | **String**| Return records starting with the specified key. | [optional]
 **endkey** | **String**| Stop returning records when this key is reached. | [optional]
 **limit** | **BigDecimal**| This limits the number of result rows returned. Using a value of &#x60;0&#x60; has the same effect as the value &#x60;1&#x60;. | [optional]

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceDocid"></a>
# **getKeyspaceDocid**
> InlineResponse20016 getKeyspaceDocid(keyspace, docid, rev, openRevs, showExp, revsFrom, attsSince, revsLimit, attachments, replicator2)

Get a document

Retrieve a document from the database by its doc ID.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String rev = "rev_example"; // String | The document revision to target.
List<String> openRevs = Arrays.asList("openRevs_example"); // List<String> | Option to fetch specified revisions of the document. The value can be all to fetch all leaf revisions or an array of revision numbers (i.e. open_revs=[\"rev1\", \"rev2\"]). Only leaf revision bodies that haven't been pruned are guaranteed to be returned. If this option is specified the response will be in multipart format. Use the `Accept: application/json` request header to get the result as a JSON object.
Boolean showExp = true; // Boolean | Whether to show the expiry property (`_exp`) in the response.
List<String> revsFrom = Arrays.asList("revsFrom_example"); // List<String> | Trim the revision history to stop at the first revision in the provided list. If no match is found, the revisions will be trimmed to the `revs_limit`.
List<String> attsSince = Arrays.asList("attsSince_example"); // List<String> | Include attachments only since specified revisions. Excludes the attachments for the specified revisions. Only gets used if `attachments=true`.
Integer revsLimit = 56; // Integer | Maximum amount of revisions to return for each document.
Boolean attachments = true; // Boolean | Include attachment bodies in response.
Boolean replicator2 = true; // Boolean | Returns the document with the required properties for replication. This is an enterprise-edition only feature.
try {
    InlineResponse20016 result = apiInstance.getKeyspaceDocid(keyspace, docid, rev, openRevs, showExp, revsFrom, attsSince, revsLimit, attachments, replicator2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#getKeyspaceDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **rev** | **String**| The document revision to target. | [optional]
 **openRevs** | [**List&lt;String&gt;**](String.md)| Option to fetch specified revisions of the document. The value can be all to fetch all leaf revisions or an array of revision numbers (i.e. open_revs&#x3D;[\&quot;rev1\&quot;, \&quot;rev2\&quot;]). Only leaf revision bodies that haven&#x27;t been pruned are guaranteed to be returned. If this option is specified the response will be in multipart format. Use the &#x60;Accept: application/json&#x60; request header to get the result as a JSON object. | [optional]
 **showExp** | **Boolean**| Whether to show the expiry property (&#x60;_exp&#x60;) in the response. | [optional]
 **revsFrom** | [**List&lt;String&gt;**](String.md)| Trim the revision history to stop at the first revision in the provided list. If no match is found, the revisions will be trimmed to the &#x60;revs_limit&#x60;. | [optional]
 **attsSince** | [**List&lt;String&gt;**](String.md)| Include attachments only since specified revisions. Excludes the attachments for the specified revisions. Only gets used if &#x60;attachments&#x3D;true&#x60;. | [optional]
 **revsLimit** | **Integer**| Maximum amount of revisions to return for each document. | [optional]
 **attachments** | **Boolean**| Include attachment bodies in response. | [optional]
 **replicator2** | **Boolean**| Returns the document with the required properties for replication. This is an enterprise-edition only feature. | [optional]

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceDocidAttach"></a>
# **getKeyspaceDocidAttach**
> getKeyspaceDocidAttach(keyspace, docid, attach, rev, contentEncoding, range, meta)

Get an attachment from a document

This request retrieves a file attachment associated with the document.  The raw data of the associated attachment is returned (just as if you were accessing a static file). The &#x60;Content-Type&#x60; response header is the same content type set when the document attachment was added to the database. The &#x60;Content-Disposition&#x60; response header will be set if the content type is considered unsafe to display in a browser (unless overridden by by database config option &#x60;serve_insecure_attachment_types&#x60;) which will force the attachment to be downloaded.  If the &#x60;meta&#x60; query parameter is set then the response will be in JSON with the additional metadata tags.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String attach = "attach_example"; // String | The attachment name. This value must be URL encoded. For example, if the attachment name is `blob_/avatar`, the path component passed to the URL should be `blob_%2Favatar` (tested with [URLEncoder](https://www.urlencoder.org/)).
String rev = "rev_example"; // String | The document revision to target.
Boolean contentEncoding = true; // Boolean | Set to false to disable the `Content-Encoding` response header.
String range = "range_example"; // String | RFC-2616 bytes range header.
Boolean meta = false; // Boolean | Return only the metadata of the attachment in the response body.
try {
    apiInstance.getKeyspaceDocidAttach(keyspace, docid, attach, rev, contentEncoding, range, meta);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#getKeyspaceDocidAttach");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **attach** | **String**| The attachment name. This value must be URL encoded. For example, if the attachment name is &#x60;blob_/avatar&#x60;, the path component passed to the URL should be &#x60;blob_%2Favatar&#x60; (tested with [URLEncoder](https://www.urlencoder.org/)). |
 **rev** | **String**| The document revision to target. | [optional]
 **contentEncoding** | **Boolean**| Set to false to disable the &#x60;Content-Encoding&#x60; response header. | [optional] [default to true]
 **range** | **String**| RFC-2616 bytes range header. | [optional]
 **meta** | **Boolean**| Return only the metadata of the attachment in the response body. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceLocalDocid"></a>
# **getKeyspaceLocalDocid**
> getKeyspaceLocalDocid(keyspace, docid)

Get local document

This request retrieves a local document.  Local document IDs begin with &#x60;_local/&#x60;. Local documents are not replicated or indexed, don&#x27;t support attachments, and don&#x27;t save revision histories. In practice they are almost only used by Couchbase Lite&#x27;s replicator, as a place to store replication checkpoint data.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The name of the local document ID excluding the `_local/` prefix.
try {
    apiInstance.getKeyspaceLocalDocid(keyspace, docid);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#getKeyspaceLocalDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The name of the local document ID excluding the &#x60;_local/&#x60; prefix. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceRawDocid"></a>
# **getKeyspaceRawDocid**
> InlineResponse2001 getKeyspaceRawDocid(keyspace, docid, includeDoc, redact)

Get a document with the corresponding metadata

Returns the a documents latest revision with its metadata.  Note: The direct use of this endpoint is unsupported. The sync metadata is maintained internally by Sync Gateway and its structure can change. It should not be used to drive business logic of applications since the response to the &#x60;/{db}/_raw/{id}&#x60; endpoint can change at any time.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String includeDoc = "includeDoc_example"; // String | Include the body associated with the document.
Boolean redact = true; // Boolean | This redacts sensitive parts of the response. Cannot be used when `include_docs=true`
try {
    InlineResponse2001 result = apiInstance.getKeyspaceRawDocid(keyspace, docid, includeDoc, redact);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#getKeyspaceRawDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **includeDoc** | **String**| Include the body associated with the document. | [optional]
 **redact** | **Boolean**| This redacts sensitive parts of the response. Cannot be used when &#x60;include_docs&#x3D;true&#x60; | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceAllDocs"></a>
# **headKeyspaceAllDocs**
> headKeyspaceAllDocs(keyspace, includeDocs, channels, access, revs, updateSeq, keys, startkey, endkey, limit)

/{db}/_all_docs

Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String includeDocs = "includeDocs_example"; // String | Include the body associated with each document.
String channels = "channels_example"; // String | Include the channels each document is part of that the calling user also has access too.
String access = "access_example"; // String | Include what user/roles that each document grants access too.
String revs = "revs_example"; // String | Include all the revisions for each document under the `_revisions` property.
String updateSeq = "updateSeq_example"; // String | Include the document sequence number `update_seq` property for each document.
List<String> keys = Arrays.asList("keys_example"); // List<String> | An array of document ID strings to filter by.
String startkey = "startkey_example"; // String | Return records starting with the specified key.
String endkey = "endkey_example"; // String | Stop returning records when this key is reached.
BigDecimal limit = new BigDecimal(); // BigDecimal | This limits the number of result rows returned. Using a value of `0` has the same effect as the value `1`.
try {
    apiInstance.headKeyspaceAllDocs(keyspace, includeDocs, channels, access, revs, updateSeq, keys, startkey, endkey, limit);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#headKeyspaceAllDocs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **includeDocs** | **String**| Include the body associated with each document. | [optional]
 **channels** | **String**| Include the channels each document is part of that the calling user also has access too. | [optional]
 **access** | **String**| Include what user/roles that each document grants access too. | [optional]
 **revs** | **String**| Include all the revisions for each document under the &#x60;_revisions&#x60; property. | [optional]
 **updateSeq** | **String**| Include the document sequence number &#x60;update_seq&#x60; property for each document. | [optional]
 **keys** | [**List&lt;String&gt;**](String.md)| An array of document ID strings to filter by. | [optional]
 **startkey** | **String**| Return records starting with the specified key. | [optional]
 **endkey** | **String**| Stop returning records when this key is reached. | [optional]
 **limit** | **BigDecimal**| This limits the number of result rows returned. Using a value of &#x60;0&#x60; has the same effect as the value &#x60;1&#x60;. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceDocid"></a>
# **headKeyspaceDocid**
> headKeyspaceDocid(keyspace, docid, rev, openRevs, showExp, revsFrom, attsSince, revsLimit, attachments, replicator2)

Check if a document exists

Return a status code based on if the document exists or not.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String rev = "rev_example"; // String | The document revision to target.
List<String> openRevs = Arrays.asList("openRevs_example"); // List<String> | Option to fetch specified revisions of the document. The value can be all to fetch all leaf revisions or an array of revision numbers (i.e. open_revs=[\"rev1\", \"rev2\"]). Only leaf revision bodies that haven't been pruned are guaranteed to be returned. If this option is specified the response will be in multipart format. Use the `Accept: application/json` request header to get the result as a JSON object.
Boolean showExp = true; // Boolean | Whether to show the expiry property (`_exp`) in the response.
List<String> revsFrom = Arrays.asList("revsFrom_example"); // List<String> | Trim the revision history to stop at the first revision in the provided list. If no match is found, the revisions will be trimmed to the `revs_limit`.
List<String> attsSince = Arrays.asList("attsSince_example"); // List<String> | Include attachments only since specified revisions. Excludes the attachments for the specified revisions. Only gets used if `attachments=true`.
Integer revsLimit = 56; // Integer | Maximum amount of revisions to return for each document.
Boolean attachments = true; // Boolean | Include attachment bodies in response.
Boolean replicator2 = true; // Boolean | Returns the document with the required properties for replication. This is an enterprise-edition only feature.
try {
    apiInstance.headKeyspaceDocid(keyspace, docid, rev, openRevs, showExp, revsFrom, attsSince, revsLimit, attachments, replicator2);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#headKeyspaceDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **rev** | **String**| The document revision to target. | [optional]
 **openRevs** | [**List&lt;String&gt;**](String.md)| Option to fetch specified revisions of the document. The value can be all to fetch all leaf revisions or an array of revision numbers (i.e. open_revs&#x3D;[\&quot;rev1\&quot;, \&quot;rev2\&quot;]). Only leaf revision bodies that haven&#x27;t been pruned are guaranteed to be returned. If this option is specified the response will be in multipart format. Use the &#x60;Accept: application/json&#x60; request header to get the result as a JSON object. | [optional]
 **showExp** | **Boolean**| Whether to show the expiry property (&#x60;_exp&#x60;) in the response. | [optional]
 **revsFrom** | [**List&lt;String&gt;**](String.md)| Trim the revision history to stop at the first revision in the provided list. If no match is found, the revisions will be trimmed to the &#x60;revs_limit&#x60;. | [optional]
 **attsSince** | [**List&lt;String&gt;**](String.md)| Include attachments only since specified revisions. Excludes the attachments for the specified revisions. Only gets used if &#x60;attachments&#x3D;true&#x60;. | [optional]
 **revsLimit** | **Integer**| Maximum amount of revisions to return for each document. | [optional]
 **attachments** | **Boolean**| Include attachment bodies in response. | [optional]
 **replicator2** | **Boolean**| Returns the document with the required properties for replication. This is an enterprise-edition only feature. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceDocidAttach"></a>
# **headKeyspaceDocidAttach**
> headKeyspaceDocidAttach(keyspace, docid, attach, rev)

Check if attachment exists

This request check if the attachment exists on the specified document.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String attach = "attach_example"; // String | The attachment name. This value must be URL encoded. For example, if the attachment name is `blob_/avatar`, the path component passed to the URL should be `blob_%2Favatar` (tested with [URLEncoder](https://www.urlencoder.org/)).
String rev = "rev_example"; // String | The document revision to target.
try {
    apiInstance.headKeyspaceDocidAttach(keyspace, docid, attach, rev);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#headKeyspaceDocidAttach");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **attach** | **String**| The attachment name. This value must be URL encoded. For example, if the attachment name is &#x60;blob_/avatar&#x60;, the path component passed to the URL should be &#x60;blob_%2Favatar&#x60; (tested with [URLEncoder](https://www.urlencoder.org/)). |
 **rev** | **String**| The document revision to target. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceLocalDocid"></a>
# **headKeyspaceLocalDocid**
> headKeyspaceLocalDocid(keyspace, docid)

Check if local document exists

This request checks if a local document exists.  Local document IDs begin with &#x60;_local/&#x60;. Local documents are not replicated or indexed, don&#x27;t support attachments, and don&#x27;t save revision histories. In practice they are almost only used by Couchbase Lite&#x27;s replicator, as a place to store replication checkpoint data.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The name of the local document ID excluding the `_local/` prefix.
try {
    apiInstance.headKeyspaceLocalDocid(keyspace, docid);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#headKeyspaceLocalDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The name of the local document ID excluding the &#x60;_local/&#x60; prefix. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headKeyspaceRawDocid"></a>
# **headKeyspaceRawDocid**
> headKeyspaceRawDocid(keyspace, docid)

/{keyspace}/_raw/{docid}

Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
try {
    apiInstance.headKeyspaceRawDocid(keyspace, docid);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#headKeyspaceRawDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postKeyspace"></a>
# **postKeyspace**
> Newrevision postKeyspace(keyspace, body, roundtrip)

Create a new document

Create a new document in the keyspace.  This will generate a random document ID unless specified in the body.  A document can have a maximum size of 20MB.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
Map<String, Object> body = new Map(); // Map<String, Object> | 
Boolean roundtrip = true; // Boolean | Block until document has been received by change cache
try {
    Newrevision result = apiInstance.postKeyspace(keyspace, body, roundtrip);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#postKeyspace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**Map&lt;String, Object&gt;**](Map.md)|  | [optional]
 **roundtrip** | **Boolean**| Block until document has been received by change cache | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspaceAllDocs"></a>
# **postKeyspaceAllDocs**
> InlineResponse20012 postKeyspaceAllDocs(keyspace, body, includeDocs, channels, access, revs, updateSeq, startkey, endkey, limit)

Get all the documents in the database using a built-in view

Get a built-in view of all the documents in the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
KeyspaceAllDocsBody body = new KeyspaceAllDocsBody(); // KeyspaceAllDocsBody | 
String includeDocs = "includeDocs_example"; // String | Include the body associated with each document.
String channels = "channels_example"; // String | Include the channels each document is part of that the calling user also has access too.
String access = "access_example"; // String | Include what user/roles that each document grants access too.
String revs = "revs_example"; // String | Include all the revisions for each document under the `_revisions` property.
String updateSeq = "updateSeq_example"; // String | Include the document sequence number `update_seq` property for each document.
String startkey = "startkey_example"; // String | Return records starting with the specified key.
String endkey = "endkey_example"; // String | Stop returning records when this key is reached.
BigDecimal limit = new BigDecimal(); // BigDecimal | This limits the number of result rows returned. Using a value of `0` has the same effect as the value `1`.
try {
    InlineResponse20012 result = apiInstance.postKeyspaceAllDocs(keyspace, body, includeDocs, channels, access, revs, updateSeq, startkey, endkey, limit);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#postKeyspaceAllDocs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**KeyspaceAllDocsBody**](KeyspaceAllDocsBody.md)|  | [optional]
 **includeDocs** | **String**| Include the body associated with each document. | [optional]
 **channels** | **String**| Include the channels each document is part of that the calling user also has access too. | [optional]
 **access** | **String**| Include what user/roles that each document grants access too. | [optional]
 **revs** | **String**| Include all the revisions for each document under the &#x60;_revisions&#x60; property. | [optional]
 **updateSeq** | **String**| Include the document sequence number &#x60;update_seq&#x60; property for each document. | [optional]
 **startkey** | **String**| Return records starting with the specified key. | [optional]
 **endkey** | **String**| Stop returning records when this key is reached. | [optional]
 **limit** | **BigDecimal**| This limits the number of result rows returned. Using a value of &#x60;0&#x60; has the same effect as the value &#x60;1&#x60;. | [optional]

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspaceBulkDocs"></a>
# **postKeyspaceBulkDocs**
> List&lt;InlineResponse201&gt; postKeyspaceBulkDocs(keyspace, body)

Bulk document operations

This will allow multiple documented to be created, updated or deleted in bulk.  To create a new document, simply add the body in an object under &#x60;docs&#x60;. A doc ID will be generated by Sync Gateway unless &#x60;_id&#x60; is specified.  To update an existing document, provide the document ID (&#x60;_id&#x60;) and revision ID (&#x60;_rev&#x60;) as well as the new body values.  To delete an existing document, provide the document ID (&#x60;_id&#x60;), revision ID (&#x60;_rev&#x60;), and set the deletion flag (&#x60;_deleted&#x60;) to true.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
KeyspaceBulkDocsBody body = new KeyspaceBulkDocsBody(); // KeyspaceBulkDocsBody | 
try {
    List<InlineResponse201> result = apiInstance.postKeyspaceBulkDocs(keyspace, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#postKeyspaceBulkDocs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**KeyspaceBulkDocsBody**](KeyspaceBulkDocsBody.md)|  | [optional]

### Return type

[**List&lt;InlineResponse201&gt;**](InlineResponse201.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspaceBulkGet"></a>
# **postKeyspaceBulkGet**
> postKeyspaceBulkGet(keyspace, body, xAcceptPartEncoding, acceptEncoding, attachments, revs, revsLimit)

Get multiple documents in a MIME multipart response

This request returns any number of documents, as individual bodies in a MIME multipart response.  Each enclosed body contains one requested document. The bodies appear in the same order as in the request, but can also be identified by their &#x60;X-Doc-ID&#x60; and &#x60;X-Rev-ID&#x60; headers (if the &#x60;attachments&#x60; query is &#x60;true&#x60;).  A body for a document with no attachments will have content type &#x60;application/json&#x60; and contain the document itself.  A body for a document that has attachments will be written as a nested &#x60;multipart/related&#x60; body.  Required Sync Gateway RBAC roles:  * Sync Gateway Application * Sync Gateway Application Read Only 

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
KeyspaceBulkGetBody body = new KeyspaceBulkGetBody(); // KeyspaceBulkGetBody | 
String xAcceptPartEncoding = "xAcceptPartEncoding_example"; // String | If this header includes `gzip` then the part HTTP compression encoding will be done.
String acceptEncoding = "acceptEncoding_example"; // String | If this header includes `gzip` then the the HTTP response will be compressed. This takes priority over `X-Accept-Part-Encoding`. Only part compression will be done if `X-Accept-Part-Encoding=gzip` and the `User-Agent` is below 1.2 due to clients not being able to handle full compression.
Boolean attachments = false; // Boolean | This is for whether to include attachments in each of the documents returned or not.
String revs = "revs_example"; // String | Include all the revisions for each document under the `_revisions` property.
Integer revsLimit = 56; // Integer | The number of revisions to include in the response from the document history. This parameter only makes a different if the `revs` query parameter is set to `true`. The full revision history will be returned if `revs` is set but this is not.
try {
    apiInstance.postKeyspaceBulkGet(keyspace, body, xAcceptPartEncoding, acceptEncoding, attachments, revs, revsLimit);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#postKeyspaceBulkGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**KeyspaceBulkGetBody**](KeyspaceBulkGetBody.md)|  | [optional]
 **xAcceptPartEncoding** | **String**| If this header includes &#x60;gzip&#x60; then the part HTTP compression encoding will be done. | [optional]
 **acceptEncoding** | **String**| If this header includes &#x60;gzip&#x60; then the the HTTP response will be compressed. This takes priority over &#x60;X-Accept-Part-Encoding&#x60;. Only part compression will be done if &#x60;X-Accept-Part-Encoding&#x3D;gzip&#x60; and the &#x60;User-Agent&#x60; is below 1.2 due to clients not being able to handle full compression. | [optional]
 **attachments** | **Boolean**| This is for whether to include attachments in each of the documents returned or not. | [optional] [default to false]
 **revs** | **String**| Include all the revisions for each document under the &#x60;_revisions&#x60; property. | [optional]
 **revsLimit** | **Integer**| The number of revisions to include in the response from the document history. This parameter only makes a different if the &#x60;revs&#x60; query parameter is set to &#x60;true&#x60;. The full revision history will be returned if &#x60;revs&#x60; is set but this is not. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postKeyspacePurge"></a>
# **postKeyspacePurge**
> InlineResponse2007 postKeyspacePurge(keyspace, body)

Purge a document

The purge command provides a way to remove a document from the database. The operation removes *all* revisions (active and tombstones) for the specified document(s). A common usage of this endpoint is to remove tombstone documents that are no longer needed, thus recovering storage space and reducing data replicated to clients. Other clients are not notified when a revision has been purged; so in order to purge a revision from the system it must be done from all databases (on Couchbase Lite and Sync Gateway).  When &#x60;enable_shared_bucket_access&#x60; is enabled, this endpoint removes the document and its associated extended attributes.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
Map<String, List<String>> body = new Map(); // Map<String, List<String>> | Purge request body
try {
    InlineResponse2007 result = apiInstance.postKeyspacePurge(keyspace, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#postKeyspacePurge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**Map&lt;String, List&lt;String&gt;&gt;**](Map.md)| Purge request body | [optional]

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putKeyspaceDocid"></a>
# **putKeyspaceDocid**
> Newrevision putKeyspaceDocid(keyspace, docid, body, ifMatch, roundtrip, replicator2, newEdits, rev)

Upsert a document

This will upsert a document meaning if it does not exist, then it will be created. Otherwise a new revision will be made for the existing document. A revision ID must be provided if targetting an existing document.  A document ID must be specified for this endpoint. To let Sync Gateway generate the ID, use the &#x60;POST /{db}/&#x60; endpoint.  If a document does exist, then replace the document content with the request body. This means unspecified fields will be removed in the new revision.  The maximum size for a document is 20MB.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
Map<String, Object> body = new Map(); // Map<String, Object> | 
String ifMatch = "ifMatch_example"; // String | The revision ID to target.
Boolean roundtrip = true; // Boolean | Block until document has been received by change cache
Boolean replicator2 = true; // Boolean | Returns the document with the required properties for replication. This is an enterprise-edition only feature.
Boolean newEdits = true; // Boolean | Setting this to false indicates that the request body is an already-existing revision that should be directly inserted into the database, instead of a modification to apply to the current document. This mode is used for replication.  This option must be used in conjunction with the `_revisions` property in the request body.
String rev = "rev_example"; // String | The document revision to target.
try {
    Newrevision result = apiInstance.putKeyspaceDocid(keyspace, docid, body, ifMatch, roundtrip, replicator2, newEdits, rev);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#putKeyspaceDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **body** | [**Map&lt;String, Object&gt;**](Map.md)|  | [optional]
 **ifMatch** | **String**| The revision ID to target. | [optional]
 **roundtrip** | **Boolean**| Block until document has been received by change cache | [optional]
 **replicator2** | **Boolean**| Returns the document with the required properties for replication. This is an enterprise-edition only feature. | [optional]
 **newEdits** | **Boolean**| Setting this to false indicates that the request body is an already-existing revision that should be directly inserted into the database, instead of a modification to apply to the current document. This mode is used for replication.  This option must be used in conjunction with the &#x60;_revisions&#x60; property in the request body. | [optional] [default to true]
 **rev** | **String**| The document revision to target. | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putKeyspaceDocidAttach"></a>
# **putKeyspaceDocidAttach**
> Newrevision putKeyspaceDocidAttach(keyspace, docid, attach, body, contentType, ifMatch, rev)

Create or update an attachment on a document

This request adds or updates an attachment associated with the document. If the document does not exist, it will be created and the attachment will be added to it.  If the attachment already exists, the data of the existing attachment will be replaced in the new revision.  The maximum content size of an attachment is 20MB. The &#x60;Content-Type&#x60; header of the request specifies the content type of the attachment.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The document ID to run the operation against.
String attach = "attach_example"; // String | The attachment name. This value must be URL encoded. For example, if the attachment name is `blob_/avatar`, the path component passed to the URL should be `blob_%2Favatar` (tested with [URLEncoder](https://www.urlencoder.org/)).
String body = "body_example"; // String | The attachment data
String contentType = "application/octet-stream"; // String | The content type of the attachment.
String ifMatch = "ifMatch_example"; // String | An alternative way of specifying the document revision ID.
String rev = "rev_example"; // String | The existing document revision ID to modify. Required only when modifying an existing document.
try {
    Newrevision result = apiInstance.putKeyspaceDocidAttach(keyspace, docid, attach, body, contentType, ifMatch, rev);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#putKeyspaceDocidAttach");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The document ID to run the operation against. |
 **attach** | **String**| The attachment name. This value must be URL encoded. For example, if the attachment name is &#x60;blob_/avatar&#x60;, the path component passed to the URL should be &#x60;blob_%2Favatar&#x60; (tested with [URLEncoder](https://www.urlencoder.org/)). |
 **body** | [**String**](String.md)| The attachment data | [optional]
 **contentType** | **String**| The content type of the attachment. | [optional] [default to application/octet-stream]
 **ifMatch** | **String**| An alternative way of specifying the document revision ID. | [optional]
 **rev** | **String**| The existing document revision ID to modify. Required only when modifying an existing document. | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Attachment content type
 - **Accept**: application/json

<a name="putKeyspaceLocalDocid"></a>
# **putKeyspaceLocalDocid**
> Newrevision putKeyspaceLocalDocid(keyspace, docid, body)

Upsert a local document

This request creates or updates a local document. Updating a local document requires that the revision ID be put in the body under &#x60;_rev&#x60;.  Local document IDs are given a &#x60;_local/&#x60; prefix. Local documents are not replicated or indexed, don&#x27;t support attachments, and don&#x27;t save revision histories. In practice they are almost only used by the client&#x27;s replicator, as a place to store replication checkpoint data.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DocumentApi;


DocumentApi apiInstance = new DocumentApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String docid = "docid_example"; // String | The name of the local document ID excluding the `_local/` prefix.
LocalDocidBody body = new LocalDocidBody(); // LocalDocidBody | The body of the document
try {
    Newrevision result = apiInstance.putKeyspaceLocalDocid(keyspace, docid, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DocumentApi#putKeyspaceLocalDocid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **docid** | **String**| The name of the local document ID excluding the &#x60;_local/&#x60; prefix. |
 **body** | [**LocalDocidBody**](LocalDocidBody.md)| The body of the document | [optional]

### Return type

[**Newrevision**](Newrevision.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

