# NodeInfoVendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Product name | 
**version** | **String** | API version. Omitted if &#x60;api.hide_product_version&#x3D;true&#x60; |  [optional]
