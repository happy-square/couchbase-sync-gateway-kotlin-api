# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**memstats** | **Map&lt;String, Object&gt;** | A set of Go runtime memory statistics. |  [optional]
