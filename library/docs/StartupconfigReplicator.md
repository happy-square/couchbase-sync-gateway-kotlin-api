# StartupconfigReplicator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxHeartbeat** | **String** | Max heartbeat value for &#x60;_changes&#x60; request.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**blipCompression** | **Integer** | BLIP data compression level (0-9) |  [optional]
**maxConcurrentReplications** | **Integer** | Maximum number of concurrent replication connections allowed. If set to 0 this limit will be ignored. |  [optional]
