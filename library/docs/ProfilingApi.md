# ProfilingApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDebugFgprof**](ProfilingApi.md#getDebugFgprof) | **GET** /_debug/fgprof | Get fgprof profile
[**getDebugPprofBlock**](ProfilingApi.md#getDebugPprofBlock) | **GET** /_debug/pprof/block | Get block profile
[**getDebugPprofCmdline**](ProfilingApi.md#getDebugPprofCmdline) | **GET** /_debug/pprof/cmdline | Get passed in command line parameters
[**getDebugPprofGoroutine**](ProfilingApi.md#getDebugPprofGoroutine) | **GET** /_debug/pprof/goroutine | Get goroutine profile
[**getDebugPprofHeap**](ProfilingApi.md#getDebugPprofHeap) | **GET** /_debug/pprof/heap | Get the heap pprof debug file
[**getDebugPprofMutex**](ProfilingApi.md#getDebugPprofMutex) | **GET** /_debug/pprof/mutex | Get mutex profile
[**getDebugPprofProfile**](ProfilingApi.md#getDebugPprofProfile) | **GET** /_debug/pprof/profile | Get the profile pprof debug file
[**getDebugPprofSymbol**](ProfilingApi.md#getDebugPprofSymbol) | **GET** /_debug/pprof/symbol | Get symbol pprof debug information
[**getDebugPprofThreadcreate**](ProfilingApi.md#getDebugPprofThreadcreate) | **GET** /_debug/pprof/threadcreate | Get the threadcreate pprof debug file
[**getDebugPprofTrace**](ProfilingApi.md#getDebugPprofTrace) | **GET** /_debug/pprof/trace | Get trace profile
[**postDebugFgprof**](ProfilingApi.md#postDebugFgprof) | **POST** /_debug/fgprof | Get fgprof profile
[**postDebugPprofBlock**](ProfilingApi.md#postDebugPprofBlock) | **POST** /_debug/pprof/block | Get block profile
[**postDebugPprofCmdline**](ProfilingApi.md#postDebugPprofCmdline) | **POST** /_debug/pprof/cmdline | Get passed in command line parameters
[**postDebugPprofGoroutine**](ProfilingApi.md#postDebugPprofGoroutine) | **POST** /_debug/pprof/goroutine | Get goroutine profile
[**postDebugPprofHeap**](ProfilingApi.md#postDebugPprofHeap) | **POST** /_debug/pprof/heap | Get the heap pprof debug file
[**postDebugPprofMutex**](ProfilingApi.md#postDebugPprofMutex) | **POST** /_debug/pprof/mutex | Get mutex profile
[**postDebugPprofProfile**](ProfilingApi.md#postDebugPprofProfile) | **POST** /_debug/pprof/profile | Get the profile pprof debug file
[**postDebugPprofSymbol**](ProfilingApi.md#postDebugPprofSymbol) | **POST** /_debug/pprof/symbol | Get symbol pprof debug information
[**postDebugPprofThreadcreate**](ProfilingApi.md#postDebugPprofThreadcreate) | **POST** /_debug/pprof/threadcreate | Get the threadcreate pprof debug file
[**postDebugPprofTrace**](ProfilingApi.md#postDebugPprofTrace) | **POST** /_debug/pprof/trace | Get trace profile
[**postHeap**](ProfilingApi.md#postHeap) | **POST** /_heap | Dump heap profile
[**postProfile**](ProfilingApi.md#postProfile) | **POST** /_profile | Start or Stop continuous CPU profiling
[**postProfileProfilename**](ProfilingApi.md#postProfileProfilename) | **POST** /_profile/{profilename} | Create point-in-time profile

<a name="getDebugFgprof"></a>
# **getDebugFgprof**
> String getDebugFgprof(seconds)

Get fgprof profile

A sampling Go profiler that allows you to analyze On-CPU as well as [Off-CPU](https://www.brendangregg.com/offcpuanalysis.html) (e.g. I/O) time together.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.getDebugFgprof(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugFgprof");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-gzip

<a name="getDebugPprofBlock"></a>
# **getDebugPprofBlock**
> String getDebugPprofBlock(seconds)

Get block profile

Returns stack traces that led to blocking on synchronization primitives.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.getDebugPprofBlock(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofBlock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

<a name="getDebugPprofCmdline"></a>
# **getDebugPprofCmdline**
> String getDebugPprofCmdline()

Get passed in command line parameters

Gets the command line parameters that was passed in to Sync Gateway which will include the binary, flags (if any) and startup configuration.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.getDebugPprofCmdline();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofCmdline");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getDebugPprofGoroutine"></a>
# **getDebugPprofGoroutine**
> String getDebugPprofGoroutine(seconds)

Get goroutine profile

Stack traces of all current goroutines.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.getDebugPprofGoroutine(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofGoroutine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getDebugPprofHeap"></a>
# **getDebugPprofHeap**
> String getDebugPprofHeap(seconds)

Get the heap pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.getDebugPprofHeap(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofHeap");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getDebugPprofMutex"></a>
# **getDebugPprofMutex**
> String getDebugPprofMutex(seconds)

Get mutex profile

Returns stack traces of holders of contended mutexes.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.getDebugPprofMutex(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofMutex");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

<a name="getDebugPprofProfile"></a>
# **getDebugPprofProfile**
> String getDebugPprofProfile(seconds)

Get the profile pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.getDebugPprofProfile(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getDebugPprofSymbol"></a>
# **getDebugPprofSymbol**
> String getDebugPprofSymbol()

Get symbol pprof debug information

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.getDebugPprofSymbol();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofSymbol");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="getDebugPprofThreadcreate"></a>
# **getDebugPprofThreadcreate**
> String getDebugPprofThreadcreate()

Get the threadcreate pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.getDebugPprofThreadcreate();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofThreadcreate");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="getDebugPprofTrace"></a>
# **getDebugPprofTrace**
> String getDebugPprofTrace(seconds)

Get trace profile

Responds with the execution trace in binary form.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 1; // Integer | 
try {
    String result = apiInstance.getDebugPprofTrace(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#getDebugPprofTrace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**|  | [optional] [default to 1]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postDebugFgprof"></a>
# **postDebugFgprof**
> String postDebugFgprof(seconds)

Get fgprof profile

A sampling Go profiler that allows you to analyze On-CPU as well as [Off-CPU](https://www.brendangregg.com/offcpuanalysis.html) (e.g. I/O) time together.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.postDebugFgprof(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugFgprof");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-gzip

<a name="postDebugPprofBlock"></a>
# **postDebugPprofBlock**
> String postDebugPprofBlock(seconds)

Get block profile

Returns stack traces that led to blocking on synchronization primitives.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.postDebugPprofBlock(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofBlock");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

<a name="postDebugPprofCmdline"></a>
# **postDebugPprofCmdline**
> String postDebugPprofCmdline()

Get passed in command line parameters

Gets the command line parameters that was passed in to Sync Gateway which will include the binary, flags (if any) and startup configuration.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.postDebugPprofCmdline();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofCmdline");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="postDebugPprofGoroutine"></a>
# **postDebugPprofGoroutine**
> String postDebugPprofGoroutine(seconds)

Get goroutine profile

Stack traces of all current goroutines.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.postDebugPprofGoroutine(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofGoroutine");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postDebugPprofHeap"></a>
# **postDebugPprofHeap**
> String postDebugPprofHeap(seconds)

Get the heap pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.postDebugPprofHeap(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofHeap");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postDebugPprofMutex"></a>
# **postDebugPprofMutex**
> String postDebugPprofMutex(seconds)

Get mutex profile

Returns stack traces of holders of contended mutexes.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 30; // Integer | The amount of seconds to run the profiler for.
try {
    String result = apiInstance.postDebugPprofMutex(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofMutex");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| The amount of seconds to run the profiler for. | [optional] [default to 30] [enum: ]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

<a name="postDebugPprofProfile"></a>
# **postDebugPprofProfile**
> String postDebugPprofProfile(seconds)

Get the profile pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 56; // Integer | If set, collect a delta profile for the given duration, instead of a snapshot.
try {
    String result = apiInstance.postDebugPprofProfile(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**| If set, collect a delta profile for the given duration, instead of a snapshot. | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postDebugPprofSymbol"></a>
# **postDebugPprofSymbol**
> String postDebugPprofSymbol()

Get symbol pprof debug information

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.postDebugPprofSymbol();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofSymbol");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="postDebugPprofThreadcreate"></a>
# **postDebugPprofThreadcreate**
> String postDebugPprofThreadcreate()

Get the threadcreate pprof debug file

Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
try {
    String result = apiInstance.postDebugPprofThreadcreate();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofThreadcreate");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postDebugPprofTrace"></a>
# **postDebugPprofTrace**
> String postDebugPprofTrace(seconds)

Get trace profile

Responds with the execution trace in binary form.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
Integer seconds = 1; // Integer | 
try {
    String result = apiInstance.postDebugPprofTrace(seconds);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postDebugPprofTrace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seconds** | **Integer**|  | [optional] [default to 1]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

<a name="postHeap"></a>
# **postHeap**
> postHeap(body)

Dump heap profile

This endpoint will dump a pprof heap profile.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
HeapBody body = new HeapBody(); // HeapBody | 
try {
    apiInstance.postHeap(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postHeap");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HeapBody**](HeapBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postProfile"></a>
# **postProfile**
> postProfile(body)

Start or Stop continuous CPU profiling

This endpoint allows you to start and stop continuous CPU profiling.  To start profiling the CPU, call this endpoint and supply a file to output the pprof file to.  To stop profiling, call this endpoint but don&#x27;t supply the &#x60;file&#x60; in the body.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
ProfileBody body = new ProfileBody(); // ProfileBody | 
try {
    apiInstance.postProfile(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProfileBody**](ProfileBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postProfileProfilename"></a>
# **postProfileProfilename**
> postProfileProfilename(profilename, body)

Create point-in-time profile

This endpoint allows you to create a pprof snapshot of the given type.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ProfilingApi;


ProfilingApi apiInstance = new ProfilingApi();
String profilename = "profilename_example"; // String | The handler to use for profiling.
ProfileProfilenameBody body = new ProfileProfilenameBody(); // ProfileProfilenameBody | 
try {
    apiInstance.postProfileProfilename(profilename, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilingApi#postProfileProfilename");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profilename** | **String**| The handler to use for profiling. | [enum: heap, block, threadcreate, mutex, goroutine]
 **body** | [**ProfileProfilenameBody**](ProfileProfilenameBody.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

