# UsersessioninformationUserCtx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channels** | **Object** | A map of the channels the user has access to and the sequence number each channel was created at.  The key is the channel name and the value is the sequence number.  |  [optional]
**name** | **String** | The name of the user. |  [optional]
