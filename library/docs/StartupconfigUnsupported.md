# StartupconfigUnsupported

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serverless** | [**StartupconfigUnsupportedServerless**](StartupconfigUnsupportedServerless.md) |  |  [optional]
**useXattrConfig** | **Boolean** | Store database configurations in system xattrs |  [optional]
**statsLogFrequency** | **String** | How often should stats be written to stats logs.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**useStdlibJson** | **Boolean** | Bypass the jsoniter package and use Go&#x27;s stdlib instead |  [optional]
**http2** | [**StartupconfigUnsupportedHttp2**](StartupconfigUnsupportedHttp2.md) |  |  [optional]
