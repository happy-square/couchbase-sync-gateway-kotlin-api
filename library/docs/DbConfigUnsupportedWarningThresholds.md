# DbConfigUnsupportedWarningThresholds

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xattrSizeBytes** | [**BigDecimal**](BigDecimal.md) | The number of bytes to be used as a threshold for xattr size limit warnings. |  [optional]
**channelsPerDoc** | [**BigDecimal**](BigDecimal.md) | The number of channels per document to be used as a threshold for the channel count warnings. |  [optional]
**accessAndRoleGrantsPerDoc** | [**BigDecimal**](BigDecimal.md) | The number of access and role grants per document to be used as a threshold for grant count warnings. |  [optional]
**channelsPerUser** | [**BigDecimal**](BigDecimal.md) | The number of channels per user to be used as a threshold for channel count warnings. |  [optional]
**channelNameSize** | [**BigDecimal**](BigDecimal.md) | The number of channel name characters to be used as a threshold for channel name warnings. |  [optional]
