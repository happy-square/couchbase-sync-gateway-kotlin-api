# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionId** | **String** | The ID of the session. This is the value that would be put in to the cookie to keep the user authenticated. |  [optional]
**expires** | **String** | The date and time the cookie expires. |  [optional]
**cookieName** | **String** | The name of the cookie that would be used to store the users session. |  [optional]
