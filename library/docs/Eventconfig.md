# Eventconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handler** | [**HandlerEnum**](#HandlerEnum) | The handler type. |  [optional]
**url** | **String** | The URL of the webhook. |  [optional]
**filter** | **String** | The Javascript function to use to filter the webhook events. |  [optional]
**timeout** | [**BigDecimal**](BigDecimal.md) | The amount of time (in seconds) to attempt connect to the webhook before giving up. |  [optional]
**options** | **Map&lt;String, Object&gt;** | The options for the event. |  [optional]

<a name="HandlerEnum"></a>
## Enum: HandlerEnum
Name | Value
---- | -----
WEBHOOK | &quot;webhook&quot;
