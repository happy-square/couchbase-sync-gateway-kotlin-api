# ServerApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteSgcollectInfo**](ServerApi.md#deleteSgcollectInfo) | **DELETE** /_sgcollect_info | Cancel the Sync Gateway Collect Info job
[**get**](ServerApi.md#get) | **GET** / | Get server information
[**getConfig**](ServerApi.md#getConfig) | **GET** /_config | Get server configuration
[**getLogging**](ServerApi.md#getLogging) | **GET** /_logging | Get console logging settings
[**getSgcollectInfo**](ServerApi.md#getSgcollectInfo) | **GET** /_sgcollect_info | Get the status of the Sync Gateway Collect Info
[**getStatus**](ServerApi.md#getStatus) | **GET** /_status | Get the server status
[**head**](ServerApi.md#head) | **HEAD** / | Check if server online
[**postLogging**](ServerApi.md#postLogging) | **POST** /_logging | Update console logging settings
[**postPostUpgrade**](ServerApi.md#postPostUpgrade) | **POST** /_post_upgrade | Run the post upgrade process on all databases
[**postSgcollectInfo**](ServerApi.md#postSgcollectInfo) | **POST** /_sgcollect_info | Start Sync Gateway Collect Info
[**putConfig**](ServerApi.md#putConfig) | **PUT** /_config | Set runtime configuration
[**putLogging**](ServerApi.md#putLogging) | **PUT** /_logging | Set console logging settings

<a name="deleteSgcollectInfo"></a>
# **deleteSgcollectInfo**
> InlineResponse2005 deleteSgcollectInfo()

Cancel the Sync Gateway Collect Info job

This endpoint is used to cancel a current Sync Gateway Collect Info (sgcollect_info) job that is running.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    InlineResponse2005 result = apiInstance.deleteSgcollectInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#deleteSgcollectInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="get"></a>
# **get**
> InlineResponse20011 get()

Get server information

Returns information about the Sync Gateway node.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    InlineResponse20011 result = apiInstance.get();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#get");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getConfig"></a>
# **getConfig**
> Startupconfig getConfig(redact, includeRuntime)

Get server configuration

This will return the configuration that the Sync Gateway node was initially started up with, or the currently config if &#x60;include_runtime&#x60; is set.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
Boolean redact = true; // Boolean | No longer supported field.
Boolean includeRuntime = false; // Boolean | Whether to include the values set after starting (at runtime), default values, and all loaded databases.
try {
    Startupconfig result = apiInstance.getConfig(redact, includeRuntime);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **redact** | **Boolean**| No longer supported field. | [optional] [default to true]
 **includeRuntime** | **Boolean**| Whether to include the values set after starting (at runtime), default values, and all loaded databases. | [optional] [default to false]

### Return type

[**Startupconfig**](Startupconfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLogging"></a>
# **getLogging**
> Map&lt;String, Boolean&gt; getLogging()

Get console logging settings

**Deprecated in favour of &#x60;GET /_config&#x60;** This will return a map of the log keys being used for the console logging.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    Map<String, Boolean> result = apiInstance.getLogging();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getLogging");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Map&lt;String, Boolean&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSgcollectInfo"></a>
# **getSgcollectInfo**
> InlineResponse2003 getSgcollectInfo()

Get the status of the Sync Gateway Collect Info

This will return the status of whether Sync Gateway Collect Info (sgcollect_info) is currently running or not.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    InlineResponse2003 result = apiInstance.getSgcollectInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getSgcollectInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getStatus"></a>
# **getStatus**
> Status1 getStatus()

Get the server status

This will retrieve the status of each database and the overall server status.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    Status1 result = apiInstance.getStatus();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getStatus");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Status1**](Status1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="head"></a>
# **head**
> head()

Check if server online

Check if the server is online by checking the status code of response.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
try {
    apiInstance.head();
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#head");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postLogging"></a>
# **postLogging**
> postLogging(body, logLevel, level)

Update console logging settings

**Deprecated in favour of &#x60;PUT /_config&#x60;** This is for enabling the log keys provided and optionally changing the console log level.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
Map<String, Boolean> body = new Map(); // Map<String, Boolean> | The console log keys to upsert.
String logLevel = "logLevel_example"; // String | The is what to set the console log level too.
Integer level = 56; // Integer | **Deprecated: use log level instead.**  This sets the console log level depending on the value provide. 1 sets to `info`, 2 sets to `warn`, and 3 sets to `error`.'
try {
    apiInstance.postLogging(body, logLevel, level);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#postLogging");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Map&lt;String, Boolean&gt;**](Map.md)| The console log keys to upsert. | [optional]
 **logLevel** | **String**| The is what to set the console log level too. | [optional] [enum: none, error, warn, info, debug, trace]
 **level** | **Integer**| **Deprecated: use log level instead.**  This sets the console log level depending on the value provide. 1 sets to &#x60;info&#x60;, 2 sets to &#x60;warn&#x60;, and 3 sets to &#x60;error&#x60;.&#x27; | [optional] [enum: ]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postPostUpgrade"></a>
# **postPostUpgrade**
> InlineResponse2006 postPostUpgrade(preview)

Run the post upgrade process on all databases

The post upgrade process involves removing obsolete design documents and indexes when they are no longer needed.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String preview = "false"; // String | If set, a dry-run will be done to return what would be removed.
try {
    InlineResponse2006 result = apiInstance.postPostUpgrade(preview);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#postPostUpgrade");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **preview** | **String**| If set, a dry-run will be done to return what would be removed. | [optional] [default to false]

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postSgcollectInfo"></a>
# **postSgcollectInfo**
> InlineResponse2004 postSgcollectInfo(body)

Start Sync Gateway Collect Info

This endpoint is used to start a Sync Gateway Collect Info (sgcollect_info) job so that Sync Gateway diagnostic data can be outputted to a file.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
SgcollectInfoBody body = new SgcollectInfoBody(); // SgcollectInfoBody | sgcollect_info options
try {
    InlineResponse2004 result = apiInstance.postSgcollectInfo(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#postSgcollectInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SgcollectInfoBody**](SgcollectInfoBody.md)| sgcollect_info options | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putConfig"></a>
# **putConfig**
> putConfig(body)

Set runtime configuration

This endpoint is used to dynamically set runtime options, like logging without needing a restart.  These options are not persisted, and will not survive a restart of Sync Gateway.  The endpoint only accepts a limited number of options that can be changed at runtime. See request body schema for allowable options.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
Runtimeconfig body = new Runtimeconfig(); // Runtimeconfig | 
try {
    apiInstance.putConfig(body);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#putConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Runtimeconfig**](Runtimeconfig.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putLogging"></a>
# **putLogging**
> putLogging(body, logLevel, level)

Set console logging settings

**Deprecated in favour of &#x60;PUT /_config&#x60;** Enable or disable console log keys and optionally change the console log level.  Required Sync Gateway RBAC roles:  * Sync Gateway Dev Ops

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ServerApi;


ServerApi apiInstance = new ServerApi();
Map<String, Boolean> body = new Map(); // Map<String, Boolean> | The map of log keys to use for console logging.
String logLevel = "logLevel_example"; // String | The is what to set the console log level too.
Integer level = 56; // Integer | **Deprecated: use log level instead.**  This sets the console log level depending on the value provide. 1 sets to `info`, 2 sets to `warn`, and 3 sets to `error`.'
try {
    apiInstance.putLogging(body, logLevel, level);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#putLogging");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Map&lt;String, Boolean&gt;**](Map.md)| The map of log keys to use for console logging. | [optional]
 **logLevel** | **String**| The is what to set the console log level too. | [optional] [enum: none, error, warn, info, debug, trace]
 **level** | **Integer**| **Deprecated: use log level instead.**  This sets the console log level depending on the value provide. 1 sets to &#x60;info&#x60;, 2 sets to &#x60;warn&#x60;, and 3 sets to &#x60;error&#x60;.&#x27; | [optional] [enum: ]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

