# DbConfigOidc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**providers** | [**Map&lt;String, DbConfigOidcProviders&gt;**](DbConfigOidcProviders.md) | List of OpenID Connect issuers. |  [optional]
**defaultProvider** | **String** | The default provider to use when the provider is not specified in the client. |  [optional]
