# InlineResponse2008Rows

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**key** | **Object** |  |  [optional]
**value** | **Object** |  |  [optional]
**doc** | **Object** |  |  [optional]
