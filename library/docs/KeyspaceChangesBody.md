# KeyspaceChangesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limit** | **String** | Maximum number of changes to return. |  [optional]
**style** | **String** | Controls whether to return the current winning revision (&#x60;main_only&#x60;) or all the leaf revision including conflicts and deleted former conflicts (&#x60;all_docs&#x60;). |  [optional]
**activeOnly** | **String** | Set true to exclude deleted documents and notifications for documents the user no longer has access to from the changes feed. |  [optional]
**includeDocs** | **String** | Include the body associated with each document. |  [optional]
**revocations** | **String** | If true, revocation messages will be sent on the changes feed. |  [optional]
**filter** | **String** | Set a filter to either filter by channels or document IDs. |  [optional]
**channels** | **String** | A comma-separated list of channel names to filter the response to only the channels specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;sync_gateway/bychannels&#x60;. |  [optional]
**docIds** | **String** | A valid JSON array of document IDs to filter the documents in the response to only the documents specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;_doc_ids&#x60; and the &#x60;feed&#x60; parameter must be &#x60;normal&#x60;. |  [optional]
**heartbeat** | **String** | The interval (in milliseconds) to send an empty line (CRLF) in the response. This is to help prevent gateways from deciding the socket is idle and therefore closing it. This is only applicable to &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60;. This will override any timeouts to keep the feed alive indefinitely. Setting to 0 results in no heartbeat. The maximum heartbeat can be set in the server replication configuration. |  [optional]
**timeout** | **String** | This is the maximum period (in milliseconds) to wait for a change before the response is sent, even if there are no results. This is only applicable for &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60; changes feeds. Setting to 0 results in no timeout. |  [optional]
**feed** | **String** | The type of changes feed to use.  |  [optional]
**requestPlus** | **String** | When true, ensures all valid documents written prior to the request being issued are included in the response.  This is only applicable for non-continuous feeds. |  [optional]
