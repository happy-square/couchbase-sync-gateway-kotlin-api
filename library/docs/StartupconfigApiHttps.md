# StartupconfigApiHttps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tlsMinimumVersion** | **String** | The minimum allowable TLS version for the REST APIs |  [optional]
**tlsCertPath** | **String** | The TLS cert file to use for the REST APIs |  [optional]
**tlsKeyPath** | **String** | The TLS key file to use for the REST APIs |  [optional]
