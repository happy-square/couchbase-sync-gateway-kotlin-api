# KeyspaceBulkDocsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newEdits** | **Boolean** | This controls whether to assign new revision identifiers to new edits (&#x60;true&#x60;) or use the existing ones (&#x60;false&#x60;). |  [optional]
**docs** | [**List&lt;KeyspaceBody&gt;**](KeyspaceBody.md) |  | 
