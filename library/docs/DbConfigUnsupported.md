# DbConfigUnsupported

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userViews** | [**DbConfigUnsupportedUserViews**](DbConfigUnsupportedUserViews.md) |  |  [optional]
**oidcTestProvider** | [**DbConfigUnsupportedOidcTestProvider**](DbConfigUnsupportedOidcTestProvider.md) |  |  [optional]
**apiEndpoints** | [**DbConfigUnsupportedApiEndpoints**](DbConfigUnsupportedApiEndpoints.md) |  |  [optional]
**warningThresholds** | [**DbConfigUnsupportedWarningThresholds**](DbConfigUnsupportedWarningThresholds.md) |  |  [optional]
**oidcTlsSkipVerify** | **Boolean** | Enable self-signed certificates for OIDC testing. |  [optional]
**sgrTlsSkipVerify** | **Boolean** | Enable self-signed certificates for SG-replicate testing. |  [optional]
**remoteConfigTlsSkipVerify** | **Boolean** | Enable self-signed certificates for external JavaScript load. |  [optional]
**guestReadOnly** | **Boolean** | Restrict GUEST document access to read-only. |  [optional]
**forceApiForbiddenErrors** | **Boolean** | Force REST API errors to return forbidden |  [optional]
**dcpReadBuffer** | [**BigDecimal**](BigDecimal.md) | Set the dcp feed to use a different read buffer size. |  [optional]
**kvBuffer** | [**BigDecimal**](BigDecimal.md) | Set the kv pool to use a different buffer size. |  [optional]
