# LocalDocidBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_rev** | **String** | Revision to replace. Required if updating existing local document. |  [optional]
