# DbConfigCacheChannelCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxNumber** | **Integer** | The maximum number of channel caches which can exist at any one point. |  [optional]
**compactHighWatermarkPct** | **Integer** | The trigger value for starting the channel cache eviction process.  Specify this as a percentage which will be the percentage used on &#x60;max_number).  When the cache size, determined by &#x60;max_number&#x60;, reaches the high watermark, the eviction process iterates through the cache, removing inactive channels. |  [optional]
**compactLowWatermarkPct** | **Integer** | The trigger value for stopping the channel cache eviction process.  Specify this as a percentage which will be the percentage used on &#x60;max_number).  When the cache size, determined by &#x60;max_number&#x60; returns to a value lower than the percentage of it set here, the cache eviction process is stopped. |  [optional]
**maxWaitPending** | [**BigDecimal**](BigDecimal.md) | The maximum time (in milliseconds) for waiting for a pending sequence before skipping it. |  [optional]
**maxNumPending** | **Integer** | The maximum number of pending sequences before skipping sequences. |  [optional]
**maxWaitSkipped** | [**BigDecimal**](BigDecimal.md) | The maximum amount of time (in milliseconds) to wait for a skipped sequence before abandoning it. |  [optional]
**enableStarChannel** | **Boolean** | Used to control whether Sync Gateway should use the all documents (*) channel. |  [optional]
**maxLength** | **Integer** | The maximum number of entries to maintain in the cache per channel. |  [optional]
**minLength** | **Integer** | The minimum number of entries to maintain in the cache per channel. |  [optional]
**expirySeconds** | **Integer** | The amount of time (in seconds) to keep entries in the cache beyond the minimum retained. |  [optional]
**queryLimit** | **Integer** | **Deprecated in favour of the database setting &#x60;query_pagination_limit&#x60;**  The limit used for channel queries. |  [optional]
