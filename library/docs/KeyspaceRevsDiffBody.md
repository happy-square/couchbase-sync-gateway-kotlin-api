# KeyspaceRevsDiffBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**docid** | **List&lt;String&gt;** | The document ID with an array of revisions to use for the comparison. |  [optional]
