# InlineResponse2009Scopes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collections** | [**Map&lt;String, InlineResponse2009Collections&gt;**](InlineResponse2009Collections.md) | The set of collections within the scope. |  [optional]
