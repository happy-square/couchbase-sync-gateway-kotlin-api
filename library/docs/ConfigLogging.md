# ConfigLogging

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logFilePath** | **String** | Absolute or relative path on the filesystem to the log file directory. A relative path is from the directory that contains the Sync Gateway executable file. |  [optional]
**redactionLevel** | [**RedactionLevelEnum**](#RedactionLevelEnum) | Redaction level to apply to log output. |  [optional]
**console** | [**Consoleloggingconfig**](Consoleloggingconfig.md) |  |  [optional]
**error** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]
**warn** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]
**info** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]
**debug** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]
**trace** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]
**stats** | [**Fileloggingconfig**](Fileloggingconfig.md) |  |  [optional]

<a name="RedactionLevelEnum"></a>
## Enum: RedactionLevelEnum
Name | Value
---- | -----
NONE | &quot;none&quot;
PARTIAL | &quot;partial&quot;
FULL | &quot;full&quot;
UNSET | &quot;unset&quot;
