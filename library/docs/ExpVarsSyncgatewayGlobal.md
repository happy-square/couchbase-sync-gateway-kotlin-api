# ExpVarsSyncgatewayGlobal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resourceUtilization** | [**ExpVarsSyncgatewayGlobalResourceUtilization**](ExpVarsSyncgatewayGlobalResourceUtilization.md) |  |  [optional]
