# StartupconfigApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**publicInterface** | **String** | Network interface to bind public API to |  [optional]
**adminInterface** | **String** | Network interface to bind admin API to.  By default, this will only be accessible to the localhost. |  [optional]
**metricInterface** | **String** | Network interface to bind metrics API to.  By default, this will only be accessible to the localhost. |  [optional]
**profileInterface** | **String** | Network interface to bind profiling API to |  [optional]
**adminInterfaceAuthentication** | **Boolean** | Whether the admin API requires authentication |  [optional]
**metricsInterfaceAuthentication** | **Boolean** | Whether the metrics API requires authentication |  [optional]
**enableAdvancedAuthDp** | **Boolean** | Whether to enable the DP permissions check feature of admin auth.  Defaults to &#x60;true&#x60; if using enterprise-edition or &#x60;false&#x60; if using community-edition. |  [optional]
**serverReadTimeout** | **String** | Maximum duration.Second before timing out read of the HTTP(S) request.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**serverWriteTimeout** | **String** | Maximum duration.Second before timing out write of the HTTP(S) response.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**readHeaderTimeout** | **String** | The amount of time allowed to read request headers.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**idleTimeout** | **String** | The maximum amount of time to wait for the next request when keep-alives are enabled.  This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
**pretty** | **Boolean** | Pretty-print JSON responses |  [optional]
**maxConnections** | [**BigDecimal**](BigDecimal.md) | Max of incoming HTTP connections to accept |  [optional]
**compressResponses** | **Boolean** | If false, disables compression of HTTP responses |  [optional]
**hideProductVersion** | **Boolean** | Whether product versions removed from Server headers and REST API responses |  [optional]
**https** | [**StartupconfigApiHttps**](StartupconfigApiHttps.md) |  |  [optional]
**cors** | [**StartupconfigApiCors**](StartupconfigApiCors.md) |  |  [optional]
