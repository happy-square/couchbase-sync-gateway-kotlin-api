# InlineResponse2011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instanceStartTime** | **Integer** | Timestamp of when the database opened, in microseconds since the Unix epoch. |  [optional]
**ok** | **Boolean** |  |  [optional]
