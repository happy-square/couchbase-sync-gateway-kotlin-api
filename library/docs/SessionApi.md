# SessionApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDbSessionSessionid**](SessionApi.md#deleteDbSessionSessionid) | **DELETE** /{db}/_session/{sessionid} | Remove session
[**deleteDbUserNameSession**](SessionApi.md#deleteDbUserNameSession) | **DELETE** /{db}/_user/{name}/_session | Remove all of a users sessions
[**deleteDbUserNameSessionSessionid**](SessionApi.md#deleteDbUserNameSessionSessionid) | **DELETE** /{db}/_user/{name}/_session/{sessionid} | Remove session with user validation
[**getDbSession**](SessionApi.md#getDbSession) | **GET** /{db}/_session | Get information about the current user
[**getDbSessionSessionid**](SessionApi.md#getDbSessionSessionid) | **GET** /{db}/_session/{sessionid} | Get session information
[**headDbSession**](SessionApi.md#headDbSession) | **HEAD** /{db}/_session | /{db}/_session
[**postDbSession**](SessionApi.md#postDbSession) | **POST** /{db}/_session | Create a new user session

<a name="deleteDbSessionSessionid"></a>
# **deleteDbSessionSessionid**
> deleteDbSessionSessionid(db, sessionid)

Remove session

Invalidates the session provided so that anyone using it is logged out and is prevented from future use.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String sessionid = "sessionid_example"; // String | The ID of the session to target.
try {
    apiInstance.deleteDbSessionSessionid(db, sessionid);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#deleteDbSessionSessionid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **sessionid** | **String**| The ID of the session to target. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteDbUserNameSession"></a>
# **deleteDbUserNameSession**
> deleteDbUserNameSession(db, name)

Remove all of a users sessions

Invalidates all the sessions that a user has.  Will still return a &#x60;200&#x60; status code if the user has no sessions.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
try {
    apiInstance.deleteDbUserNameSession(db, name);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#deleteDbUserNameSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteDbUserNameSessionSessionid"></a>
# **deleteDbUserNameSessionSessionid**
> deleteDbUserNameSessionSessionid(db, name, sessionid)

Remove session with user validation

Invalidates the session only if it belongs to the user.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String name = "name_example"; // String | The name of the user.
String sessionid = "sessionid_example"; // String | The ID of the session to target.
try {
    apiInstance.deleteDbUserNameSessionSessionid(db, name, sessionid);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#deleteDbUserNameSessionSessionid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **name** | **String**| The name of the user. |
 **sessionid** | **String**| The ID of the session to target. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbSession"></a>
# **getDbSession**
> UserSessionInformation getDbSession(db)

Get information about the current user

This will get the information about the current user.

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    UserSessionInformation result = apiInstance.getDbSession(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#getDbSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**UserSessionInformation**](UserSessionInformation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbSessionSessionid"></a>
# **getDbSessionSessionid**
> UserSessionInformation getDbSessionSessionid(db, sessionid)

Get session information

Retrieve session information such as the user the session belongs too and what channels that user can access.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application * Sync Gateway Application Read Only

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String sessionid = "sessionid_example"; // String | The ID of the session to target.
try {
    UserSessionInformation result = apiInstance.getDbSessionSessionid(db, sessionid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#getDbSessionSessionid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **sessionid** | **String**| The ID of the session to target. |

### Return type

[**UserSessionInformation**](UserSessionInformation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbSession"></a>
# **headDbSession**
> headDbSession(db)

/{db}/_session

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDbSession(db);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#headDbSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbSession"></a>
# **postDbSession**
> InlineResponse200 postDbSession(db, body)

Create a new user session

Generates a login session for a user and returns the session ID and cookie name for that session. If no TTL is provided, then the default of 24 hours will be used.  A session cannot be generated for an non-existent user or the &#x60;GUEST&#x60; user.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.SessionApi;


SessionApi apiInstance = new SessionApi();
String db = "db_example"; // String | The name of the database to run the operation against.
DbSessionBody body = new DbSessionBody(); // DbSessionBody | The body can depend on if using the Public or Admin APIs.
try {
    InlineResponse200 result = apiInstance.postDbSession(db, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#postDbSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**DbSessionBody**](DbSessionBody.md)| The body can depend on if using the Public or Admin APIs. | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

