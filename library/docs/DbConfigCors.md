# DbConfigCors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origin** | **List&lt;String&gt;** | List of allowed origins, use [&#x27;*&#x27;] to allow access from everywhere |  [optional]
**loginOrigin** | **List&lt;String&gt;** | List of allowed login origins |  [optional]
**headers** | **List&lt;String&gt;** | List of allowed headers |  [optional]
