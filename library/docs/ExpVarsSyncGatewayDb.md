# ExpVarsSyncGatewayDb

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelChangesFeeds** | **Object** | Number of calls to db.changesFeed, i.e. generating a changes feed for a single channel. |  [optional]
**channelLogAdds** | **Object** | Number of entries added to channel logs |  [optional]
**channelLogAppends** | **Object** | Number of times entries were written to channel logs using an APPEND operation |  [optional]
**channelLogCacheHits** | **Object** | Number of requests for channel-logs that were fulfilled from the in-memory cache |  [optional]
**channelLogRewrites** | **Object** | Number of times entries were written to channel logs using a SET operation (rewriting the entire log) |  [optional]
**channelLogRewriteCollisions** | **Object** | Number of collisions while attempting to rewrite channel logs using SET |  [optional]
**documentGets** | **Object** | Number of times a document was read from the database |  [optional]
**revisionCacheAdds** | **Object** | Number of revisions added to the revision cache |  [optional]
**revisionCacheHits** | **Object** | Number of times a revision-cache lookup succeeded |  [optional]
**revisionCacheMisses** | **Object** | Number of times a revision-cache lookup failed |  [optional]
**revsAdded** | **Object** | Number of revisions added to the database (including deletions) |  [optional]
**sequenceGets** | **Object** | Number of times the database&#x27;s lastSequence was read |  [optional]
**sequenceReserves** | **Object** | Number of times the database&#x27;s lastSequence was incremented |  [optional]
