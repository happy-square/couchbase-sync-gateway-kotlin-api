# InlineResponse2008

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalRows** | **Integer** |  | 
**rows** | [**List&lt;InlineResponse2008Rows&gt;**](InlineResponse2008Rows.md) |  | 
**errors** | [**List&lt;InlineResponse2008Errors&gt;**](InlineResponse2008Errors.md) |  |  [optional]
