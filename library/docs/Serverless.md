# Serverless

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Run SG in to serverless mode |  [optional]
**minConfigFetchInterval** | **String** | How long database configs should be kept for in Sync Gateway before refreshing. Set to 0 to fetch configs everytime. This is used for requested databases that SG does not know about.   This is a duration and therefore can be provided with units \&quot;h\&quot;, \&quot;m\&quot;, \&quot;s\&quot;, \&quot;ms\&quot;, \&quot;us\&quot;, and \&quot;ns\&quot;. For example, 5 hours, 20 minutes, and 30 seconds would be &#x60;5h20m30s&#x60;. |  [optional]
