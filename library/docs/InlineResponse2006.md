# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postUpgradeResults** | [**Map&lt;String, InlineResponse2006PostUpgradeResults&gt;**](InlineResponse2006PostUpgradeResults.md) | A map of databases. | 
**preview** | **Boolean** | If set, nothing in the database was changed as this was a dry-run. This can be controlled by the &#x60;preview&#x60; query parameter in the request. |  [optional]
