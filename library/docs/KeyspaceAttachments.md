# KeyspaceAttachments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentType** | **String** | Content type of the attachment. |  [optional]
**data** | **String** | The data in the attachment in base64. |  [optional]
