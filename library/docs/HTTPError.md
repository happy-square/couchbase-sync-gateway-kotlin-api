# HTTPError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **String** | The error name. | 
**reason** | **String** | The error description. | 
