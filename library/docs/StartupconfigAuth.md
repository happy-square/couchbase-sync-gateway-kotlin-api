# StartupconfigAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bcryptCost** | **Integer** | Cost to use for bcrypt password hashes |  [optional]
