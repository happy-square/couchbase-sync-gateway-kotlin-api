# InlineResponse2001Sync

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rev** | **String** | The current document revision ID. |  [optional]
**sequence** | [**BigDecimal**](BigDecimal.md) | The most recent sequence number of the document. |  [optional]
**recentSequences** | [**List&lt;BigDecimal&gt;**](BigDecimal.md) | The previous sequence numbers of the document. |  [optional]
**history** | [**InlineResponse2001SyncHistory**](InlineResponse2001SyncHistory.md) |  |  [optional]
**cas** | **String** | The document CAS (Concurrent Document Mutations) number used for document locking. |  [optional]
**valueCrc32c** | **String** | The documents CRC32 number. |  [optional]
**channelSet** | [**List&lt;InlineResponse2001SyncChannelSet&gt;**](InlineResponse2001SyncChannelSet.md) | The channels the document has been in. |  [optional]
**channelSetHistory** | [**List&lt;InlineResponse2001SyncChannelSetHistory&gt;**](InlineResponse2001SyncChannelSetHistory.md) |  |  [optional]
**timeSaved** | **String** | The time and date the document was most recently changed. |  [optional]
