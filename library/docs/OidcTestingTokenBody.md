# OidcTestingTokenBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grantType** | **String** | The grant type of the token to request. Can either be an &#x60;authorization_code&#x60; or &#x60;refresh_token&#x60;. | 
**code** | **String** | **&#x60;grant_type&#x3D;authorization_code&#x60; only**: The OpenID Connect authentication token. |  [optional]
**refreshToken** | **String** | **&#x60;grant_type&#x3D;refresh_token&#x60; only**: The OpenID Connect refresh token. |  [optional]
