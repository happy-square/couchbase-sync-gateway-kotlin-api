# InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_sync** | [**InlineResponse2001Sync**](InlineResponse2001Sync.md) |  |  [optional]
