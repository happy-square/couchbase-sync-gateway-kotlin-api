# InlineResponse20015Docid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**missing** | **List&lt;String&gt;** | The revisions that are not in the database (and therefore &#x60;missing&#x60;). |  [optional]
**possibleAncestors** | **List&lt;String&gt;** | An array of known revisions that might be the recent ancestors. |  [optional]
