# ExpVarsSyncgatewayPerDb

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cache** | **Object** |  |  [optional]
**database** | **Object** |  |  [optional]
**perReplication** | **Object** |  |  [optional]
**collections** | **Object** |  |  [optional]
**security** | **Object** |  |  [optional]
