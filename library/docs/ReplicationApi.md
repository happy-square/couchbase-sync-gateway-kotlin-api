# ReplicationApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDbReplicationReplicationid**](ReplicationApi.md#deleteDbReplicationReplicationid) | **DELETE** /{db}/_replication/{replicationid} | Stop and delete a replication
[**getDbBlipsync**](ReplicationApi.md#getDbBlipsync) | **GET** /{db}/_blipsync | Handle incoming BLIP Sync web socket request
[**getDbReplication**](ReplicationApi.md#getDbReplication) | **GET** /{db}/_replication/ | Get all replication configurations
[**getDbReplicationReplicationid**](ReplicationApi.md#getDbReplicationReplicationid) | **GET** /{db}/_replication/{replicationid} | Get a replication configuration
[**getDbReplicationStatus**](ReplicationApi.md#getDbReplicationStatus) | **GET** /{db}/_replicationStatus/ | Get all replication statuses
[**getDbReplicationStatusReplicationid**](ReplicationApi.md#getDbReplicationStatusReplicationid) | **GET** /{db}/_replicationStatus/{replicationid} | Get replication status
[**headDbReplication**](ReplicationApi.md#headDbReplication) | **HEAD** /{db}/_replication/ | /{db}/_replication/
[**headDbReplicationReplicationid**](ReplicationApi.md#headDbReplicationReplicationid) | **HEAD** /{db}/_replication/{replicationid} | Check if a replication exists
[**headDbReplicationStatus**](ReplicationApi.md#headDbReplicationStatus) | **HEAD** /{db}/_replicationStatus/ | /{db}/_replicationStatus/
[**headDbReplicationStatusReplicationid**](ReplicationApi.md#headDbReplicationStatusReplicationid) | **HEAD** /{db}/_replicationStatus/{replicationid} | Check if replication exists
[**postDbReplication**](ReplicationApi.md#postDbReplication) | **POST** /{db}/_replication/ | Upsert a replication
[**putDbReplicationReplicationid**](ReplicationApi.md#putDbReplicationReplicationid) | **PUT** /{db}/_replication/{replicationid} | Upsert a replication
[**putDbReplicationStatusReplicationid**](ReplicationApi.md#putDbReplicationStatusReplicationid) | **PUT** /{db}/_replicationStatus/{replicationid} | Control a replication state

<a name="deleteDbReplicationReplicationid"></a>
# **deleteDbReplicationReplicationid**
> deleteDbReplicationReplicationid(db, replicationid)

Stop and delete a replication

This will delete a replication causing it to stop and no longer exist.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
try {
    apiInstance.deleteDbReplicationReplicationid(db, replicationid);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#deleteDbReplicationReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbBlipsync"></a>
# **getDbBlipsync**
> getDbBlipsync(db, client)

Handle incoming BLIP Sync web socket request

This handles incoming BLIP Sync requests from either Couchbase Lite or another Sync Gateway node. The connection has to be upgradable to a websocket connection or else the request will fail.  Required Sync Gateway RBAC roles:  * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String client = "cbl2"; // String | This is the client type that is making the BLIP Sync request. Used to control client-type specific replication behaviour.
try {
    apiInstance.getDbBlipsync(db, client);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#getDbBlipsync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **client** | **String**| This is the client type that is making the BLIP Sync request. Used to control client-type specific replication behaviour. | [optional] [default to cbl2] [enum: cbl2, sgr2]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbReplication"></a>
# **getDbReplication**
> AllReplications getDbReplication(db)

Get all replication configurations

This will retrieve all database replication definitions.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    AllReplications result = apiInstance.getDbReplication(db);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#getDbReplication");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

[**AllReplications**](AllReplications.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbReplicationReplicationid"></a>
# **getDbReplicationReplicationid**
> Replication1 getDbReplicationReplicationid(db, replicationid)

Get a replication configuration

Retrieve a replication configuration from the database.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
try {
    Replication1 result = apiInstance.getDbReplicationReplicationid(db, replicationid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#getDbReplicationReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |

### Return type

[**Replication1**](Replication1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbReplicationStatus"></a>
# **getDbReplicationStatus**
> List&lt;Replicationstatus&gt; getDbReplicationStatus(db, activeOnly, localOnly, includeError, includeConfig)

Get all replication statuses

Retrieve all the replication statuses in the Sync Gateway node.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Boolean activeOnly = false; // Boolean | Only return replications that are actively running (`state=running`).
Boolean localOnly = false; // Boolean | Only return replications that were started on the current Sync Gateway node.
Boolean includeError = true; // Boolean | Include replications that have stopped due to an error (`state=error`).
Boolean includeConfig = false; // Boolean | Include the replication configuration with each replicator status in the response.
try {
    List<Replicationstatus> result = apiInstance.getDbReplicationStatus(db, activeOnly, localOnly, includeError, includeConfig);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#getDbReplicationStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **activeOnly** | **Boolean**| Only return replications that are actively running (&#x60;state&#x3D;running&#x60;). | [optional] [default to false]
 **localOnly** | **Boolean**| Only return replications that were started on the current Sync Gateway node. | [optional] [default to false]
 **includeError** | **Boolean**| Include replications that have stopped due to an error (&#x60;state&#x3D;error&#x60;). | [optional] [default to true]
 **includeConfig** | **Boolean**| Include the replication configuration with each replicator status in the response. | [optional] [default to false]

### Return type

[**List&lt;Replicationstatus&gt;**](Replicationstatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbReplicationStatusReplicationid"></a>
# **getDbReplicationStatusReplicationid**
> Replicationstatus1 getDbReplicationStatusReplicationid(db, replicationid, activeOnly, localOnly, includeError, includeConfig)

Get replication status

Retrieve the status of a replication.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
Boolean activeOnly = false; // Boolean | Only return replications that are actively running (`state=running`).
Boolean localOnly = false; // Boolean | Only return replications that were started on the current Sync Gateway node.
Boolean includeError = true; // Boolean | Include replications that have stopped due to an error (`state=error`).
Boolean includeConfig = false; // Boolean | Include the replication configuration with each replicator status in the response.
try {
    Replicationstatus1 result = apiInstance.getDbReplicationStatusReplicationid(db, replicationid, activeOnly, localOnly, includeError, includeConfig);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#getDbReplicationStatusReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |
 **activeOnly** | **Boolean**| Only return replications that are actively running (&#x60;state&#x3D;running&#x60;). | [optional] [default to false]
 **localOnly** | **Boolean**| Only return replications that were started on the current Sync Gateway node. | [optional] [default to false]
 **includeError** | **Boolean**| Include replications that have stopped due to an error (&#x60;state&#x3D;error&#x60;). | [optional] [default to true]
 **includeConfig** | **Boolean**| Include the replication configuration with each replicator status in the response. | [optional] [default to false]

### Return type

[**Replicationstatus1**](Replicationstatus1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="headDbReplication"></a>
# **headDbReplication**
> headDbReplication(db)

/{db}/_replication/

Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDbReplication(db);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#headDbReplication");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="headDbReplicationReplicationid"></a>
# **headDbReplicationReplicationid**
> headDbReplicationReplicationid(db, replicationid)

Check if a replication exists

Check if a replication exists.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
try {
    apiInstance.headDbReplicationReplicationid(db, replicationid);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#headDbReplicationReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="headDbReplicationStatus"></a>
# **headDbReplicationStatus**
> headDbReplicationStatus(db)

/{db}/_replicationStatus/

Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
try {
    apiInstance.headDbReplicationStatus(db);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#headDbReplicationStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="headDbReplicationStatusReplicationid"></a>
# **headDbReplicationStatusReplicationid**
> headDbReplicationStatusReplicationid(db, replicationid, activeOnly, localOnly, includeError, includeConfig)

Check if replication exists

Check if a replication exists.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
Boolean activeOnly = false; // Boolean | Only return replications that are actively running (`state=running`).
Boolean localOnly = false; // Boolean | Only return replications that were started on the current Sync Gateway node.
Boolean includeError = true; // Boolean | Include replications that have stopped due to an error (`state=error`).
Boolean includeConfig = false; // Boolean | Include the replication configuration with each replicator status in the response.
try {
    apiInstance.headDbReplicationStatusReplicationid(db, replicationid, activeOnly, localOnly, includeError, includeConfig);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#headDbReplicationStatusReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |
 **activeOnly** | **Boolean**| Only return replications that are actively running (&#x60;state&#x3D;running&#x60;). | [optional] [default to false]
 **localOnly** | **Boolean**| Only return replications that were started on the current Sync Gateway node. | [optional] [default to false]
 **includeError** | **Boolean**| Include replications that have stopped due to an error (&#x60;state&#x3D;error&#x60;). | [optional] [default to true]
 **includeConfig** | **Boolean**| Include the replication configuration with each replicator status in the response. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postDbReplication"></a>
# **postDbReplication**
> postDbReplication(db, body)

Upsert a replication

Create or update a replication in the database.  If an existing replication is being updated, that replication must be stopped first.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
UserConfigurableReplicationProperties body = new UserConfigurableReplicationProperties(); // UserConfigurableReplicationProperties | If the `replication_id` matches an existing replication then the existing configuration will be updated. Only the specified fields in the request will be used to update the existing configuration. Unspecified fields will remain untouched.
try {
    apiInstance.postDbReplication(db, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#postDbReplication");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**UserConfigurableReplicationProperties**](UserConfigurableReplicationProperties.md)| If the &#x60;replication_id&#x60; matches an existing replication then the existing configuration will be updated. Only the specified fields in the request will be used to update the existing configuration. Unspecified fields will remain untouched. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDbReplicationReplicationid"></a>
# **putDbReplicationReplicationid**
> putDbReplicationReplicationid(db, replicationid, body)

Upsert a replication

Create or update a replication in the database.  The replication ID does **not** need to be set in the request body.  If an existing replication is being updated, that replication must be stopped first and, if the &#x60;replication_id&#x60; is specified in the request body, it must match the replication ID in the URI.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
UserConfigurableReplicationProperties1 body = new UserConfigurableReplicationProperties1(); // UserConfigurableReplicationProperties1 | If the `replication_id` matches an existing replication then the existing configuration will be updated. Only the specified fields in the request will be used to update the existing configuration. Unspecified fields will remain untouched.
try {
    apiInstance.putDbReplicationReplicationid(db, replicationid, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#putDbReplicationReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |
 **body** | [**UserConfigurableReplicationProperties1**](UserConfigurableReplicationProperties1.md)| If the &#x60;replication_id&#x60; matches an existing replication then the existing configuration will be updated. Only the specified fields in the request will be used to update the existing configuration. Unspecified fields will remain untouched. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDbReplicationStatusReplicationid"></a>
# **putDbReplicationStatusReplicationid**
> Replicationstatus1 putDbReplicationStatusReplicationid(db, replicationid, action)

Control a replication state

Control the replication by changing its state.  This is done through the action query parameter, which has 3 valid values: * &#x60;start&#x60; - starts a stopped replication * &#x60;stop&#x60; - stops an active replication * &#x60;reset&#x60; - resets the replication checkpoint to 0. For bidirectional replication, both push and pull checkpoints are reset to 0. The replication must be stopped to use this.  Required Sync Gateway RBAC roles:  * Sync Gateway Replicator

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.ReplicationApi;


ReplicationApi apiInstance = new ReplicationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
String replicationid = "replicationid_example"; // String | What replication to target based on its replication ID.
String action = "action_example"; // String | The target state to put the replicator into.
try {
    Replicationstatus1 result = apiInstance.putDbReplicationStatusReplicationid(db, replicationid, action);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReplicationApi#putDbReplicationStatusReplicationid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **replicationid** | **String**| What replication to target based on its replication ID. |
 **action** | **String**| The target state to put the replicator into. | [enum: start, stop, reset]

### Return type

[**Replicationstatus1**](Replicationstatus1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

