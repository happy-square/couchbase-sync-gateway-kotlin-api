# OidcTestingAuthenticateBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**tokenttl** | **String** |  | 
**identityTokenFormats** | **String** |  | 
**authenticated** | **String** |  | 
