# OpenIDConnectCallbackProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idToken** | **String** | The OpenID Connect ID token |  [optional]
**refreshToken** | **String** | The OpenID Connect ID refresh token |  [optional]
**sessionId** | **String** | The Sync Gateway session token |  [optional]
**name** | **String** | The Sync Gateway user |  [optional]
**accessToken** | **String** | The OpenID Connect access token |  [optional]
**tokenType** | **String** | The OpenID Connect ID token type |  [optional]
**expiresIn** | [**BigDecimal**](BigDecimal.md) | The time until the id_token expires (TTL). |  [optional]
