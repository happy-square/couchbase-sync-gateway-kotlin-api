# DbSessionBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | User name to generate the session for. |  [optional]
**ttl** | **Integer** | Time until the session expires. Uses default value of 24 hours if left blank. |  [optional]
