# DbOnlineBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delay** | **Integer** | The amount of seconds to delay bringing the database online. |  [optional]
