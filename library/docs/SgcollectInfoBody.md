# SgcollectInfoBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redactLevel** | [**RedactLevelEnum**](#RedactLevelEnum) | The redaction level to use for redacting the collected logs. |  [optional]
**redactSalt** | **String** | The salt to use for the log redactions. |  [optional]
**outputDir** | **String** | The directory to output the collected logs zip file at.  This overrides the configured default output directory configured in the startup config &#x60;logging.log_file_path&#x60;. |  [optional]
**upload** | **Boolean** | If set, upload the logs to Couchbase Support.  A customer name must be set if this is set. |  [optional]
**uploadHost** | **String** | The host to send the logs too. |  [optional]
**uploadProxy** | **String** | The proxy to use while uploading the logs. |  [optional]
**customer** | **String** | The customer name to use when uploading the logs. |  [optional]
**ticket** | **String** | The Zendesk ticket number to use when uploading logs. |  [optional]

<a name="RedactLevelEnum"></a>
## Enum: RedactLevelEnum
Name | Value
---- | -----
PARTIAL | &quot;partial&quot;
NONE | &quot;none&quot;
