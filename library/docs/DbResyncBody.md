# DbResyncBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scopes** | [**Map&lt;String, List&lt;String&gt;&gt;**](List.md) | This controls for which collections resync will run |  [optional]
**regenerateSequences** | **Boolean** | This can be used as an alternative to query param &#x60;regenerate_sequences&#x60;. If either query param or this is set to true, then the request will regenerate the sequence numbers for each document processed. |  [optional]
