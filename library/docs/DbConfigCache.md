# DbConfigCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revCache** | [**DbConfigCacheRevCache**](DbConfigCacheRevCache.md) |  |  [optional]
**channelCache** | [**DbConfigCacheChannelCache**](DbConfigCacheChannelCache.md) |  |  [optional]
**maxWaitPending** | [**BigDecimal**](BigDecimal.md) | **Deprecated, please use the database setting &#x60;cache.channel_cache.max_wait_pending&#x60; instead**  The maximum time (in milliseconds) for waiting for a pending sequence before skipping it. |  [optional]
**maxWaitSkipped** | [**BigDecimal**](BigDecimal.md) | **Deprecated, please use the database setting &#x60;cache.channel_cache.max_wait_skipped&#x60; instead**  The maximum time (in milliseconds) for waiting for pending sequences before skipping. |  [optional]
**enableStarChannel** | **Boolean** | **Deprecated, please use the database setting &#x60;cache.channel_cache.enable_star_channel&#x60; instead**  Used to control whether Sync Gateway should use the all documents (*) channel. |  [optional]
**channelCacheMaxLength** | [**BigDecimal**](BigDecimal.md) | **Deprecated, please use the database setting &#x60;cache.channel_cache.max_length&#x60; instead**  The maximum number of entries maintained in cache per channel. |  [optional]
**channelCacheMinLength** | **Integer** | **Deprecated, please use the database setting &#x60;cache.channel_cache.min_length&#x60; instead**  The minimum number of entries maintained in cache per channel. |  [optional]
**channelCacheExpiry** | **Integer** | **Deprecated, please use the database setting &#x60;cache.channel_cache.expiry_seconds&#x60; instead**  The time (seconds) to keep entries in cache beyond the minimum retained. |  [optional]
**maxNumPending** | **Integer** | **Deprecated, please use the database setting &#x60;cache.channel_cache.max_num_pending&#x60; instead**  The max number of pending sequences before skipping. |  [optional]
