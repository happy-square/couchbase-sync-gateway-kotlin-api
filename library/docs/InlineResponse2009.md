# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dbName** | **String** | Database name |  [optional]
**updateSeq** | **Integer** | The last sequence number that was committed to the database.  Will return 0 if the database is offline. |  [optional]
**committedUpdateSeq** | **Integer** | The last sequence number that was committed to the database.  Will return 0 if the database is offline. |  [optional]
**instanceStartTime** | **Integer** | Timestamp of when the database opened, in microseconds since the Unix epoch. |  [optional]
**compactRunning** | **Boolean** | Indicates whether database compaction is currently taking place or not. |  [optional]
**purgeSeq** | [**BigDecimal**](BigDecimal.md) | Unused field. |  [optional]
**diskFormatVersion** | [**BigDecimal**](BigDecimal.md) | Unused field. |  [optional]
**state** | [**StateEnum**](#StateEnum) | The database state. Change using the &#x60;/{db}/_offline&#x60; and &#x60;/{db}/_online&#x60; endpoints. |  [optional]
**serverUuid** | **String** | Unique server identifier. |  [optional]
**scopes** | [**Map&lt;String, InlineResponse2009Scopes&gt;**](InlineResponse2009Scopes.md) | Scopes that are used by the database. |  [optional]

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
ONLINE | &quot;Online&quot;
OFFLINE | &quot;Offline&quot;
