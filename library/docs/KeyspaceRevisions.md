# KeyspaceRevisions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | [**BigDecimal**](BigDecimal.md) | Prefix number for the latest revision. |  [optional]
**ids** | **List&lt;String&gt;** | Array of valid revision IDs, in reverse order (latest first). |  [optional]
