# DbConfigLocalJwt

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuer** | **String** | The value to match against the \&quot;iss\&quot; claim of JWTs. | 
**register** | **Boolean** | If to register a new Sync Gateway user account when a user logs in with a JWT. |  [optional]
**clientId** | **String** | The value to match against the \&quot;aud\&quot; claim of JWTs. Set to an empty string to disable audience validation. | 
**algorithms** | **List&lt;String&gt;** | The JWT signing algorithms to accept for authentication. | 
**keys** | [**List&lt;DbConfigKeys&gt;**](DbConfigKeys.md) | The JSON Web Keys to use to validate JWTs. | 
**disableSession** | **Boolean** | Disable Sync Gateway session creation on successful JWT authentication. |  [optional]
**userPrefix** | **String** | This is the username prefix for all users created through this provider. |  [optional]
**usernameClaim** | **String** | Allows a different OpenID Connect field to be specified instead of the Subject (&#x60;sub&#x60;).  The field name to use can be specified here. |  [optional]
**rolesClaim** | **String** | If set, the value(s) of the given JSON Web Token claim will be added to the user&#x27;s roles.  The value of this claim must be either a string or an array of strings, any other type will result in an error. |  [optional]
**channelsClaim** | **String** | If set, the value(s) of the given JSON Web Token claim will be added to the user&#x27;s channels.  The value of this claim must be either a string or an array of strings, any other type will result in an error. |  [optional]
