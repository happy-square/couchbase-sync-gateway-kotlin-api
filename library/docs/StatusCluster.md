# StatusCluster

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replication** | [**StatusClusterReplication**](StatusClusterReplication.md) |  |  [optional]
**nodes** | [**StatusClusterNodes**](StatusClusterNodes.md) |  |  [optional]
