# InlineResponse2001SyncChannelSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the channel. |  [optional]
**start** | **String** | The sequence number that document was added to the channel. |  [optional]
**end** | **String** | The sequence number the document was removed from the channel. Omitted if not removed. |  [optional]
