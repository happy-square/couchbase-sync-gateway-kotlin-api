# InlineResponse20018Keys

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **Object** |  | 
**keyID** | **String** |  | 
**use** | **String** |  | 
**certificates** | **List&lt;Object&gt;** |  |  [optional]
**algorithm** | **String** |  |  [optional]
