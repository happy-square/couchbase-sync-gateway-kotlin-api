# DatabaseConfigurationApi

All URIs are relative to *{protocol}://{hostname}:4985*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteKeyspaceConfigImportFilter**](DatabaseConfigurationApi.md#deleteKeyspaceConfigImportFilter) | **DELETE** /{keyspace}/_config/import_filter | Delete import filter
[**deleteKeyspaceConfigSync**](DatabaseConfigurationApi.md#deleteKeyspaceConfigSync) | **DELETE** /{keyspace}/_config/sync | Remove custom sync function
[**getDbConfig**](DatabaseConfigurationApi.md#getDbConfig) | **GET** /{db}/_config | Get database configuration
[**getKeyspaceConfigImportFilter**](DatabaseConfigurationApi.md#getKeyspaceConfigImportFilter) | **GET** /{keyspace}/_config/import_filter | Get database import filter
[**getKeyspaceConfigSync**](DatabaseConfigurationApi.md#getKeyspaceConfigSync) | **GET** /{keyspace}/_config/sync | Get database sync function
[**postDbConfig**](DatabaseConfigurationApi.md#postDbConfig) | **POST** /{db}/_config | Update database configuration
[**putDbConfig**](DatabaseConfigurationApi.md#putDbConfig) | **PUT** /{db}/_config | Replace database configuration
[**putKeyspaceConfigImportFilter**](DatabaseConfigurationApi.md#putKeyspaceConfigImportFilter) | **PUT** /{keyspace}/_config/import_filter | Set database import filter
[**putKeyspaceConfigSync**](DatabaseConfigurationApi.md#putKeyspaceConfigSync) | **PUT** /{keyspace}/_config/sync | Set database sync function

<a name="deleteKeyspaceConfigImportFilter"></a>
# **deleteKeyspaceConfigImportFilter**
> deleteKeyspaceConfigImportFilter(keyspace, ifMatch)

Delete import filter

This will remove the custom import filter function from the database configuration so that Sync Gateway will not filter any documents during import.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String ifMatch = "ifMatch_example"; // String | If set to a configuration's Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one.
try {
    apiInstance.deleteKeyspaceConfigImportFilter(keyspace, ifMatch);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#deleteKeyspaceConfigImportFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **ifMatch** | **String**| If set to a configuration&#x27;s Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteKeyspaceConfigSync"></a>
# **deleteKeyspaceConfigSync**
> deleteKeyspaceConfigSync(keyspace, ifMatch)

Remove custom sync function

This will remove the custom sync function from the database configuration.  The default sync function is equivalent to: &#x60;&#x60;&#x60;javascript function (doc) {   channel(doc.channels); } &#x60;&#x60;&#x60;  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String ifMatch = "ifMatch_example"; // String | The revision ID to target.
try {
    apiInstance.deleteKeyspaceConfigSync(keyspace, ifMatch);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#deleteKeyspaceConfigSync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **ifMatch** | **String**| The revision ID to target. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDbConfig"></a>
# **getDbConfig**
> Databaseconfig getDbConfig(db, redact, includeJavascript, includeRuntime, refreshConfig)

Get database configuration

Retrieve the full configuration for the database specified.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Boolean redact = true; // Boolean | No longer supported field.
Boolean includeJavascript = true; // Boolean | Include the fields that have Javascript functions in the response. E.g. sync function, import filter, and event handlers.
Boolean includeRuntime = false; // Boolean | Whether to include the values set at runtime, and default values.
Boolean refreshConfig = false; // Boolean | Forces the configuration to be reloaded on the Sync Gateway node.
try {
    Databaseconfig result = apiInstance.getDbConfig(db, redact, includeJavascript, includeRuntime, refreshConfig);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#getDbConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **redact** | **Boolean**| No longer supported field. | [optional] [default to true]
 **includeJavascript** | **Boolean**| Include the fields that have Javascript functions in the response. E.g. sync function, import filter, and event handlers. | [optional] [default to true]
 **includeRuntime** | **Boolean**| Whether to include the values set at runtime, and default values. | [optional] [default to false]
 **refreshConfig** | **Boolean**| Forces the configuration to be reloaded on the Sync Gateway node. | [optional] [default to false]

### Return type

[**Databaseconfig**](Databaseconfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getKeyspaceConfigImportFilter"></a>
# **getKeyspaceConfigImportFilter**
> String getKeyspaceConfigImportFilter(keyspace)

Get database import filter

This returns the database&#x27;s import filter that documents are ran through when importing.  Response will be blank if there has been no import filter set.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
try {
    String result = apiInstance.getKeyspaceConfigImportFilter(keyspace);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#getKeyspaceConfigImportFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/javascript, application/json

<a name="getKeyspaceConfigSync"></a>
# **getKeyspaceConfigSync**
> String getKeyspaceConfigSync(keyspace)

Get database sync function

This returns the database&#x27;s sync function.  Response will be blank if there has been no sync function set.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
try {
    String result = apiInstance.getKeyspaceConfigSync(keyspace);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#getKeyspaceConfigSync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/javascript, application/json

<a name="postDbConfig"></a>
# **postDbConfig**
> postDbConfig(db, body, ifMatch)

Update database configuration

This is used to update the database configuration fields specified. Only the fields specified in the request will have their values replaced.  The bucket and database name cannot be changed. If these need to be changed, the database will need to be deleted then recreated with the new settings.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Databaseconfig2 body = new Databaseconfig2(); // Databaseconfig2 | The database configuration fields to update
String ifMatch = "ifMatch_example"; // String | If set to a configuration's Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one.
try {
    apiInstance.postDbConfig(db, body, ifMatch);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#postDbConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**Databaseconfig2**](Databaseconfig2.md)| The database configuration fields to update | [optional]
 **ifMatch** | **String**| If set to a configuration&#x27;s Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putDbConfig"></a>
# **putDbConfig**
> putDbConfig(db, body, ifMatch, disableOidcValidation)

Replace database configuration

Replaces the database configuration with the one sent in the request.  The bucket and database name cannot be changed. If these need to be changed, the database will need to be deleted then recreated with the new settings.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect * Sync Gateway Application

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String db = "db_example"; // String | The name of the database to run the operation against.
Databaseconfig1 body = new Databaseconfig1(); // Databaseconfig1 | The new database configuration to use
String ifMatch = "ifMatch_example"; // String | If set to a configuration's Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one.
Boolean disableOidcValidation = false; // Boolean | If set, will not attempt to validate the configured OpenID Connect providers are reachable.
try {
    apiInstance.putDbConfig(db, body, ifMatch, disableOidcValidation);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#putDbConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **db** | **String**| The name of the database to run the operation against. |
 **body** | [**Databaseconfig1**](Databaseconfig1.md)| The new database configuration to use | [optional]
 **ifMatch** | **String**| If set to a configuration&#x27;s Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one. | [optional]
 **disableOidcValidation** | **Boolean**| If set, will not attempt to validate the configured OpenID Connect providers are reachable. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putKeyspaceConfigImportFilter"></a>
# **putKeyspaceConfigImportFilter**
> putKeyspaceConfigImportFilter(keyspace, body, ifMatch, disableOidcValidation)

Set database import filter

This will allow you to update the database&#x27;s import filter.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String body = "body_example"; // String | The import filter to use
String ifMatch = "ifMatch_example"; // String | If set to a configuration's Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one.
Boolean disableOidcValidation = false; // Boolean | If set, will not attempt to validate the configured OpenID Connect providers are reachable.
try {
    apiInstance.putKeyspaceConfigImportFilter(keyspace, body, ifMatch, disableOidcValidation);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#putKeyspaceConfigImportFilter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**String**](String.md)| The import filter to use | [optional]
 **ifMatch** | **String**| If set to a configuration&#x27;s Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one. | [optional]
 **disableOidcValidation** | **Boolean**| If set, will not attempt to validate the configured OpenID Connect providers are reachable. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/javascript
 - **Accept**: application/json

<a name="putKeyspaceConfigSync"></a>
# **putKeyspaceConfigSync**
> putKeyspaceConfigSync(keyspace, body, ifMatch, disableOidcValidation)

Set database sync function

This will allow you to update the sync function.  Required Sync Gateway RBAC roles:  * Sync Gateway Architect

### Example
```java
// Import classes:
//import de.happysquare.couchbase.sg.ApiException;
//import de.happysquare.couchbase.sg.api.DatabaseConfigurationApi;


DatabaseConfigurationApi apiInstance = new DatabaseConfigurationApi();
String keyspace = "keyspace_example"; // String | The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection.
String body = "\"function (doc, oldDoc) {\\n  channel(doc.channels);\\n}\""; // String | The new sync function to use
String ifMatch = "ifMatch_example"; // String | If set to a configuration's Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one.
Boolean disableOidcValidation = false; // Boolean | If set, will not attempt to validate the configured OpenID Connect providers are reachable.
try {
    apiInstance.putKeyspaceConfigSync(keyspace, body, ifMatch, disableOidcValidation);
} catch (ApiException e) {
    System.err.println("Exception when calling DatabaseConfigurationApi#putKeyspaceConfigSync");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyspace** | **String**| The keyspace to run the operation against.  A keyspace is a dot-separated string, comprised of a database name, and optionally a named scope and collection. |
 **body** | [**String**](String.md)| The new sync function to use | [optional]
 **ifMatch** | **String**| If set to a configuration&#x27;s Etag value, enables optimistic concurrency control for the request. Returns HTTP 412 if another update happened underneath this one. | [optional]
 **disableOidcValidation** | **Boolean**| If set, will not attempt to validate the configured OpenID Connect providers are reachable. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/javascript
 - **Accept**: application/json

