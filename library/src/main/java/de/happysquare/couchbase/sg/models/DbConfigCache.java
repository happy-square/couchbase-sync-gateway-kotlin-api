/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.happysquare.couchbase.sg.models.DbConfigCacheChannelCache;
import de.happysquare.couchbase.sg.models.DbConfigCacheRevCache;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.math.BigDecimal;
/**
 * DbConfigCache
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class DbConfigCache {
  @SerializedName("rev_cache")
  private DbConfigCacheRevCache revCache = null;

  @SerializedName("channel_cache")
  private DbConfigCacheChannelCache channelCache = null;

  @SerializedName("max_wait_pending")
  private BigDecimal maxWaitPending = null;

  @SerializedName("max_wait_skipped")
  private BigDecimal maxWaitSkipped = null;

  @SerializedName("enable_star_channel")
  private Boolean enableStarChannel = null;

  @SerializedName("channel_cache_max_length")
  private BigDecimal channelCacheMaxLength = null;

  @SerializedName("channel_cache_min_length")
  private Integer channelCacheMinLength = null;

  @SerializedName("channel_cache_expiry")
  private Integer channelCacheExpiry = null;

  @SerializedName("max_num_pending")
  private Integer maxNumPending = null;

  public DbConfigCache revCache(DbConfigCacheRevCache revCache) {
    this.revCache = revCache;
    return this;
  }

   /**
   * Get revCache
   * @return revCache
  **/
  @Schema(description = "")
  public DbConfigCacheRevCache getRevCache() {
    return revCache;
  }

  public void setRevCache(DbConfigCacheRevCache revCache) {
    this.revCache = revCache;
  }

  public DbConfigCache channelCache(DbConfigCacheChannelCache channelCache) {
    this.channelCache = channelCache;
    return this;
  }

   /**
   * Get channelCache
   * @return channelCache
  **/
  @Schema(description = "")
  public DbConfigCacheChannelCache getChannelCache() {
    return channelCache;
  }

  public void setChannelCache(DbConfigCacheChannelCache channelCache) {
    this.channelCache = channelCache;
  }

  public DbConfigCache maxWaitPending(BigDecimal maxWaitPending) {
    this.maxWaitPending = maxWaitPending;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.max_wait_pending&#x60; instead**  The maximum time (in milliseconds) for waiting for a pending sequence before skipping it.
   * @return maxWaitPending
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.max_wait_pending` instead**  The maximum time (in milliseconds) for waiting for a pending sequence before skipping it.")
  public BigDecimal getMaxWaitPending() {
    return maxWaitPending;
  }

  public void setMaxWaitPending(BigDecimal maxWaitPending) {
    this.maxWaitPending = maxWaitPending;
  }

  public DbConfigCache maxWaitSkipped(BigDecimal maxWaitSkipped) {
    this.maxWaitSkipped = maxWaitSkipped;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.max_wait_skipped&#x60; instead**  The maximum time (in milliseconds) for waiting for pending sequences before skipping.
   * @return maxWaitSkipped
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.max_wait_skipped` instead**  The maximum time (in milliseconds) for waiting for pending sequences before skipping.")
  public BigDecimal getMaxWaitSkipped() {
    return maxWaitSkipped;
  }

  public void setMaxWaitSkipped(BigDecimal maxWaitSkipped) {
    this.maxWaitSkipped = maxWaitSkipped;
  }

  public DbConfigCache enableStarChannel(Boolean enableStarChannel) {
    this.enableStarChannel = enableStarChannel;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.enable_star_channel&#x60; instead**  Used to control whether Sync Gateway should use the all documents (*) channel.
   * @return enableStarChannel
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.enable_star_channel` instead**  Used to control whether Sync Gateway should use the all documents (*) channel.")
  public Boolean isEnableStarChannel() {
    return enableStarChannel;
  }

  public void setEnableStarChannel(Boolean enableStarChannel) {
    this.enableStarChannel = enableStarChannel;
  }

  public DbConfigCache channelCacheMaxLength(BigDecimal channelCacheMaxLength) {
    this.channelCacheMaxLength = channelCacheMaxLength;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.max_length&#x60; instead**  The maximum number of entries maintained in cache per channel.
   * @return channelCacheMaxLength
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.max_length` instead**  The maximum number of entries maintained in cache per channel.")
  public BigDecimal getChannelCacheMaxLength() {
    return channelCacheMaxLength;
  }

  public void setChannelCacheMaxLength(BigDecimal channelCacheMaxLength) {
    this.channelCacheMaxLength = channelCacheMaxLength;
  }

  public DbConfigCache channelCacheMinLength(Integer channelCacheMinLength) {
    this.channelCacheMinLength = channelCacheMinLength;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.min_length&#x60; instead**  The minimum number of entries maintained in cache per channel.
   * @return channelCacheMinLength
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.min_length` instead**  The minimum number of entries maintained in cache per channel.")
  public Integer getChannelCacheMinLength() {
    return channelCacheMinLength;
  }

  public void setChannelCacheMinLength(Integer channelCacheMinLength) {
    this.channelCacheMinLength = channelCacheMinLength;
  }

  public DbConfigCache channelCacheExpiry(Integer channelCacheExpiry) {
    this.channelCacheExpiry = channelCacheExpiry;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.expiry_seconds&#x60; instead**  The time (seconds) to keep entries in cache beyond the minimum retained.
   * @return channelCacheExpiry
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.expiry_seconds` instead**  The time (seconds) to keep entries in cache beyond the minimum retained.")
  public Integer getChannelCacheExpiry() {
    return channelCacheExpiry;
  }

  public void setChannelCacheExpiry(Integer channelCacheExpiry) {
    this.channelCacheExpiry = channelCacheExpiry;
  }

  public DbConfigCache maxNumPending(Integer maxNumPending) {
    this.maxNumPending = maxNumPending;
    return this;
  }

   /**
   * **Deprecated, please use the database setting &#x60;cache.channel_cache.max_num_pending&#x60; instead**  The max number of pending sequences before skipping.
   * @return maxNumPending
  **/
  @Schema(description = "**Deprecated, please use the database setting `cache.channel_cache.max_num_pending` instead**  The max number of pending sequences before skipping.")
  public Integer getMaxNumPending() {
    return maxNumPending;
  }

  public void setMaxNumPending(Integer maxNumPending) {
    this.maxNumPending = maxNumPending;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DbConfigCache dbConfigCache = (DbConfigCache) o;
    return Objects.equals(this.revCache, dbConfigCache.revCache) &&
        Objects.equals(this.channelCache, dbConfigCache.channelCache) &&
        Objects.equals(this.maxWaitPending, dbConfigCache.maxWaitPending) &&
        Objects.equals(this.maxWaitSkipped, dbConfigCache.maxWaitSkipped) &&
        Objects.equals(this.enableStarChannel, dbConfigCache.enableStarChannel) &&
        Objects.equals(this.channelCacheMaxLength, dbConfigCache.channelCacheMaxLength) &&
        Objects.equals(this.channelCacheMinLength, dbConfigCache.channelCacheMinLength) &&
        Objects.equals(this.channelCacheExpiry, dbConfigCache.channelCacheExpiry) &&
        Objects.equals(this.maxNumPending, dbConfigCache.maxNumPending);
  }

  @Override
  public int hashCode() {
    return Objects.hash(revCache, channelCache, maxWaitPending, maxWaitSkipped, enableStarChannel, channelCacheMaxLength, channelCacheMinLength, channelCacheExpiry, maxNumPending);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DbConfigCache {\n");
    
    sb.append("    revCache: ").append(toIndentedString(revCache)).append("\n");
    sb.append("    channelCache: ").append(toIndentedString(channelCache)).append("\n");
    sb.append("    maxWaitPending: ").append(toIndentedString(maxWaitPending)).append("\n");
    sb.append("    maxWaitSkipped: ").append(toIndentedString(maxWaitSkipped)).append("\n");
    sb.append("    enableStarChannel: ").append(toIndentedString(enableStarChannel)).append("\n");
    sb.append("    channelCacheMaxLength: ").append(toIndentedString(channelCacheMaxLength)).append("\n");
    sb.append("    channelCacheMinLength: ").append(toIndentedString(channelCacheMinLength)).append("\n");
    sb.append("    channelCacheExpiry: ").append(toIndentedString(channelCacheExpiry)).append("\n");
    sb.append("    maxNumPending: ").append(toIndentedString(maxNumPending)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
