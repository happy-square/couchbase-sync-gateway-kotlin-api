/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * InlineResponse20017
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class InlineResponse20017 {
  @SerializedName("issuer")
  private String issuer = null;

  @SerializedName("authorization_endpoint")
  private String authorizationEndpoint = null;

  @SerializedName("token_endpoint")
  private String tokenEndpoint = null;

  @SerializedName("jwks_uri")
  private String jwksUri = null;

  @SerializedName("userinfo_endpoint")
  private String userinfoEndpoint = null;

  @SerializedName("id_token_signing_alg_values_supported")
  private String idTokenSigningAlgValuesSupported = null;

  @SerializedName("response_types_supported")
  private String responseTypesSupported = null;

  @SerializedName("subject_types_supported")
  private String subjectTypesSupported = null;

  @SerializedName("scopes_supported")
  private String scopesSupported = null;

  @SerializedName("claims_supported")
  private String claimsSupported = null;

  @SerializedName("token_endpoint_auth_methods_supported")
  private String tokenEndpointAuthMethodsSupported = null;

  public InlineResponse20017 issuer(String issuer) {
    this.issuer = issuer;
    return this;
  }

   /**
   * Get issuer
   * @return issuer
  **/
  @Schema(description = "")
  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public InlineResponse20017 authorizationEndpoint(String authorizationEndpoint) {
    this.authorizationEndpoint = authorizationEndpoint;
    return this;
  }

   /**
   * Get authorizationEndpoint
   * @return authorizationEndpoint
  **/
  @Schema(description = "")
  public String getAuthorizationEndpoint() {
    return authorizationEndpoint;
  }

  public void setAuthorizationEndpoint(String authorizationEndpoint) {
    this.authorizationEndpoint = authorizationEndpoint;
  }

  public InlineResponse20017 tokenEndpoint(String tokenEndpoint) {
    this.tokenEndpoint = tokenEndpoint;
    return this;
  }

   /**
   * Get tokenEndpoint
   * @return tokenEndpoint
  **/
  @Schema(description = "")
  public String getTokenEndpoint() {
    return tokenEndpoint;
  }

  public void setTokenEndpoint(String tokenEndpoint) {
    this.tokenEndpoint = tokenEndpoint;
  }

  public InlineResponse20017 jwksUri(String jwksUri) {
    this.jwksUri = jwksUri;
    return this;
  }

   /**
   * Get jwksUri
   * @return jwksUri
  **/
  @Schema(description = "")
  public String getJwksUri() {
    return jwksUri;
  }

  public void setJwksUri(String jwksUri) {
    this.jwksUri = jwksUri;
  }

  public InlineResponse20017 userinfoEndpoint(String userinfoEndpoint) {
    this.userinfoEndpoint = userinfoEndpoint;
    return this;
  }

   /**
   * Get userinfoEndpoint
   * @return userinfoEndpoint
  **/
  @Schema(description = "")
  public String getUserinfoEndpoint() {
    return userinfoEndpoint;
  }

  public void setUserinfoEndpoint(String userinfoEndpoint) {
    this.userinfoEndpoint = userinfoEndpoint;
  }

  public InlineResponse20017 idTokenSigningAlgValuesSupported(String idTokenSigningAlgValuesSupported) {
    this.idTokenSigningAlgValuesSupported = idTokenSigningAlgValuesSupported;
    return this;
  }

   /**
   * Get idTokenSigningAlgValuesSupported
   * @return idTokenSigningAlgValuesSupported
  **/
  @Schema(description = "")
  public String getIdTokenSigningAlgValuesSupported() {
    return idTokenSigningAlgValuesSupported;
  }

  public void setIdTokenSigningAlgValuesSupported(String idTokenSigningAlgValuesSupported) {
    this.idTokenSigningAlgValuesSupported = idTokenSigningAlgValuesSupported;
  }

  public InlineResponse20017 responseTypesSupported(String responseTypesSupported) {
    this.responseTypesSupported = responseTypesSupported;
    return this;
  }

   /**
   * Get responseTypesSupported
   * @return responseTypesSupported
  **/
  @Schema(description = "")
  public String getResponseTypesSupported() {
    return responseTypesSupported;
  }

  public void setResponseTypesSupported(String responseTypesSupported) {
    this.responseTypesSupported = responseTypesSupported;
  }

  public InlineResponse20017 subjectTypesSupported(String subjectTypesSupported) {
    this.subjectTypesSupported = subjectTypesSupported;
    return this;
  }

   /**
   * Get subjectTypesSupported
   * @return subjectTypesSupported
  **/
  @Schema(description = "")
  public String getSubjectTypesSupported() {
    return subjectTypesSupported;
  }

  public void setSubjectTypesSupported(String subjectTypesSupported) {
    this.subjectTypesSupported = subjectTypesSupported;
  }

  public InlineResponse20017 scopesSupported(String scopesSupported) {
    this.scopesSupported = scopesSupported;
    return this;
  }

   /**
   * Get scopesSupported
   * @return scopesSupported
  **/
  @Schema(description = "")
  public String getScopesSupported() {
    return scopesSupported;
  }

  public void setScopesSupported(String scopesSupported) {
    this.scopesSupported = scopesSupported;
  }

  public InlineResponse20017 claimsSupported(String claimsSupported) {
    this.claimsSupported = claimsSupported;
    return this;
  }

   /**
   * Get claimsSupported
   * @return claimsSupported
  **/
  @Schema(description = "")
  public String getClaimsSupported() {
    return claimsSupported;
  }

  public void setClaimsSupported(String claimsSupported) {
    this.claimsSupported = claimsSupported;
  }

  public InlineResponse20017 tokenEndpointAuthMethodsSupported(String tokenEndpointAuthMethodsSupported) {
    this.tokenEndpointAuthMethodsSupported = tokenEndpointAuthMethodsSupported;
    return this;
  }

   /**
   * Get tokenEndpointAuthMethodsSupported
   * @return tokenEndpointAuthMethodsSupported
  **/
  @Schema(description = "")
  public String getTokenEndpointAuthMethodsSupported() {
    return tokenEndpointAuthMethodsSupported;
  }

  public void setTokenEndpointAuthMethodsSupported(String tokenEndpointAuthMethodsSupported) {
    this.tokenEndpointAuthMethodsSupported = tokenEndpointAuthMethodsSupported;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse20017 inlineResponse20017 = (InlineResponse20017) o;
    return Objects.equals(this.issuer, inlineResponse20017.issuer) &&
        Objects.equals(this.authorizationEndpoint, inlineResponse20017.authorizationEndpoint) &&
        Objects.equals(this.tokenEndpoint, inlineResponse20017.tokenEndpoint) &&
        Objects.equals(this.jwksUri, inlineResponse20017.jwksUri) &&
        Objects.equals(this.userinfoEndpoint, inlineResponse20017.userinfoEndpoint) &&
        Objects.equals(this.idTokenSigningAlgValuesSupported, inlineResponse20017.idTokenSigningAlgValuesSupported) &&
        Objects.equals(this.responseTypesSupported, inlineResponse20017.responseTypesSupported) &&
        Objects.equals(this.subjectTypesSupported, inlineResponse20017.subjectTypesSupported) &&
        Objects.equals(this.scopesSupported, inlineResponse20017.scopesSupported) &&
        Objects.equals(this.claimsSupported, inlineResponse20017.claimsSupported) &&
        Objects.equals(this.tokenEndpointAuthMethodsSupported, inlineResponse20017.tokenEndpointAuthMethodsSupported);
  }

  @Override
  public int hashCode() {
    return Objects.hash(issuer, authorizationEndpoint, tokenEndpoint, jwksUri, userinfoEndpoint, idTokenSigningAlgValuesSupported, responseTypesSupported, subjectTypesSupported, scopesSupported, claimsSupported, tokenEndpointAuthMethodsSupported);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse20017 {\n");
    
    sb.append("    issuer: ").append(toIndentedString(issuer)).append("\n");
    sb.append("    authorizationEndpoint: ").append(toIndentedString(authorizationEndpoint)).append("\n");
    sb.append("    tokenEndpoint: ").append(toIndentedString(tokenEndpoint)).append("\n");
    sb.append("    jwksUri: ").append(toIndentedString(jwksUri)).append("\n");
    sb.append("    userinfoEndpoint: ").append(toIndentedString(userinfoEndpoint)).append("\n");
    sb.append("    idTokenSigningAlgValuesSupported: ").append(toIndentedString(idTokenSigningAlgValuesSupported)).append("\n");
    sb.append("    responseTypesSupported: ").append(toIndentedString(responseTypesSupported)).append("\n");
    sb.append("    subjectTypesSupported: ").append(toIndentedString(subjectTypesSupported)).append("\n");
    sb.append("    scopesSupported: ").append(toIndentedString(scopesSupported)).append("\n");
    sb.append("    claimsSupported: ").append(toIndentedString(claimsSupported)).append("\n");
    sb.append("    tokenEndpointAuthMethodsSupported: ").append(toIndentedString(tokenEndpointAuthMethodsSupported)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
