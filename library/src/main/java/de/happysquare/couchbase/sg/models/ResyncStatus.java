/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * The status of a resync operation
 */
@Schema(description = "The status of a resync operation")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class Resyncstatus {
  /**
   * The status of the current operation.
   */
  @JsonAdapter(StatusEnum.Adapter.class)
  public enum StatusEnum {
    @SerializedName("running")
    RUNNING("running"),
    @SerializedName("completed")
    COMPLETED("completed"),
    @SerializedName("stopping")
    STOPPING("stopping"),
    @SerializedName("stopped")
    STOPPED("stopped"),
    @SerializedName("error")
    ERROR("error");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static StatusEnum fromValue(String input) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<StatusEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final StatusEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public StatusEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextString();
        return StatusEnum.fromValue((String)(value));
      }
    }
  }  @SerializedName("status")
  private StatusEnum status = null;

  @SerializedName("start_time")
  private String startTime = null;

  @SerializedName("last_error")
  private String lastError = null;

  @SerializedName("docs_changed")
  private Integer docsChanged = null;

  @SerializedName("docs_processed")
  private Integer docsProcessed = null;

  public Resyncstatus status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * The status of the current operation.
   * @return status
  **/
  @Schema(required = true, description = "The status of the current operation.")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public Resyncstatus startTime(String startTime) {
    this.startTime = startTime;
    return this;
  }

   /**
   * The ISO-8601 date and time the resync operation was started.
   * @return startTime
  **/
  @Schema(required = true, description = "The ISO-8601 date and time the resync operation was started.")
  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public Resyncstatus lastError(String lastError) {
    this.lastError = lastError;
    return this;
  }

   /**
   * The last error that occurred in the resync operation (if any).
   * @return lastError
  **/
  @Schema(required = true, description = "The last error that occurred in the resync operation (if any).")
  public String getLastError() {
    return lastError;
  }

  public void setLastError(String lastError) {
    this.lastError = lastError;
  }

  public Resyncstatus docsChanged(Integer docsChanged) {
    this.docsChanged = docsChanged;
    return this;
  }

   /**
   * The amount of documents that have been changed as a result of the resync operation.
   * @return docsChanged
  **/
  @Schema(required = true, description = "The amount of documents that have been changed as a result of the resync operation.")
  public Integer getDocsChanged() {
    return docsChanged;
  }

  public void setDocsChanged(Integer docsChanged) {
    this.docsChanged = docsChanged;
  }

  public Resyncstatus docsProcessed(Integer docsProcessed) {
    this.docsProcessed = docsProcessed;
    return this;
  }

   /**
   * The amount of docs that have been processed so far in the resync operation.
   * @return docsProcessed
  **/
  @Schema(required = true, description = "The amount of docs that have been processed so far in the resync operation.")
  public Integer getDocsProcessed() {
    return docsProcessed;
  }

  public void setDocsProcessed(Integer docsProcessed) {
    this.docsProcessed = docsProcessed;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Resyncstatus resyncstatus = (Resyncstatus) o;
    return Objects.equals(this.status, resyncstatus.status) &&
        Objects.equals(this.startTime, resyncstatus.startTime) &&
        Objects.equals(this.lastError, resyncstatus.lastError) &&
        Objects.equals(this.docsChanged, resyncstatus.docsChanged) &&
        Objects.equals(this.docsProcessed, resyncstatus.docsProcessed);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, startTime, lastError, docsChanged, docsProcessed);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Resyncstatus {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    lastError: ").append(toIndentedString(lastError)).append("\n");
    sb.append("    docsChanged: ").append(toIndentedString(docsChanged)).append("\n");
    sb.append("    docsProcessed: ").append(toIndentedString(docsProcessed)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
