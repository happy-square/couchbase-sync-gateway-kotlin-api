/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.happysquare.couchbase.sg.models.DbDesignddocOptions;
import de.happysquare.couchbase.sg.models.DbDesignddocViews;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Properties of a design document
 */
@Schema(description = "Properties of a design document")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class DesignDdocBody {
  @SerializedName("language")
  private String language = null;

  @SerializedName("views")
  private Map<String, DbDesignddocViews> views = null;

  @SerializedName("options")
  private DbDesignddocOptions options = null;

  public DesignDdocBody language(String language) {
    this.language = language;
    return this;
  }

   /**
   * Get language
   * @return language
  **/
  @Schema(description = "")
  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public DesignDdocBody views(Map<String, DbDesignddocViews> views) {
    this.views = views;
    return this;
  }

  public DesignDdocBody putViewsItem(String key, DbDesignddocViews viewsItem) {
    if (this.views == null) {
      this.views = new HashMap<String, DbDesignddocViews>();
    }
    this.views.put(key, viewsItem);
    return this;
  }

   /**
   * Get views
   * @return views
  **/
  @Schema(description = "")
  public Map<String, DbDesignddocViews> getViews() {
    return views;
  }

  public void setViews(Map<String, DbDesignddocViews> views) {
    this.views = views;
  }

  public DesignDdocBody options(DbDesignddocOptions options) {
    this.options = options;
    return this;
  }

   /**
   * Get options
   * @return options
  **/
  @Schema(description = "")
  public DbDesignddocOptions getOptions() {
    return options;
  }

  public void setOptions(DbDesignddocOptions options) {
    this.options = options;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DesignDdocBody _designDdocBody = (DesignDdocBody) o;
    return Objects.equals(this.language, _designDdocBody.language) &&
        Objects.equals(this.views, _designDdocBody.views) &&
        Objects.equals(this.options, _designDdocBody.options);
  }

  @Override
  public int hashCode() {
    return Objects.hash(language, views, options);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DesignDdocBody {\n");
    
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("    views: ").append(toIndentedString(views)).append("\n");
    sb.append("    options: ").append(toIndentedString(options)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
