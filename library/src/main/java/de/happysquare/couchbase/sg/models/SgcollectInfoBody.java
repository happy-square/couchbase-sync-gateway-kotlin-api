/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * SgcollectInfoBody
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class SgcollectInfoBody {
  /**
   * The redaction level to use for redacting the collected logs.
   */
  @JsonAdapter(RedactLevelEnum.Adapter.class)
  public enum RedactLevelEnum {
    @SerializedName("partial")
    PARTIAL("partial"),
    @SerializedName("none")
    NONE("none");

    private String value;

    RedactLevelEnum(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    public static RedactLevelEnum fromValue(String input) {
      for (RedactLevelEnum b : RedactLevelEnum.values()) {
        if (b.value.equals(input)) {
          return b;
        }
      }
      return null;
    }
    public static class Adapter extends TypeAdapter<RedactLevelEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final RedactLevelEnum enumeration) throws IOException {
        jsonWriter.value(String.valueOf(enumeration.getValue()));
      }

      @Override
      public RedactLevelEnum read(final JsonReader jsonReader) throws IOException {
        Object value = jsonReader.nextString();
        return RedactLevelEnum.fromValue((String)(value));
      }
    }
  }  @SerializedName("redact_level")
  private RedactLevelEnum redactLevel = RedactLevelEnum.PARTIAL;

  @SerializedName("redact_salt")
  private String redactSalt = null;

  @SerializedName("output_dir")
  private String outputDir = "The configured path set in the startup config `logging.log_file_path`";

  @SerializedName("upload")
  private Boolean upload = null;

  @SerializedName("upload_host")
  private String uploadHost = "https://uploads.couchbase.com";

  @SerializedName("upload_proxy")
  private String uploadProxy = null;

  @SerializedName("customer")
  private String customer = null;

  @SerializedName("ticket")
  private String ticket = null;

  public SgcollectInfoBody redactLevel(RedactLevelEnum redactLevel) {
    this.redactLevel = redactLevel;
    return this;
  }

   /**
   * The redaction level to use for redacting the collected logs.
   * @return redactLevel
  **/
  @Schema(description = "The redaction level to use for redacting the collected logs.")
  public RedactLevelEnum getRedactLevel() {
    return redactLevel;
  }

  public void setRedactLevel(RedactLevelEnum redactLevel) {
    this.redactLevel = redactLevel;
  }

  public SgcollectInfoBody redactSalt(String redactSalt) {
    this.redactSalt = redactSalt;
    return this;
  }

   /**
   * The salt to use for the log redactions.
   * @return redactSalt
  **/
  @Schema(description = "The salt to use for the log redactions.")
  public String getRedactSalt() {
    return redactSalt;
  }

  public void setRedactSalt(String redactSalt) {
    this.redactSalt = redactSalt;
  }

  public SgcollectInfoBody outputDir(String outputDir) {
    this.outputDir = outputDir;
    return this;
  }

   /**
   * The directory to output the collected logs zip file at.  This overrides the configured default output directory configured in the startup config &#x60;logging.log_file_path&#x60;.
   * @return outputDir
  **/
  @Schema(description = "The directory to output the collected logs zip file at.  This overrides the configured default output directory configured in the startup config `logging.log_file_path`.")
  public String getOutputDir() {
    return outputDir;
  }

  public void setOutputDir(String outputDir) {
    this.outputDir = outputDir;
  }

  public SgcollectInfoBody upload(Boolean upload) {
    this.upload = upload;
    return this;
  }

   /**
   * If set, upload the logs to Couchbase Support.  A customer name must be set if this is set.
   * @return upload
  **/
  @Schema(description = "If set, upload the logs to Couchbase Support.  A customer name must be set if this is set.")
  public Boolean isUpload() {
    return upload;
  }

  public void setUpload(Boolean upload) {
    this.upload = upload;
  }

  public SgcollectInfoBody uploadHost(String uploadHost) {
    this.uploadHost = uploadHost;
    return this;
  }

   /**
   * The host to send the logs too.
   * @return uploadHost
  **/
  @Schema(description = "The host to send the logs too.")
  public String getUploadHost() {
    return uploadHost;
  }

  public void setUploadHost(String uploadHost) {
    this.uploadHost = uploadHost;
  }

  public SgcollectInfoBody uploadProxy(String uploadProxy) {
    this.uploadProxy = uploadProxy;
    return this;
  }

   /**
   * The proxy to use while uploading the logs.
   * @return uploadProxy
  **/
  @Schema(description = "The proxy to use while uploading the logs.")
  public String getUploadProxy() {
    return uploadProxy;
  }

  public void setUploadProxy(String uploadProxy) {
    this.uploadProxy = uploadProxy;
  }

  public SgcollectInfoBody customer(String customer) {
    this.customer = customer;
    return this;
  }

   /**
   * The customer name to use when uploading the logs.
   * @return customer
  **/
  @Schema(description = "The customer name to use when uploading the logs.")
  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  public SgcollectInfoBody ticket(String ticket) {
    this.ticket = ticket;
    return this;
  }

   /**
   * The Zendesk ticket number to use when uploading logs.
   * @return ticket
  **/
  @Schema(description = "The Zendesk ticket number to use when uploading logs.")
  public String getTicket() {
    return ticket;
  }

  public void setTicket(String ticket) {
    this.ticket = ticket;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SgcollectInfoBody _sgcollectInfoBody = (SgcollectInfoBody) o;
    return Objects.equals(this.redactLevel, _sgcollectInfoBody.redactLevel) &&
        Objects.equals(this.redactSalt, _sgcollectInfoBody.redactSalt) &&
        Objects.equals(this.outputDir, _sgcollectInfoBody.outputDir) &&
        Objects.equals(this.upload, _sgcollectInfoBody.upload) &&
        Objects.equals(this.uploadHost, _sgcollectInfoBody.uploadHost) &&
        Objects.equals(this.uploadProxy, _sgcollectInfoBody.uploadProxy) &&
        Objects.equals(this.customer, _sgcollectInfoBody.customer) &&
        Objects.equals(this.ticket, _sgcollectInfoBody.ticket);
  }

  @Override
  public int hashCode() {
    return Objects.hash(redactLevel, redactSalt, outputDir, upload, uploadHost, uploadProxy, customer, ticket);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SgcollectInfoBody {\n");
    
    sb.append("    redactLevel: ").append(toIndentedString(redactLevel)).append("\n");
    sb.append("    redactSalt: ").append(toIndentedString(redactSalt)).append("\n");
    sb.append("    outputDir: ").append(toIndentedString(outputDir)).append("\n");
    sb.append("    upload: ").append(toIndentedString(upload)).append("\n");
    sb.append("    uploadHost: ").append(toIndentedString(uploadHost)).append("\n");
    sb.append("    uploadProxy: ").append(toIndentedString(uploadProxy)).append("\n");
    sb.append("    customer: ").append(toIndentedString(customer)).append("\n");
    sb.append("    ticket: ").append(toIndentedString(ticket)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
