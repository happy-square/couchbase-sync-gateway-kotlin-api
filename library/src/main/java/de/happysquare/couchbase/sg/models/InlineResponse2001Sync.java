/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.happysquare.couchbase.sg.models.InlineResponse2001SyncChannelSet;
import de.happysquare.couchbase.sg.models.InlineResponse2001SyncChannelSetHistory;
import de.happysquare.couchbase.sg.models.InlineResponse2001SyncHistory;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * InlineResponse2001Sync
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class InlineResponse2001Sync {
  @SerializedName("rev")
  private String rev = null;

  @SerializedName("sequence")
  private BigDecimal sequence = null;

  @SerializedName("recent_sequences")
  private List<BigDecimal> recentSequences = null;

  @SerializedName("history")
  private InlineResponse2001SyncHistory history = null;

  @SerializedName("cas")
  private String cas = null;

  @SerializedName("value_crc32c")
  private String valueCrc32c = null;

  @SerializedName("channel_set")
  private List<InlineResponse2001SyncChannelSet> channelSet = null;

  @SerializedName("channel_set_history")
  private List<InlineResponse2001SyncChannelSetHistory> channelSetHistory = null;

  @SerializedName("time_saved")
  private String timeSaved = null;

  public InlineResponse2001Sync rev(String rev) {
    this.rev = rev;
    return this;
  }

   /**
   * The current document revision ID.
   * @return rev
  **/
  @Schema(description = "The current document revision ID.")
  public String getRev() {
    return rev;
  }

  public void setRev(String rev) {
    this.rev = rev;
  }

  public InlineResponse2001Sync sequence(BigDecimal sequence) {
    this.sequence = sequence;
    return this;
  }

   /**
   * The most recent sequence number of the document.
   * @return sequence
  **/
  @Schema(description = "The most recent sequence number of the document.")
  public BigDecimal getSequence() {
    return sequence;
  }

  public void setSequence(BigDecimal sequence) {
    this.sequence = sequence;
  }

  public InlineResponse2001Sync recentSequences(List<BigDecimal> recentSequences) {
    this.recentSequences = recentSequences;
    return this;
  }

  public InlineResponse2001Sync addRecentSequencesItem(BigDecimal recentSequencesItem) {
    if (this.recentSequences == null) {
      this.recentSequences = new ArrayList<BigDecimal>();
    }
    this.recentSequences.add(recentSequencesItem);
    return this;
  }

   /**
   * The previous sequence numbers of the document.
   * @return recentSequences
  **/
  @Schema(description = "The previous sequence numbers of the document.")
  public List<BigDecimal> getRecentSequences() {
    return recentSequences;
  }

  public void setRecentSequences(List<BigDecimal> recentSequences) {
    this.recentSequences = recentSequences;
  }

  public InlineResponse2001Sync history(InlineResponse2001SyncHistory history) {
    this.history = history;
    return this;
  }

   /**
   * Get history
   * @return history
  **/
  @Schema(description = "")
  public InlineResponse2001SyncHistory getHistory() {
    return history;
  }

  public void setHistory(InlineResponse2001SyncHistory history) {
    this.history = history;
  }

  public InlineResponse2001Sync cas(String cas) {
    this.cas = cas;
    return this;
  }

   /**
   * The document CAS (Concurrent Document Mutations) number used for document locking.
   * @return cas
  **/
  @Schema(description = "The document CAS (Concurrent Document Mutations) number used for document locking.")
  public String getCas() {
    return cas;
  }

  public void setCas(String cas) {
    this.cas = cas;
  }

  public InlineResponse2001Sync valueCrc32c(String valueCrc32c) {
    this.valueCrc32c = valueCrc32c;
    return this;
  }

   /**
   * The documents CRC32 number.
   * @return valueCrc32c
  **/
  @Schema(description = "The documents CRC32 number.")
  public String getValueCrc32c() {
    return valueCrc32c;
  }

  public void setValueCrc32c(String valueCrc32c) {
    this.valueCrc32c = valueCrc32c;
  }

  public InlineResponse2001Sync channelSet(List<InlineResponse2001SyncChannelSet> channelSet) {
    this.channelSet = channelSet;
    return this;
  }

  public InlineResponse2001Sync addChannelSetItem(InlineResponse2001SyncChannelSet channelSetItem) {
    if (this.channelSet == null) {
      this.channelSet = new ArrayList<InlineResponse2001SyncChannelSet>();
    }
    this.channelSet.add(channelSetItem);
    return this;
  }

   /**
   * The channels the document has been in.
   * @return channelSet
  **/
  @Schema(description = "The channels the document has been in.")
  public List<InlineResponse2001SyncChannelSet> getChannelSet() {
    return channelSet;
  }

  public void setChannelSet(List<InlineResponse2001SyncChannelSet> channelSet) {
    this.channelSet = channelSet;
  }

  public InlineResponse2001Sync channelSetHistory(List<InlineResponse2001SyncChannelSetHistory> channelSetHistory) {
    this.channelSetHistory = channelSetHistory;
    return this;
  }

  public InlineResponse2001Sync addChannelSetHistoryItem(InlineResponse2001SyncChannelSetHistory channelSetHistoryItem) {
    if (this.channelSetHistory == null) {
      this.channelSetHistory = new ArrayList<InlineResponse2001SyncChannelSetHistory>();
    }
    this.channelSetHistory.add(channelSetHistoryItem);
    return this;
  }

   /**
   * Get channelSetHistory
   * @return channelSetHistory
  **/
  @Schema(description = "")
  public List<InlineResponse2001SyncChannelSetHistory> getChannelSetHistory() {
    return channelSetHistory;
  }

  public void setChannelSetHistory(List<InlineResponse2001SyncChannelSetHistory> channelSetHistory) {
    this.channelSetHistory = channelSetHistory;
  }

  public InlineResponse2001Sync timeSaved(String timeSaved) {
    this.timeSaved = timeSaved;
    return this;
  }

   /**
   * The time and date the document was most recently changed.
   * @return timeSaved
  **/
  @Schema(description = "The time and date the document was most recently changed.")
  public String getTimeSaved() {
    return timeSaved;
  }

  public void setTimeSaved(String timeSaved) {
    this.timeSaved = timeSaved;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2001Sync inlineResponse2001Sync = (InlineResponse2001Sync) o;
    return Objects.equals(this.rev, inlineResponse2001Sync.rev) &&
        Objects.equals(this.sequence, inlineResponse2001Sync.sequence) &&
        Objects.equals(this.recentSequences, inlineResponse2001Sync.recentSequences) &&
        Objects.equals(this.history, inlineResponse2001Sync.history) &&
        Objects.equals(this.cas, inlineResponse2001Sync.cas) &&
        Objects.equals(this.valueCrc32c, inlineResponse2001Sync.valueCrc32c) &&
        Objects.equals(this.channelSet, inlineResponse2001Sync.channelSet) &&
        Objects.equals(this.channelSetHistory, inlineResponse2001Sync.channelSetHistory) &&
        Objects.equals(this.timeSaved, inlineResponse2001Sync.timeSaved);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rev, sequence, recentSequences, history, cas, valueCrc32c, channelSet, channelSetHistory, timeSaved);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2001Sync {\n");
    
    sb.append("    rev: ").append(toIndentedString(rev)).append("\n");
    sb.append("    sequence: ").append(toIndentedString(sequence)).append("\n");
    sb.append("    recentSequences: ").append(toIndentedString(recentSequences)).append("\n");
    sb.append("    history: ").append(toIndentedString(history)).append("\n");
    sb.append("    cas: ").append(toIndentedString(cas)).append("\n");
    sb.append("    valueCrc32c: ").append(toIndentedString(valueCrc32c)).append("\n");
    sb.append("    channelSet: ").append(toIndentedString(channelSet)).append("\n");
    sb.append("    channelSetHistory: ").append(toIndentedString(channelSetHistory)).append("\n");
    sb.append("    timeSaved: ").append(toIndentedString(timeSaved)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
