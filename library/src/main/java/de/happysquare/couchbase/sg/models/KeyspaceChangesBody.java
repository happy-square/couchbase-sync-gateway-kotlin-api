/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * KeyspaceChangesBody
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class KeyspaceChangesBody {
  @SerializedName("limit")
  private String limit = null;

  @SerializedName("style")
  private String style = null;

  @SerializedName("active_only")
  private String activeOnly = null;

  @SerializedName("include_docs")
  private String includeDocs = null;

  @SerializedName("revocations")
  private String revocations = null;

  @SerializedName("filter")
  private String filter = null;

  @SerializedName("channels")
  private String channels = null;

  @SerializedName("doc_ids")
  private String docIds = null;

  @SerializedName("heartbeat")
  private String heartbeat = null;

  @SerializedName("timeout")
  private String timeout = null;

  @SerializedName("feed")
  private String feed = null;

  @SerializedName("request_plus")
  private String requestPlus = null;

  public KeyspaceChangesBody limit(String limit) {
    this.limit = limit;
    return this;
  }

   /**
   * Maximum number of changes to return.
   * @return limit
  **/
  @Schema(description = "Maximum number of changes to return.")
  public String getLimit() {
    return limit;
  }

  public void setLimit(String limit) {
    this.limit = limit;
  }

  public KeyspaceChangesBody style(String style) {
    this.style = style;
    return this;
  }

   /**
   * Controls whether to return the current winning revision (&#x60;main_only&#x60;) or all the leaf revision including conflicts and deleted former conflicts (&#x60;all_docs&#x60;).
   * @return style
  **/
  @Schema(description = "Controls whether to return the current winning revision (`main_only`) or all the leaf revision including conflicts and deleted former conflicts (`all_docs`).")
  public String getStyle() {
    return style;
  }

  public void setStyle(String style) {
    this.style = style;
  }

  public KeyspaceChangesBody activeOnly(String activeOnly) {
    this.activeOnly = activeOnly;
    return this;
  }

   /**
   * Set true to exclude deleted documents and notifications for documents the user no longer has access to from the changes feed.
   * @return activeOnly
  **/
  @Schema(description = "Set true to exclude deleted documents and notifications for documents the user no longer has access to from the changes feed.")
  public String getActiveOnly() {
    return activeOnly;
  }

  public void setActiveOnly(String activeOnly) {
    this.activeOnly = activeOnly;
  }

  public KeyspaceChangesBody includeDocs(String includeDocs) {
    this.includeDocs = includeDocs;
    return this;
  }

   /**
   * Include the body associated with each document.
   * @return includeDocs
  **/
  @Schema(description = "Include the body associated with each document.")
  public String getIncludeDocs() {
    return includeDocs;
  }

  public void setIncludeDocs(String includeDocs) {
    this.includeDocs = includeDocs;
  }

  public KeyspaceChangesBody revocations(String revocations) {
    this.revocations = revocations;
    return this;
  }

   /**
   * If true, revocation messages will be sent on the changes feed.
   * @return revocations
  **/
  @Schema(description = "If true, revocation messages will be sent on the changes feed.")
  public String getRevocations() {
    return revocations;
  }

  public void setRevocations(String revocations) {
    this.revocations = revocations;
  }

  public KeyspaceChangesBody filter(String filter) {
    this.filter = filter;
    return this;
  }

   /**
   * Set a filter to either filter by channels or document IDs.
   * @return filter
  **/
  @Schema(description = "Set a filter to either filter by channels or document IDs.")
  public String getFilter() {
    return filter;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }

  public KeyspaceChangesBody channels(String channels) {
    this.channels = channels;
    return this;
  }

   /**
   * A comma-separated list of channel names to filter the response to only the channels specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;sync_gateway/bychannels&#x60;.
   * @return channels
  **/
  @Schema(description = "A comma-separated list of channel names to filter the response to only the channels specified. To use this option, the `filter` query option must be set to `sync_gateway/bychannels`.")
  public String getChannels() {
    return channels;
  }

  public void setChannels(String channels) {
    this.channels = channels;
  }

  public KeyspaceChangesBody docIds(String docIds) {
    this.docIds = docIds;
    return this;
  }

   /**
   * A valid JSON array of document IDs to filter the documents in the response to only the documents specified. To use this option, the &#x60;filter&#x60; query option must be set to &#x60;_doc_ids&#x60; and the &#x60;feed&#x60; parameter must be &#x60;normal&#x60;.
   * @return docIds
  **/
  @Schema(description = "A valid JSON array of document IDs to filter the documents in the response to only the documents specified. To use this option, the `filter` query option must be set to `_doc_ids` and the `feed` parameter must be `normal`.")
  public String getDocIds() {
    return docIds;
  }

  public void setDocIds(String docIds) {
    this.docIds = docIds;
  }

  public KeyspaceChangesBody heartbeat(String heartbeat) {
    this.heartbeat = heartbeat;
    return this;
  }

   /**
   * The interval (in milliseconds) to send an empty line (CRLF) in the response. This is to help prevent gateways from deciding the socket is idle and therefore closing it. This is only applicable to &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60;. This will override any timeouts to keep the feed alive indefinitely. Setting to 0 results in no heartbeat. The maximum heartbeat can be set in the server replication configuration.
   * @return heartbeat
  **/
  @Schema(description = "The interval (in milliseconds) to send an empty line (CRLF) in the response. This is to help prevent gateways from deciding the socket is idle and therefore closing it. This is only applicable to `feed=longpoll` or `feed=continuous`. This will override any timeouts to keep the feed alive indefinitely. Setting to 0 results in no heartbeat. The maximum heartbeat can be set in the server replication configuration.")
  public String getHeartbeat() {
    return heartbeat;
  }

  public void setHeartbeat(String heartbeat) {
    this.heartbeat = heartbeat;
  }

  public KeyspaceChangesBody timeout(String timeout) {
    this.timeout = timeout;
    return this;
  }

   /**
   * This is the maximum period (in milliseconds) to wait for a change before the response is sent, even if there are no results. This is only applicable for &#x60;feed&#x3D;longpoll&#x60; or &#x60;feed&#x3D;continuous&#x60; changes feeds. Setting to 0 results in no timeout.
   * @return timeout
  **/
  @Schema(description = "This is the maximum period (in milliseconds) to wait for a change before the response is sent, even if there are no results. This is only applicable for `feed=longpoll` or `feed=continuous` changes feeds. Setting to 0 results in no timeout.")
  public String getTimeout() {
    return timeout;
  }

  public void setTimeout(String timeout) {
    this.timeout = timeout;
  }

  public KeyspaceChangesBody feed(String feed) {
    this.feed = feed;
    return this;
  }

   /**
   * The type of changes feed to use. 
   * @return feed
  **/
  @Schema(description = "The type of changes feed to use. ")
  public String getFeed() {
    return feed;
  }

  public void setFeed(String feed) {
    this.feed = feed;
  }

  public KeyspaceChangesBody requestPlus(String requestPlus) {
    this.requestPlus = requestPlus;
    return this;
  }

   /**
   * When true, ensures all valid documents written prior to the request being issued are included in the response.  This is only applicable for non-continuous feeds.
   * @return requestPlus
  **/
  @Schema(description = "When true, ensures all valid documents written prior to the request being issued are included in the response.  This is only applicable for non-continuous feeds.")
  public String getRequestPlus() {
    return requestPlus;
  }

  public void setRequestPlus(String requestPlus) {
    this.requestPlus = requestPlus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KeyspaceChangesBody keyspaceChangesBody = (KeyspaceChangesBody) o;
    return Objects.equals(this.limit, keyspaceChangesBody.limit) &&
        Objects.equals(this.style, keyspaceChangesBody.style) &&
        Objects.equals(this.activeOnly, keyspaceChangesBody.activeOnly) &&
        Objects.equals(this.includeDocs, keyspaceChangesBody.includeDocs) &&
        Objects.equals(this.revocations, keyspaceChangesBody.revocations) &&
        Objects.equals(this.filter, keyspaceChangesBody.filter) &&
        Objects.equals(this.channels, keyspaceChangesBody.channels) &&
        Objects.equals(this.docIds, keyspaceChangesBody.docIds) &&
        Objects.equals(this.heartbeat, keyspaceChangesBody.heartbeat) &&
        Objects.equals(this.timeout, keyspaceChangesBody.timeout) &&
        Objects.equals(this.feed, keyspaceChangesBody.feed) &&
        Objects.equals(this.requestPlus, keyspaceChangesBody.requestPlus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(limit, style, activeOnly, includeDocs, revocations, filter, channels, docIds, heartbeat, timeout, feed, requestPlus);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KeyspaceChangesBody {\n");
    
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    style: ").append(toIndentedString(style)).append("\n");
    sb.append("    activeOnly: ").append(toIndentedString(activeOnly)).append("\n");
    sb.append("    includeDocs: ").append(toIndentedString(includeDocs)).append("\n");
    sb.append("    revocations: ").append(toIndentedString(revocations)).append("\n");
    sb.append("    filter: ").append(toIndentedString(filter)).append("\n");
    sb.append("    channels: ").append(toIndentedString(channels)).append("\n");
    sb.append("    docIds: ").append(toIndentedString(docIds)).append("\n");
    sb.append("    heartbeat: ").append(toIndentedString(heartbeat)).append("\n");
    sb.append("    timeout: ").append(toIndentedString(timeout)).append("\n");
    sb.append("    feed: ").append(toIndentedString(feed)).append("\n");
    sb.append("    requestPlus: ").append(toIndentedString(requestPlus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
