/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * The name of the collection.
 */
@Schema(description = "The name of the collection.")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class InlineResponse2009Collections {
  @SerializedName("update_seq")
  private Integer updateSeq = null;

  public InlineResponse2009Collections updateSeq(Integer updateSeq) {
    this.updateSeq = updateSeq;
    return this;
  }

   /**
   * The last sequence number that was committed to the collection.
   * @return updateSeq
  **/
  @Schema(example = "123456", description = "The last sequence number that was committed to the collection.")
  public Integer getUpdateSeq() {
    return updateSeq;
  }

  public void setUpdateSeq(Integer updateSeq) {
    this.updateSeq = updateSeq;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2009Collections inlineResponse2009Collections = (InlineResponse2009Collections) o;
    return Objects.equals(this.updateSeq, inlineResponse2009Collections.updateSeq);
  }

  @Override
  public int hashCode() {
    return Objects.hash(updateSeq);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2009Collections {\n");
    
    sb.append("    updateSeq: ").append(toIndentedString(updateSeq)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
