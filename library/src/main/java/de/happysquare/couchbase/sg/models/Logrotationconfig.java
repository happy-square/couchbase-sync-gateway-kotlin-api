/*
 * Sync Gateway
 * Sync Gateway manages access and synchronization between Couchbase Lite and Couchbase Server
 *
 * OpenAPI spec version: 3.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.happysquare.couchbase.sg.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * Logrotationconfig
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2023-08-22T13:35:47.438049+02:00[Europe/Berlin]")

public class Logrotationconfig {
  @SerializedName("max_size")
  private Integer maxSize = null;

  @SerializedName("max_age")
  private Integer maxAge = null;

  @SerializedName("localtime")
  private Boolean localtime = null;

  @SerializedName("rotated_logs_size")
  private Integer rotatedLogsSize = null;

  public Logrotationconfig maxSize(Integer maxSize) {
    this.maxSize = maxSize;
    return this;
  }

   /**
   * The maximum size in MB of the log file before it gets rotated.
   * @return maxSize
  **/
  @Schema(description = "The maximum size in MB of the log file before it gets rotated.")
  public Integer getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(Integer maxSize) {
    this.maxSize = maxSize;
  }

  public Logrotationconfig maxAge(Integer maxAge) {
    this.maxAge = maxAge;
    return this;
  }

   /**
   * The maximum number of days to retain old log files.
   * @return maxAge
  **/
  @Schema(description = "The maximum number of days to retain old log files.")
  public Integer getMaxAge() {
    return maxAge;
  }

  public void setMaxAge(Integer maxAge) {
    this.maxAge = maxAge;
  }

  public Logrotationconfig localtime(Boolean localtime) {
    this.localtime = localtime;
    return this;
  }

   /**
   * If true, it uses the computer&#x27;s local time to format the backup timestamp.
   * @return localtime
  **/
  @Schema(description = "If true, it uses the computer's local time to format the backup timestamp.")
  public Boolean isLocaltime() {
    return localtime;
  }

  public void setLocaltime(Boolean localtime) {
    this.localtime = localtime;
  }

  public Logrotationconfig rotatedLogsSize(Integer rotatedLogsSize) {
    this.rotatedLogsSize = rotatedLogsSize;
    return this;
  }

   /**
   * Max Size (in mb) of log files before deletion
   * @return rotatedLogsSize
  **/
  @Schema(description = "Max Size (in mb) of log files before deletion")
  public Integer getRotatedLogsSize() {
    return rotatedLogsSize;
  }

  public void setRotatedLogsSize(Integer rotatedLogsSize) {
    this.rotatedLogsSize = rotatedLogsSize;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Logrotationconfig logrotationconfig = (Logrotationconfig) o;
    return Objects.equals(this.maxSize, logrotationconfig.maxSize) &&
        Objects.equals(this.maxAge, logrotationconfig.maxAge) &&
        Objects.equals(this.localtime, logrotationconfig.localtime) &&
        Objects.equals(this.rotatedLogsSize, logrotationconfig.rotatedLogsSize);
  }

  @Override
  public int hashCode() {
    return Objects.hash(maxSize, maxAge, localtime, rotatedLogsSize);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Logrotationconfig {\n");
    
    sb.append("    maxSize: ").append(toIndentedString(maxSize)).append("\n");
    sb.append("    maxAge: ").append(toIndentedString(maxAge)).append("\n");
    sb.append("    localtime: ").append(toIndentedString(localtime)).append("\n");
    sb.append("    rotatedLogsSize: ").append(toIndentedString(rotatedLogsSize)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
